#### Validate and Prepare RMM Data

** This folder needs cleaning, most scripts aren't used **

Use the `validate_data.py` to generate png figs of the RMM output. This can be changed for whatever the validation needs are. 

Next is to convert the RMM root files into a `.csv` for Tensorflow input.
Go into the folder `/prepare`

Here the macro `A_PROCESS` should be used. May need to be adjust to your needs
 
