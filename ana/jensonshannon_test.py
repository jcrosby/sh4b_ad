import matplotlib as plt
from scipy.spatial import distance
import math
import pandas as pd
import numpy as np

from global_module import *

import tensorflow as tf
import os,sys

print ('Number of arguments:', len(sys.argv), 'arguments.')
print ('Argument List:', str(sys.argv))
n = len(sys.argv)
if (n != 5):
      print ("Need more arguments!. Needs one models input data RMM, bsm RMM as csv.zip")
      sys.exit()

model = sys.argv[1]

inputData = sys.argv[2]
inputBSM  = sys.argv[3]

#  AE type
TYPE=sys.argv[5]
NN_TYPE=sys.argv[6]

input_trigger_type=int(TYPE.replace("t",""))


def custom_mse_loss(y_true, y_pred):
    # Assuming y_true and y_pred are tensors of shape (batch_size, N)
    n = tf.shape(y_true)[-1]  # N, original dimension of the input
    # Select N-1 dimensions for computation of MSE
    reduced_dim = n - 1
    y_true_reduced = y_true[:, :reduced_dim]
    y_pred_reduced = y_pred[:, :reduced_dim]
    mse_loss = tf.keras.losses.mse(y_true_reduced, y_pred_reduced)
    return mse_loss


if (NN_TYPE == "AE"):
  fj1="models/training_v11/"+TYPE+"/"+model+"/mdAEleakyRelu800_400_200_400_800_bs100_sc1.0_dbFalse_seed101_laten20_lr0.001_sergei/models"
  print("--> Loading AE model from "+fj1)
  loaded_model = tf.keras.models.load_model( fj1 )
  loaded_model.summary()
  loaded_model.compile(optimizer='adam', loss='mse' )

if (NN_TYPE == "PAE"):
  fj1="models/training_v11/parametrised/"+model+"/mdAEleakyRelu800_400_200_400_800_bs100_sc1.0_dbFalse_seed101_laten20_lr0.001_sergei/models"
  print("--> Loading  PARAMETERIZED model from "+fj1)
  loaded_model = tf.keras.models.load_model(fj1, custom_objects={'custom_mse_loss': custom_mse_loss})
  loaded_model.summary()
  loaded_model.compile(optimizer='adam', loss='mse' )
# one-hot-encoder
if (NN_TYPE == "HAE"):
  fj1="models/training_v11/one_hot_encoding/"+model+"/mdAEleakyRelu800_400_200_400_800_bs100_sc1.0_dbFalse_seed101_laten20_lr0.001_sergei/models"
  print("--> Loading ONE-HOT-ENCORDER  model from "+fj1)
  loaded_model = tf.keras.models.load_model(fj1, custom_objects={'custom_mse_loss': custom_mse_loss})
  loaded_model.summary()
  loaded_model.compile(optimizer='adam', loss='mse' )

if (loaded_model == None):
              print("Model was not loaded!")
              sys.exit()


IsReadCommonEmptyColumns=1
# 1 drop columns based on common vector
# 2 drop columns as found by the current dataframe
file0=""
if (IsReadCommonEmptyColumns==1):
   file0="columns_with_0_10j10b5rest.txt"
print ("Read columns with 0 from ",file0)
dcol0=pd.read_csv(file0,header = None)
col0=dcol0[dcol0.columns[0]]


# double lit to keep data for dataframe
columnsX=[]
for i in range(1,mSize*mSize+1):
       columnsX.append("V_"+str(i))
# last column labels the data (put 0) 
# columnsX.append("Label")
df = pd.DataFrame(columns=columnsX)
print("DF size=",df.size," DF shape=",df.shape," DF dimension=",df.ndim)

evtINchunk=1000
proc = [inputData,inputBSM]
rfile=[]
evt = 0
nchunk=0
loss_dist = []
for i in proc:
     rfile.append(ROOT.TFile.Open(i))
     print(i)

for p in range(len(proc)):
    for i in range(len(proc[p])):
        ev = 0
        RMM = np.zeros(shape=(evtINchunk, mSize*mSize))

        NtotInFile=(rfile[i].inputNN).GetEntries()
        print("Analyse ",NtotInFile," from file=",rfile[i])

        for event in rfile[i].inputNN:
            NN=(event.proj).size()
            emptyMatrix = numpy.zeros(shape=(mSize,mSize))

            dataRMM=(emptyMatrix.flatten()).tolist()
        
            RMM[evt,:]=dataRMM

            evt=evt+1
            ev=ev+1

            if (ev ==  NtotInFile or evt%evtINchunk==0):
                        df = pd.DataFrame(data=RMM, columns=columnsX)

                        # in the case of parameterized NN, need to add extra parameter..
                        if (NN_TYPE == "PAE"): df['Parameter'] = input_trigger_type;  # parameter that tells which trigger stream 
                        # one hot encoder
                        if (NN_TYPE == "HAE"):
                                for trg in range(1,8):  # channels 1-7 are 0 
                                    df[f'Parameter{trg}'] = 0
                                df[f'Parameter{input_trigger_type}'] = 1

                        df=df.drop(col0, axis = 1)
                       
                        RMM = df.to_numpy()

                        predictions = loaded_model.predict( RMM )

                        train_loss = tf.keras.losses.mae(predictions, RMM).numpy() 
                        # take log of loss
                        log_loss = math.log(train_loss)
                        loss_dist.append(log_loss)

                        nle=len(train_loss)
                        if (ev ==  NtotInFile):
                            nle=ev- nchunk*evtINchunk 
                            print(" -> Last event ",ev, " from ",NtotInFile, "remaining=",nle)



js = distance.jensenshannon(loss_dist[0],loss_dist[1])

print("Jenson-Shannon metric for the model: ",loaded_model)
print(js)