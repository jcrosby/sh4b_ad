# Anomaly detection https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.10.3542&rep=rep1&type=pdf
# Stouffer's Z-score

import sys
sys.path.append("modules/")
from AtlasStyle import *
from AtlasUtils import *
from global_module import *

from ROOT import TH1D,TF1,TProfile2D,TEllipse, THStack,TRandom3,TFile,TLatex,TLegend,TPaveText,TGraphErrors,kRed,kBlue,kGreen,kCyan,kAzure,kYellow,kTRUE
import ROOT

# trigger type
myinput="interactive"
trig_type="1" 
if (len(sys.argv) ==2):
   trig_type=sys.argv[1]
if (len(sys.argv) ==3):
   trig_type=sys.argv[1]
   myinput = sys.argv[2] 
if (len(sys.argv) == 4):
   trig_type=sys.argv[2]
   myinput = sys.argv[3]
   myinput = sys.argv[4]

print ("Mode=",myinput)
print ("trigger",trig_type)

Tlab="1 lep"
# the algoritm  the algorithm: HighsPt cut x 3 
if (int(trig_type)==1):
             Xmin=600
             Tlab="T1:\; MET"
if (int(trig_type)==2): # 1 lepton 
             Xmin=300
             Tlab="T2:\; 1 \ell"
if (int(trig_type)==3): # 2 lepton pT>25 GeV  
             Xmin=300
             Tlab="T3:\; 2 \ell"
if (int(trig_type)==4): #  single photon 
             Xmin=500  # 150*3 
             Tlab="T4:\; 1 \gamma"
if (int(trig_type)==5): #  2 photon    
             Xmin=300  # 160x3 
             Tlab="T5:\; 2 \gamma"
if (int(trig_type)==6): #  single jet (pT>500 GeV)  
             Xmin=1500  #  
             Tlab="T6:\; 1 jet"
if (int(trig_type)==7): #  4 jet  (lead 200 GeV) 
             Xmin=600  #  
             Tlab="T7:\; 4 jets"


print ('Number of arguments:', len(sys.argv), 'arguments.') 
print ('Argument List:', str(sys.argv))
print ('Use as: script.py -b 0 (or 1,2)') 
print ("Mode=",myinput) 


gROOT.Reset()
figdir="figs/"
fname=os.path.basename(__file__)
fname=fname.replace("loss_data1percent","loss_data1percent_"+trig_type);
epsfig=figdir+(fname).replace(".py",".eps")


nameX="log (Loss)"
nameY="Events"
#Ymin=0.1 
#Ymax=19000000000
Ymax=190000000000
Ymin=0.002
Xmin=-11.5
Xmax=-4

######################################################
gROOT.SetStyle("Plain");
gROOT.SetStyle("ATLAS");

c1=TCanvas("c_massjj","BPRE",10,10,600,500);
c1.Divide(1,1,0.008,0.007);
ps1 = TPostScript( epsfig,113)

c1.cd(1);
gPad.SetLogy(1)
gPad.SetLogx(0)
gPad.SetTopMargin(0.02)
gPad.SetBottomMargin(0.12)
gPad.SetLeftMargin(0.14)
gPad.SetRightMargin(0.02)


h=gPad.DrawFrame(Xmin,Ymin,Xmax,Ymax);
h.Draw()


name="Loss_"
root_model2use="root/data1percent_t"+trig_type+"_AE_nominal_10PB.root"
xfile=TFile(root_model2use)
xfile.ls()
data=xfile.Get(name+"data1percent")
data.SetTitle("")
data.SetStats(0)
data.SetLineWidth(2)
data.SetLineColor( 1 )
data.SetMarkerColor( 1 )
data.SetMarkerSize( 0.5 )
data.SetFillColor(0);
data.Draw("same histo pe")
xsum=data.Integral();
print("Summ=",xsum)


# mc20_13TeV:mc20_13TeV.426345.Pythia8EvtGen_A14NNPDF23LO_Zprime_tt_flatpT.deriv.DAOD_PHYS.e6880_s3681_r13145_p5631
run=426345
lcolor=42
ha7=drawBSM(name,lcolor,run,trig_type)
ha7.Draw("same histo")


run=426347
lcolor=3
ha8=drawBSM(name,lcolor,run,trig_type)
ha8.Draw("same histo")

# 100 fb
Xcut=CutOutlier_01PB 
xsum14000=data.Integral(data.FindBin(Xcut), data.FindBin(0));
print("Summ=",xsum14000, " for cut=",Xcut)

x1=c1.XtoPad(Xcut)
x2=c1.XtoPad(Xcut)
ar5=TArrow(x1,Ymin,x2,c1.YtoPad(200000),0.05,">");
ar5.SetLineWidth(3)
ar5.SetLineStyle(3)
ar5.SetLineColor(2)
ar5.Draw("same")


Xcut=CutOutlier_1PB
xsum14000=data.Integral(data.FindBin(Xcut), data.FindBin(0));
print("Summ=",xsum14000, " for cut=",Xcut)
x1=c1.XtoPad(Xcut)
x2=c1.XtoPad(Xcut)
ar7=TArrow(x1,Ymin,x2,c1.YtoPad(200000),0.05,">");
ar7.SetLineWidth(3)
ar7.SetLineStyle(2)
ar7.SetLineColor(2)
ar7.Draw("same")


# BSM cross sections at around 400 GeV
# -------------------------------------
# Techicolor model: 1 pb
# SSM:  9 pb
# H+ model:  9 pb
# DM model: 0.9 pb
# Simplified DM mode with W: 1.3 pb
# Radion model: 2.26 pb near 500 GeV
# Composite lepton model 1.4 pb (max)
 
# 10 pb
# 10000*140 = 1400000  #  1.4M
Xcut=CutOutlier_10PB
xsum14000=data.Integral(data.FindBin(Xcut), data.FindBin(0));
print("Summ=",xsum14000, " for cut=",Xcut)
x1=c1.XtoPad(Xcut)
x2=c1.XtoPad(Xcut)
ar6=TArrow(x1,Ymin,x2,c1.YtoPad(200000),0.05,">");
ar6.SetLineWidth(3)
ar6.SetLineStyle(1)
ar6.SetLineColor(2)
ar6.Draw("same")

dis=distanceHistograms(data, bsm=[ha7,ha8])
#dis=distanceHistograms(data, bsm=[data])
dis='%.2f'%( dis )


ax=h.GetXaxis(); ax.SetTitleOffset(0.8)
ax.SetTitle( nameX );
ay=h.GetYaxis(); ay.SetTitleOffset(0.8)
ay.SetTitle( nameY );
ax.SetTitleOffset(1.1); ay.SetTitleOffset(1.5)
ax.Draw("same")
ay.Draw("same")

leg2=TLegend(0.52, 0.75, 0.95, 0.87);
leg2.SetBorderSize(0);
leg2.SetTextFont(62);
leg2.SetFillColor(10);
leg2.SetTextSize(0.04);
leg2.AddEntry(data,"Run2 Data (1%)","lfp")
leg2.AddEntry(ha7,"Z\,{\\prime} \\rightarrow t\\bar{t}\, flatPT","lp")
leg2.AddEntry(ha8,"W\,{\\prime} \\rightarrow WZ\, flatPT","lp")
leg2.Draw("same")

leg3=TLegend(0.76, 0.510, 0.974, 0.640);
leg3.SetBorderSize(0);
leg3.SetTextFont(62);
leg3.SetFillColor(10);
leg3.SetTextSize(0.035);
# leg3.SetHeader("AR:")
leg3.AddEntry(ar6,"10 pb AR","l")
leg3.AddEntry(ar7,"1 pb AR","l")
leg3.AddEntry(ar5,"0.1 pb AR","l")
leg3.Draw("same")


myText(0.19,0.82,1,0.04,UsedData0)
myText(0.7,0.9,2,0.06, Tlab)
myText(0.2,0.3,36,0.06, "D="+str(dis) )
# myText(0.3,0.3,1,0.04,"BSM models added")
ATLASLabel(0.19,0.89,0.14,0.03)

## record ROOT file
rootfile=figdir+fname.replace(".py",".root")
out="root/"+rootfile
hfile=TFile(out,"RECREATE","FitResult")
data.SetName("data")
data.SetTitle("data")
data.Write()
hfile.Close()
print("Write=",out)



print (epsfig) 
gPad.RedrawAxis()
c1.Update()
ps1.Close()
if (myinput != "-b"):
#              if (raw_input("Press any key to exit") != "-9999"):
               if (input("Press any key to exit") != "-9999"):
                 c1.Close(); sys.exit(1);




