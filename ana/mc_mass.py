import sys
sys.path.append("modules/")
import math

print ('Number of arguments:', len(sys.argv), 'arguments..') 
print ('Argument List:', str(sys.argv) )
print ('Use as: script.py -b 1 (or 1, = 7)') 
myinput="interactive"
channel="jj"

# trigger type
trig_type=1
if (len(sys.argv) ==2):
   channel =sys.argv[1]
   trig_type=sys.argv[2]
if (len(sys.argv) ==3):
   channel =sys.argv[1]
   trig_type=sys.argv[2]
if (len(sys.argv) == 4):
   channel =sys.argv[1]
   trig_type=sys.argv[2]
   myinput = sys.argv[3]

print ("Mode=",myinput) 
print ("Channel=",channel) 


Xmin=300
Xmax=9000
Ymin=0.011
Ymax=10000000000-1000000

Tlab="sing lepton"
# the algoritm  the algorithm: HighsPt cut x 3 
if (int(trig_type)==1):
             Xmin=600
             Tlab="T1:\; MET"
if (int(trig_type)==2): # 1 lepton 
             Xmin=300
             Tlab="T2:\; 1 \ell"
if (int(trig_type)==3): # 2 lepton pT>25 GeV  
             Xmin=300
             Tlab="T3:\; 2 \ell"
if (int(trig_type)==4): #  single photon 
             Xmin=500  # 150*3 
             Tlab="T4:\; 1 #gamma"
if (int(trig_type)==5): #  2 photon    
             Xmin=300   # 160x3 
             Tlab="T5:\; 2 #gamma"
if (int(trig_type)==6): #  single jet (pT>500 GeV)  
             Xmin=1500  #  
             Tlab="T6:\; 1 jet"
if (int(trig_type)==7): #  4 jet  (lead 200 GeV) 
             Xmin=600  #  
             Tlab="T7:\; 4 jets"

print ("Type=",Tlab, " number=",trig_type) 
print ("Xmin=",Xmin) 


############# Configs ##############
nameX="m [GeV]"
MLabel="jj"
NoBottom=False
NoLeft=False
nameY="Events / GeV"
namdYL="Sign."

## defaults 
#name="Mjj_data_LECR"
name="Mjj"


Position="1x3" #position 
# 1st column...
# channel by channel 
if channel == "jj":
  Position="1x1" #position 
  MLabel="j+j"
  name="Mjj_data100percent"  

# channel by channel 
if channel == "je":
  Position="1x2" #position 
  MLabel="j + e"
  name="Mje_data100percent" 

# channel by channel 
if channel == "jm":
  Position="1x3" #position 
  MLabel="j + \mu"
  name="Mjm_data100percent" 

# 2nd colums..################################
if channel == "jb":
  Position="2x1" #position 
  MLabel="j + b\mbox{-}jet"
  name="Mjb_data100percent"

# channel by channel 
if channel == "be":
  Position="2x2" #position 
  MLabel="b\mbox{-}jet + e"
  name="Mbe_data100percent"

# channel by channel 
if channel == "bm":
  Position="2x3" #position 
  MLabel="b\mbox{-}jet + \mu"
  name="Mbm_data100percent"

## 3rd column
if channel == "bb":
  Position="3x1" #position 
  MLabel="2 b\mbox{-}jet"
  name="Mbb_data100percent"

if channel == "jg":
  Position="3x2" #position 
  MLabel="j + \gamma"
  name="Mjg_data100percent"

if channel == "bg":
  Position="3x3" #position 
  MLabel="b\mbox{-}jet + \gamma"
  name="Mbg_data100percent"


if (Position =="1x1"):
    NoBottom=True
    NoLeft=False
    nameX=""

if (Position =="1x2"):
    NoBottom=True
    NoLeft=True 
    nameX=""

if (Position =="1x3"):
    NoBottom=True
    NoLeft=True
 
if (Position =="2x1"):
    NoBottom=True
    NoLeft=True 
    nameX=""
    nameY=""
    namdYL=""

if (Position =="2x2"):
    NoBottom=True
    NoLeft=True
    nameX=""
    nameY=""
    namdYL=""

if (Position =="2x3"):
    NoBottom=True
    NoLeft=True
    nameY=""
    namdYL=""

if (Position =="3x1"):
    NoBottom=True
    NoLeft=True
    nameX=""
    nameY=""
    namdYL=""

if (Position =="3x2"):
    NoBottom=True
    NoLeft=True 
    nameX=""
    nameY=""
    namdYL=""

if (Position =="3x3"):
    NoBottom=False
    NoLeft=True 
    nameY=""
    namdYL=""

print("Position:",Position)
print("No bottom:",NoBottom)
print("No left:",NoLeft)

##################################

# overwite to make it correct 
NoBottom=False
NoLeft=False


## no bottom
if (NoBottom):
    nameX="";


# import atlas styles
from AtlasStyle import *
from AtlasUtils import *
from initialize  import *
from global_module import *
from functions import *
from module_functions import *

gROOT.Reset()
figdir="figs/"
name=os.path.basename(__file__)
name=name.replace(".py","")
figname=name+"_"+str(trig_type)+"_"+channel
epsfig=figdir+figname+".eps"

YRMIN=-8.999 
YRMAX=8.999 

######################################################
gROOT.SetStyle("Plain");


xwin=600
ywin=600
#if (NoLeft==True): xwin=600-int(600*0.13) 
#if (NoBottom==True): ywin=600-int(600*0.1)

c1=TCanvas("c","Mass",10,10,xwin,ywin);

c1.SetFrameBorderMode(0);
ps1 = TPostScript( epsfig,113)
#c1.Divide(1,1,0.0,0.0);
c1.SetTickx()
c1.SetTicky()
c1.SetTitle("")
c1.SetLineWidth(3)
c1.SetBottomMargin(0.1)
#if (NoBottom): c1.SetBottomMargin(0.01)
c1.SetTopMargin(0.0)
c1.SetRightMargin(0.01)
c1.SetFillColor(0)

#### the main plot
pad1 = TPad("pad1","pad1",0,0.3,1,0.97)
pad1.SetBottomMargin(0.0)
pad1.SetLeftMargin(0.14)
if (NoLeft==True): pad1.SetLeftMargin(0.0)

pad1.SetRightMargin(0.01)
pad1.SetTopMargin(0.0)
pad1.Draw()
pad1.cd()
pad1.SetLogy(1)
pad1.SetLogx(1)
#if (NoLeft):
#    ch=drawXAxis(1,pad1,Xmin, Ymin, Xmax, Ymax,nameX,nameY,showXAxis=False, showYAxis=False)
#else:
#   ch=drawXAxis(1,pad1,Xmin, Ymin, Xmax, Ymax,nameX,nameY,showXAxis=False, showYAxis=True)

ch=drawXAxis(1,pad1,Xmin, Ymin, Xmax, Ymax,nameX,nameY,showXAxis=True, showYAxis=True)

ax=ch.GetXaxis();
ay=ch.GetYaxis();
ay.SetTitleOffset(0.7)
ay.SetLabelSize(0.07)
ay.SetTitleSize(0.09)
gPad.SetTickx()
gPad.SetTicky()


ax.Draw("same")
if (NoLeft): ay.Draw("same")

# get bin width
xfile="../analysis/out/t1/sys0/data/data_2015_2016_2017_2018.root"
xfile=TFile( xfile )
bins=getBinSize(xfile)

systematics=0
hall,hjets,ttbar, wzjets, diboson, photons=StandardModelPrediction(systematics, trig_type, histo_name="M"+channel, massbins=None)
mcprediction=[hall,hjets,ttbar, wzjets, diboson, photons]
if (hall == None):
            print("No data")
            sys.exit()

# hall=hjets.Clone()

# divide by bin width
# correct if errors are unnatural
countingErrorsCorrect( hall )
final_BINS=hall.Clone()
#smooth histogram 
#hall.SetDirectory(0)
#final_BINS=smoothTH1( hall, bins, 15, 3 )

# divide by bins
final_BINS.Divide(bins)



final_BINS.SetAxisRange(Ymin, Ymax,"y");
final_BINS.SetAxisRange(Xmin, Xmax,"x");
final_BINS.Draw("same pe ][")

hjets.Divide(bins)
hjets.SetAxisRange(Xmin, Xmax,"x");

ttbar.Divide(bins)
ttbar.SetAxisRange(Xmin, Xmax,"x");

wzjets.Divide(bins)
wzjets.SetAxisRange(Xmin, Xmax,"x");

diboson.Divide(bins)
diboson.SetAxisRange(Xmin, Xmax,"x");

photons.Divide(bins)
photons.SetAxisRange(Xmin, Xmax,"x");


ttbar.Draw("same  ][")
hjets.Draw("same  ][")
wzjets.Draw("same ][")
diboson.Draw("same  ][")
photons.Draw("same  ][")



# fit ranges
MyMinX=Xmin
MyMaxX=Xmax


MyMaxX=getMaxNonzero(hall,ycut=0.5)
if (MyMaxX>Xmax): MyMaxX=Xmax
print("Last non-zero=",MyMaxX)
#if (channel=="bm" and trig_type==2):
#            MyMinX=600 

back=TF1("back",FiveParam2015(),MyMinX,MyMaxX,5);

# correct channel
print(channel,trig_type)
if (channel=="bm" and trig_type=="2"):
         back=TF1("back",SixParam2015(),MyMinX,MyMaxX,6);
#if (channel=="jb" and trig_type=="2"):
#         back=TF1("back",SixParam2015(),MyMinX,MyMaxX,6);

back=style5par(back)

back.SetParameter(0, 2.39202e+09)
back.SetParameter(1, 3.47948e+01)
back.SetParameter(2, 1.33956e+01)

# https://root.cern.ch/root/html/ROOT__Fit__FitResult.html
import random
nn=0
chi2min=10000000
parbest=[]
for i in range(200):
     fitr=final_BINS.Fit(back,"SMR0")
     print ("Status=",int(fitr), " is valid=",fitr.IsValid(), " chi2/ndf=",back.GetChisquare()/back.GetNDF() )
     if (fitr.IsValid()==True):
            chi2=back.GetChisquare()/back.GetNDF()
            if chi2<chi2min:
                    chi2min=chi2;
                    if nn>4:
                           break;
                    nn=nn+1
            if (chi2<5):
                 chi2min=chi2;
                 ppp = back.GetParameters()
                 back.SetParameter(0,ppp[0] + 0.2*ppp[0]*random.randint(-1,1 ))
                 back.SetParameter(1,ppp[1] + 0.2*ppp[1]*random.randint(-1,1 ))
                 continue
            if chi2>=chi2min:
                 back.SetParameter(0,random.randint(0,100000))
                 back.SetParameter(1,random.randint(0,100))
                 back.SetParameter(2,random.randint(-2,2))
                 continue
            back.SetParameter(0,random.randint(0,100000))
            back.SetParameter(1,random.randint(0,100))
            back.SetParameter(2,random.randint(-2,2))

fitr.Print()
print ("Is valid=",fitr.IsValid(), " chi2/ndf=",back.GetChisquare()/back.GetNDF() )
#print "Histogram Normalization=", hh.Integral()

par = back.GetParameters()
back.Draw("same")
chi2= back.GetChisquare()
ndf=back.GetNDF()
print ("Chi2=", chi2," ndf=",ndf, " chi2/ndf=",chi2/ndf) 

leg2=TLegend(0.65, 0.45, 0.89, 0.96);
leg2.SetBorderSize(0);
leg2.SetTextFont(42);
leg2.SetFillColor(10);
leg2.SetTextSize(0.06);
if (Position == "1x1"): 

          for k in mcprediction:
               leg2.AddEntry(k,k.GetTitle(),"pl")

          #leg2.AddEntry(final_BINS,"Data","pl")
          leg2.AddEntry(back,"Background fit","l")
leg2.AddEntry(back,"#chi^{2}/ndf="+"{0:.2f}".format(chi2/ndf),"")
leg2.Draw("same");



if (Position =="1x1"):
  ATLASLabel6(0.19,0.89,0.21,0.07)
  #myText(0.19,0.81,1,0.07,UsedData0)
  myText(0.19,0.81,1,0.08,Tlab)
  myTextIt(0.2,0.12,1,0.14,MLabel)
else:
  myTextIt(0.2,0.8,1,0.14,MLabel)

#myText(0.75,0.4,1,0.04,"pp #sqrt{s}=13 TeV")
# myText(0.75,0.61,1,0.04,intLUMI)


gPad.RedrawAxis()
c1.Update()

#### ratio
c1.cd()
pad2 = TPad("pad2","pad2",0,0.01,1,0.3)

pad2.SetLeftMargin(0.14)
#if (NoLeft): pad2.SetLeftMargin(0.0)

pad2.SetRightMargin(0.01)
pad2.SetTopMargin(0)
pad2.SetBottomMargin(0.35)
#if (NoBottom):
#      pad2.SetBottomMargin(0.003)
pad2.Draw()
pad2.cd()
pad2.SetLogy(0)
pad2.SetLogx(1)

"""
if (NoLeft and NoBottom):
   hb=drawXAxis(2,pad2,Xmin, YRMIN, Xmax, YRMAX,nameX,nameY,False,False)

if (NoLeft and NoBottom==True):
   hb=drawXAxis(2,pad2,Xmin, YRMIN, Xmax, YRMAX,nameX,nameY,True,False)

if (NoLeft==False and NoBottom ==True):
   hb=drawXAxis(2,pad2,Xmin, YRMIN, Xmax, YRMAX,nameX,nameY,True,False)

if (NoLeft==True and NoBottom ==False):
   hb=drawXAxis(2,pad2,Xmin, YRMIN, Xmax, YRMAX,nameX,nameY,False,True)
"""

# if (NoLeft==False and NoBottom == False):
hb=drawXAxis(2,pad2,Xmin, YRMIN, Xmax, YRMAX,nameX,nameY,True,True)


hb.GetXaxis().SetTickLength(0.05)
#ax.SetLabelSize(0.04)
#ax.SetTitleSize(0.05)
hb.GetXaxis().SetNdivisions(20,5,1, ROOT.kTRUE);
hb.GetXaxis().SetMoreLogLabels(ROOT.kTRUE)


hb.GetXaxis().SetTitleOffset(0.7)
hb.GetXaxis().SetLabelSize(0.17)
hb.GetXaxis().SetTitleSize(0.2)
hb.GetXaxis().SetTitle( nameX );

hb.GetYaxis().SetLabelOffset(0.02)
hb.GetYaxis().SetLabelSize(0.16)


# BG
DiffBG=TGraphErrors()
DiffBG.SetMarkerColor( ROOT.kRed )
DiffBG.SetMarkerStyle( 20 )
DiffBG.SetMarkerSize( 0.7 )

Sig=bins.Clone();
Sig.SetFillColor(2);
# Sig.SetFillStyle(True);
Sig.SetLineColor(2);

res=TH1D("Res","Res",100,-10,10);

n=0
for i in range(1, final_BINS.GetNbinsX()):
  Sig.SetBinContent(i,0)
  Sig.SetBinError(i,0)
  if (final_BINS.GetBinCenter(i)>MyMinX and final_BINS.GetBinCenter(i)<MyMaxX and final_BINS.GetBinContent(i)>0):
     center=final_BINS.GetBinCenter(i)
     x=final_BINS.GetBinCenter(i)
     bwidth=bins.GetBinCenter(i)
     D = final_BINS.GetBinContent(i) * bwidth;
     Derr = final_BINS.GetBinError(i) * bwidth; 
     B = back.Eval(center)* bwidth;
     Berr = sqrt( B ); # counting experiment 
     frac=0
     fracErr = 0.;
     if B != 0:
        frac = (D-B)/sqrt(Berr *  Berr + Derr*Derr) 
     
     res.Fill(frac)

     Sig.SetBinContent(i,frac)
     Sig.SetBinError(i,fracErr)

     DiffBG.SetPoint(n,x,frac)
     # DiffBG.SetPointError(n,fracErr,fracErr)
     # y=10*hh.GetBinContent(i) / back.Eval(center) 
     # ey=hh.GetBinError(i)/ back.Eval(center)
     #x=hh.GetBinCenter(i)
     #ex=0 # h1.GetBinWidth(i)/2.0
     #DiffBG.SetPoint(n,x,y)
     #DiffBG.SetPointError(n,ex,ey)
     n=n+1


# draw line
x1=c1.XtoPad(Xmin)
x2=c1.XtoPad(Xmax)
ar5=TLine(x1,0,x2,0);
ar5.SetLineWidth(2)
ar5.SetLineStyle(2)
ar5.Draw("same")

if (NoLeft==False):
  l1=TLatex()
  l1.SetTextAngle(90)
  l1.SetTextSize(0.21);
  l1.SetNDC();
  l1.SetTextColor(1);
  l1.DrawLatex(0.05,0.4,namdYL);


Sig.Draw("same histo ][")
# DiffBG.Draw("p same")
print (epsfig) 
ps1.Close()

sfigs=figdir+figname+"_residuals.eps"
showResiduals(final_BINS,back, sfigs);


if (myinput != "-b"):
              if (raw_input("Press any key to exit") != "-9999"): 
                         c1.Close(); sys.exit(1);


