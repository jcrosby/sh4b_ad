import random
import sys
sys.path.append("modules/")

# import atlas styles
from AtlasStyle import *
from AtlasUtils import *
from array import *
from math import *
from ROOT import TH1D,TF1,TProfile2D,TEllipse, THStack,TRandom3,TFile,TLatex,TLegend,TPaveText,TGraphErrors,kRed,kBlue,kGreen,kCyan,kAzure,kYellow,kTRUE
import math,sys,os 
import shapiro
import ROOT
from array import array
from decimal import Decimal
import numpy
import random
import sys,zipfile,json,math


CMS=13000.0


lumi2015=3210. # error 3.21 +-0.07 
Lumi2015=" %.1f fb^{-1}" % (lumi2015/1000.)
intLUMI2015="#int L dt = "+Lumi2015

lumi2016=33651.0 # error 3.21 +-0.07 
Lumi2016=" %.1f fb^{-1}" % (lumi2016/1000.)
intLUMI2016="#int L dt = "+Lumi2016

lumi2017=43813.7 # error 3.21 +-0.07 
Lumi2017=" %.1f fb^{-1}" % (lumi2017/1000.)
intLUMI2017="#int L dt = "+Lumi2017

# Nov
# lumi2018=43003.7 # error 3.21 +-0.07 
# Dec
lumi2018=58450.1 # error 3.21 +-0.07 
Lumi2018=" %.1f fb^{-1}" % (lumi2018/1000.)
intLUMI2018="#int L dt = "+Lumi2018

# lumi  in pb
lumi2015_2018=lumi2015+lumi2016+lumi2017+lumi2018
lumi=lumi2015_2018 # take into account missing files 

Lumi=" %.0f fb^{-1}" % (lumi/1000.)
intLUMI="#int L dt = "+Lumi


Lumi10=" %.0f fb^{-1}" % (0.1*lumi/1000.)
intLUMI10="#int L dt = "+Lumi10


print "Lumi=",lumi


# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/XsecSummaryWjetsPowPy8Incl
# 361100-361104
# 2.65 + 2.65  + 4.83 + 2.41
powheg_lumi=(2.65+2.65+4.83+2.41)*1000  # pb-1
powheg_kfactor=1.0172
powheg_scale=(lumi/powheg_lumi)*powheg_kfactor

# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XsecSummaryTTbar
ttbar_cross=729.0 # pb
# 97% grid efficiency
ttbar_events=49874000*0.97
# ttbar_lumi=ttbar_file.Get("cutflow").GetBinContent(1) /(ttbar_cross*0.543) # pb
ttbar_lumi=ttbar_events /(ttbar_cross*0.543) # pb
ttbar_kfactor=1.195
ttbar_scale=(lumi/ttbar_lumi)*ttbar_kfactor


# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XsecSummarySingleTop
# Run 410659 and  410647 
stop_cross=22.175+36.996 # pb
# stop_events=5968000+6226000+6226000+6226000.. Rough number of input 
stop_events=9968000*6
# ttbar_lumi=ttbar_file.Get("cutflow").GetBinContent(1) /(ttbar_cross*0.543) # pb
stop_lumi=stop_events /stop_cross # pb
stop_kfactor=1.195
stop_scale=(lumi/stop_lumi)*stop_kfactor


# Dijet QCD
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/JetEtmissMC15
# in nb-1
pythia_lumi={}

# events/cros*eff 
# in nb
factors={}
factors[0]=2000000 / (7.8420E+07*1.0240E+0)
factors[1]=2000000 / (7.8420E+07*6.7198E-04)
factors[2]=1992000  / (2.4334E+06*3.3264E-04)
factors[3]=1767000  / (2.6454E+04*3.1953E-04)
factors[4]=1997000 /  (2.5464E+02*5.3009E-04)
factors[5]=1995000  / (4.5536E+00*9.2325E-04)
factors[6]=1997000 / (2.5752E-01*9.4016E-04)
factors[7]=1990000  / (1.6214E-02*3.9282E-04)
factors[8]=2000000 / (6.2505E-04*1.0162E-02)
factors[9]=2000000 / (1.9640E-05*1.2054E-02)
factors[10]=2000000 / (1.1961E-06*5.8935E-03)



DataLab="Data #sqrt{s}=13 TeV"
KinemCuts="E_{T}^{jet}>410 GeV  |#eta^{#gamma}|<2.5";
ATLASprel="ATLAS internal"
mcBkg="PYTHIA8"
mcSig="PYTHIA t#bar{t}"
mcPowheg="W+jet POWHEG"
mcPowhegTTbar="t#bar{t} POWHEG"
mcSTop="s-top POWHEG"
mcBkgHrw="HERWIG++ QCD"
mcBkgWJ="Multijets PYTHIA8"

DataLab2015="Data 2015 #sqrt{s}=13 TeV"
DataLab2016="Data 2016 #sqrt{s}=13 TeV"
DataLab2017="Data 2017 #sqrt{s}=13 TeV"
DataLab2018="Data 2018 #sqrt{s}=13 TeV"


mjjBinsL = [99,112,125,138,151,164,177,190, 203, 216, 229, 243, 257, 272, 287, 303, 319, 335, 352, 369, 387, 405, 424, 443, 462, 482, 502, 523, 544, 566, 588, 611, 634, 657, 681, 705, 730, 755, 781, 807, 834, 861, 889, 917, 946, 976, 1006, 1037, 1068, 1100, 1133, 1166, 1200, 1234, 1269, 1305, 1341, 1378, 1416, 1454, 1493, 1533, 1573, 1614, 1656, 1698, 1741, 1785, 1830, 1875, 1921, 1968, 2016, 2065, 2114, 2164, 2215, 2267, 2320, 2374, 2429, 2485, 2542, 2600, 2659, 2719, 2780, 2842, 2905, 2969, 3034, 3100, 3167, 3235, 3305, 3376, 3448, 3521, 3596, 3672, 3749, 3827, 3907, 3988, 4070, 4154, 4239, 4326, 4414, 4504, 4595, 4688, 4782, 4878, 4975, 5074, 5175, 5277, 5381, 5487, 5595, 5705, 5817, 5931, 6047, 6165, 6285, 6407, 6531, 6658, 6787, 6918, 7052, 7188, 7326, 7467, 7610, 7756, 7904, 8055, 8208, 8364, 8523, 8685, 8850, 9019, 9191, 9366, 9544, 9726, 9911, 10100, 10292, 10488, 10688, 10892, 11100, 11312, 11528, 11748, 11972, 12200, 12432, 12669, 12910, 13156];

mjjBins = array("d", mjjBinsL)



### Read configuration of the RMM matrix
### This is common for all
with open('data/config.json') as json_file:
    data = json.load(json_file)
    maxNumber=int(data['maxNumber'])
    maxTypes=int(data['maxTypes'])
    mSize=int(data['mSize'])
print "maxNumber=",maxNumber," maxTypes=",maxTypes," mSize=",mSize
mSize=maxTypes*maxNumber+1;

# dijet invariant mass
x=1+0*maxNumber+1  # X position  
y=1+0*maxNumber    # Y position 
mjj=(x,y) #  index of Mjj  matrix ellement 

# PT of first jet
x=1+0*maxNumber  # X position  
y=1+0*maxNumber  # Y position 
pT=(x,y) #  index of Mjj  matrix ellement 


#  bb mass 
x=1+1*maxNumber+1
y=1+1*maxNumber
mbb=(x,y)

#  bj mass 
x=1+1*maxNumber+1
y=1+0*maxNumber
mbj=(x,y) 

# e+e- 
x=1+2*maxNumber+1
y=1+2*maxNumber
mee=(x,y)

# mu+mu 
x=1+3*maxNumber+1
y=1+3*maxNumber
mmumu=(x,y)


### This list contains excluded values for Z-score calculation
### We excluding pT of leading jet, Mjj and mbb
# excluded_val= ( pT, mjj, mbb)
excluded_val= (mjj, mbb, mbj)
print "Excluded cells=",excluded_val  

#### Exclusion values for RMM matrix #############
###################################


# dijet invariant mass
x=2 # X position  
y=1 # Y position 
inx1=x*mSize+y; #  index of hxw matrix ellement 

# pT1 
x=1 # X position  
y=1 # Y position 
inx2=x*mSize+y; #  index of hxw matrix ellement 

# Mjj for for light-jet + b-jets
x=1+maxNumber # X position  
y=1 # Y position 
inx3=x*mSize+y; #  index of hxw matrix ellement 

# pT1 for for b-jets
x=1+maxNumber # X position  
y=1+maxNumber # Y position 
inx4=x*mSize+y; #  index # pT for for b-jets

# Mjj for for 2-b jets 
x=2+maxNumber # X position  
y=1+maxNumber # Y position 
inx5=x*mSize+y; #  index of hxw matrix ellement 


# exlusion matrix for RMM in terms of indexes (how it is packed) 
excluded=(inx1,inx2,inx3,inx4,inx5)

## draw axis
def drawXAxis(sf,gPad,XMIN,YMIN,XMAX,YMAX,nameX,nameY):
 h=gPad.DrawFrame(XMIN,YMIN,XMAX,YMAX);
 ay=h.GetYaxis();
 ay.SetLabelFont(42)

 if (sf==1):
             ay.SetLabelSize(0.05)
             ay.SetTitleSize(0.06)

 if (sf==2 or sf==3):
             ay.SetLabelSize(0.10)
             ay.SetTitleSize(0.3)
 if (sf==20):
             ay.SetLabelSize(0.18)
 if (sf==30):
             ay.SetLabelSize(0.12)

# ay.SetTitleSize(0.1)
 ay.SetNdivisions(505);
 if (sf==1): ay.SetTitle( nameY )
 # ay.Draw("same")
 ax=h.GetXaxis();
 if (sf==1 or sf==2): ax.SetTitle( nameX );
 if (sf==30): ax.SetTitle( nameX );
 ax.SetTitleOffset(1.18)
 ay.SetTitleOffset(0.8)

 ax.SetLabelFont(42)
 # ax.SetTitleFont(42)
 ay.SetLabelFont(42)
 # ay.SetTitleFont(42)
 ax.SetLabelSize(0.12)
 ax.SetTitleSize(0.14)
 #ay.SetTitleSize(0.14)
 if (sf==30):
          ax.SetLabelSize(0.12)
          ax.SetTitleSize(0.12)
 if (sf==2 or sf==3):
             ay.SetLabelSize(0.12)
             ay.SetTitleSize(0.2)

 ax.Draw("same");
 ay.Draw("same");
 return h


# get full predictions
# inpput: histogram name
# bins for histograms
# returns: total prediction and components
def getStandardModel(xfile, name,inLabel,bins=None):
  global ttbar_scale, powheg_scale, stop_scale

  # xfile.ls()
  ttot=xfile.Get(name+"_"+inLabel[0])
  ptot=xfile.Get(name+"_"+inLabel[1])
  stop=xfile.Get(name+"_"+inLabel[2])

  ttot.Scale(ttbar_scale)
  ptot.Scale(powheg_scale)
  stop.Scale(stop_scale)

  ptot.SetTitle("")
  ptot.SetStats(0)
  ptot.SetLineWidth(2)
  ptot.SetLineColor(1 )
  ptot.SetMarkerColor( 1 )
  ptot.SetFillColor(42);

  ttot.SetTitle("")
  ttot.SetStats(0)
  ttot.SetLineWidth(2)
  ttot.SetLineColor(1 )
  ttot.SetMarkerColor( 1 )
  ttot.SetFillColor(35);

  stop.SetTitle("")
  stop.SetStats(0)
  stop.SetLineWidth(2)
  stop.SetLineColor(1 )
  stop.SetMarkerColor( 1 )
  stop.SetFillColor(32);

  nameX=name+"_qcd_jz2"
  mqcd=(xfile.Get(nameX)).Clone()
  for i in range(3,12):
       nameX=name+"_qcd_jz"+str(i)  
       fqcd=xfile.Get(nameX)
       mqcd.Add( fqcd )


  MCTOT=ttot.Clone()

  MCTOT.SetLineColor(2)
  MCTOT.Add(ptot)
  MCTOT.Add(stop)
  # No QCD
  # MCTOT.Add(mqcd)

  # divide by bins if needed
  if bins is not None: MCTOT.Divide(bins)
  if bins is not None: mqcd.Divide(bins)
  if bins is not None: ptot.Divide(bins)
  if bins is not None: ttot.Divide(bins)


  return MCTOT,mqcd,ptot,ttot,stop 


# residual plots: input histoogram, function, file name 
# http://sdittami.altervista.org/shapirotest/ShapiroTest.html
from module_functions import  Gauss
def showResiduals(hh,func,fname, MyMinX=-12,  MyMaxX=12, isKS=True):
   print "showResiduals: Calculate residuals."
   MyBins=100
   res=TH1D("Residuals","Residuals",MyBins,MyMinX,MyMaxX);
   res.SetTitle("")
   res.SetStats(1)
   res.SetLineWidth(2)
   res.SetMarkerColor( 1 )
   res.SetMarkerStyle( 20 )
   res.SetMarkerSize( 0.8 )
   res.SetFillColor(42)
   nameX="D_{i} - F_{i} / #Delta D_{i}"
   nameY="Entries"
   FitMin=func.GetXmin()
   FitMax=func.GetXmax()
   print "Fit min=",FitMin,"  max=",FitMax
   nres=0.0
   residuals=[]
   for i in range(1,hh.GetNbinsX()):
     center=hh.GetBinCenter(i)
     if (hh.GetBinContent(i)>0 and center>FitMin and center<FitMax):
       center=hh.GetBinCenter(i)
       x=hh.GetBinCenter(i)
       D = hh.GetBinContent(i);
       Derr = hh.GetBinError(i);
       B = func.Eval(center);
       frac=0
       if Derr>0:
          frac = (D-B)/Derr
       residuals.append(frac)
       res.Fill(frac)
       nres=nres+1.0
   res.SetStats(1)
   back=TF1("back",Gauss(),MyMinX,MyMaxX,3);
   back.SetNpx(200); back.SetLineColor(4); back.SetLineStyle(1)
   back.SetLineWidth(2)
   back.SetParameter(0,10)
   back.SetParameter(1,0)
   back.SetParameter(2,1.0)
   back.SetParLimits(2,0.1,1000)
   #back.SetParLimits(0,0.01,10000000)
   #back.SetParLimits(1,-5.0,5.0)
   #back.SetParLimits(2,0.0,5.0)

   #back.FixParameter(1,0)
   #back.FixParameter(2,1.0)


   nn=0
   chi2min=10000
   parbest=[]
   for i in range(10):
     fitr=res.Fit(back,"SMR0")
     print "Status=",int(fitr), " is valid=",fitr.IsValid()
     if (fitr.IsValid()==True):
             chi2=back.GetChisquare()/back.GetNDF()
             if chi2<chi2min:
                    nn=nn+1
                    if nn>3:
                           break;
                    back.SetParameter(0,random.randint(0,10))
                    back.SetParameter(1,random.randint(-1,1))
                    back.SetParameter(2,random.randint(0,2.0))
                    par = back.GetParameters()

   #fitr=res.Fit(back,"SMR0")
   fitr.Print()
   print "Is valid=",fitr.IsValid()

   par = back.GetParameters()
   err=back.GetParErrors()
   chi2= back.GetChisquare()
   ndf=back.GetNDF()
   print "Chi2=", chi2," ndf=",ndf, " chi2/ndf=",chi2/ndf
   prob=fitr.Prob();
   print "Chi2 Probability=",fitr.Prob();

   # make reference for normal
   norm_mean=0
   norm_width=1
   normal=TH1D("Normal with sig=1","Reference normal",MyBins,MyMinX,MyMaxX);
   normal.SetLineWidth(3)
   normal.SetLineColor(2)
   # normal.SetFillColor( 5 )

   maxe=5000
   r = TRandom3()
   for i in range (maxe) :
          xA = r.Gaus(norm_mean, norm_width)
          normal.Fill(xA)
   norM=nres/maxe
   normal.Scale(norM)
   pKSbinned = res.KolmogorovTest(normal)
   KSprob="KS prob ="+"{0:.2f}".format(pKSbinned)
   if (isKS): print KSprob

   shapiro_prob=shapiro.ShapiroWilkW(residuals)
   Shapiro="ShapiroWilk ={0:.2f}".format(shapiro_prob)
   print Shapiro

   gROOT.SetStyle("ATLAS");
   gStyle.SetOptStat(220002210);
   gStyle.SetStatW(0.32)
   c2=TCanvas("c","BPRE",10,10,600,540);
   c2.Divide(1,1,0.008,0.007);
   c2.SetBottomMargin(0.1)
   c2.SetTopMargin(0.05)
   c2.SetRightMargin(0.02)
   c2.SetLeftMargin(0.10)

   binmax = normal.GetMaximumBin();
   Ymax=normal.GetBinContent(normal.FindBin(0));
   for i in xrange(1,res.GetNbinsX()):
      if res.GetBinContent(i)>Ymax: Ymax=res.GetBinContent(i);
   Ymax=1.15*Ymax;

   #h=gPad.DrawFrame(MyMinX,0,MyMaxX,Ymax)

   ps2 = TPostScript( fname,113)

   res.SetStats(1)
   gStyle.SetOptStat(220002200);
   gStyle.SetStatW(0.32)

   res.SetAxisRange(0, Ymax,"y");
   res.Draw("histo")
   back.Draw("same")
   if (isKS): normal.Draw("histo same")
   leg2=TLegend(0.11, 0.6, 0.39, 0.90);
   leg2.SetBorderSize(0);
   leg2.SetTextFont(62);
   leg2.SetFillColor(10);
   leg2.SetTextSize(0.04);
   leg2.AddEntry(res,"Residuals","f")
   leg2.AddEntry(back,"Gauss fit","l")
   mean= "mean="+"{0:.2f}".format(par[1])
   mean_err= "#pm "+"{0:.2f}".format(err[1])
   sig= "#sigma="+"{0:.2f}".format(par[2])
   sig_err= "#pm "+"{0:.2f}".format(err[2])
   leg2.AddEntry(back,mean+mean_err,"")
   leg2.AddEntry(back,sig+sig_err,"")
   leg2.AddEntry(back,"#chi^{2}/ndf="+"{0:.2f}".format(chi2/ndf)+"(p="+"{0:.2f})".format(prob),"")
   leg2.AddEntry(back,Shapiro,"")
   if (isKS): leg2.AddEntry(normal,"Normal (#sigma=1)","l")
   if (isKS): leg2.AddEntry(back,KSprob,"")

   leg2.Draw("same");
   ax=res.GetXaxis();
   ax.SetTitle( nameX );
   ay=res.GetYaxis();
   ay.SetTitle( nameY );
   ax.SetTitleOffset(1.0); ay.SetTitleOffset(1.0)
   ax.Draw("same")
   ay.Draw("same")
   gPad.RedrawAxis()
   c2.Update()
   ps2.Close()
   c2.Close();
   print fname, " done"


def style3par(back):
     back.SetNpx(100); back.SetLineColor(4); back.SetLineStyle(1)
     back.SetLineWidth(2)
     back.SetParameter(0,4.61489e-02)
     back.SetParameter(1,1.23190e+01)
     back.SetParameter(2,3.65204e+00)

     #back.SetParameter(3,-6.81801e-01)
     #back.SetParLimits(0,0,100)
     # back.SetParLimits(1,0,12)
     #back.SetParLimits(2,-100,100)
     return back

def style5par(back):
     back.SetNpx(200); back.SetLineColor(4); back.SetLineStyle(1)
     back.SetLineWidth(2)
     back.SetParameter(0,24)
     back.SetParameter(1,1.33731e+01)
     back.SetParameter(2,-6.87060e-01)
     back.SetParameter(3,-1.31)
     back.SetParameter(4,-0.17)
     # back.SetParLimits(0,0,10000)
     back.SetParLimits(1,0,10000)
     back.SetParLimits(2,-100,100)
     back.SetParLimits(2,-20,20)
     back.SetParLimits(3,-10,10)
     return back

