## The Autoencoder

Currently Tensorflow with Keras backend is used to train the autoencoder. 

There are scripts in here that can either train a normal AE or a Probabilistic Autoencoder (PAE)
Currently we will use the normal AE to make it easy though I do encourage to try the PAE. Results could be interesting.

The only adjustment needed for the PAE is the loss function, currently fairly unstable and needs to be rewritten to match the loss of the AE

### Training

The config file to adjust AE script is found under `/pae/pae_config`

When training the AE, the main line in this config is:

```
autoencoder_architecture: [160,120,80,40,20,10]
```

To run the AE, use the shell macro `A_RUN`. In this macro, change data location and comments to your needs.

## Analyzing Data using AE Model

The main script to use is `analysis_root_chunky.py`. This output root files into the folder `/root`. Change this script w.r.t. your needs and interests in the analysis!
A few scripts are currently being worked on are:
```
loss_data1percent_allYears.py
loss_data1percent_BSM.py
```
