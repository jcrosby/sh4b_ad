# Github and documentation can be found here:  https://github.com/VMBoehm/PAE/blob/master/TrainNVP_simplified_and_explained.ipynb

# The bijector of the normalizing flow in this script is made out of transformation layers and permutation layers. 
# The tranformations can either be a shift and rescale transformation (RealNVP) or a spline transformation (Neural Spline Flow). 
# Every second permutation layer is trainable (GLOW).


#  https://github.com/LukasRinder/normalizing-flows/blob/master/experiments/real-nvp/real_nvp_uci.ipynb

import os,sys
sys.path.append("modules/")
from global_module import *
import shutil
import time
####*IMPORANT*: Have to do this line *before* importing tensorflow
os.environ['PYTHONHASHSEED']=str(1)

from pae.bijector_init import get_prior,init_once
print ('Number of arguments:', len(sys.argv), 'arguments.')
print ('Argument List:', str(sys.argv))
n = len(sys.argv)
if (n != 3):
      print ("No arguments!. Need at least 1 model and 1 input file in csv.zip")
      sys.exit()
inputData=sys.argv[2]

# Model name to save
from pathlib import Path

tail = Path(inputData).name+"_NF" 
modelName="./models/"+tail.replace(".csv.gz","")
figsDir ="./figs/"+tail.replace(".csv.gz","")
aetail = Path(inputData).name
aeName = "./models/"+aetail.replace(".csv.gz","")
print("Train model = ",modelName) 
print("Figures in = ", figsDir )

if (os.path.exists(figsDir)):
    shutil.rmtree(figsDir,ignore_errors=True)

if not os.path.exists(figsDir):
    os.makedirs(figsDir)

if (os.path.exists("models/data1percent_NF.log")):
    os.remove( "models/data1percent_NF.log" )
""" 
# Start log file
log = open(modelName+".log", "a")
sys.stdout = log

 """
# Data Preprocessing
import yaml
import pandas
import matplotlib
import seaborn
import tensorflow
import tensorflow as tf
import tensorflow.compat.v1 as tf1
tf1.compat.v1.enable_eager_execution()

import tensorflow_probability as tfp
#import tensorflow_hub as hub
tfd = tfp.distributions
tfb = tfp.bijectors

from pae.flow_class import RealNVP, SplineParams,Made
from pae.train_utils import train_density_estimation, nll,sanity_check
from pae.bijector_init import get_nvp, get_prior, nvp_module_spec

#from pae.visu_density import plot_heatmap_2d, plot_samples_2d
########## 
import pickle
print('Numpy version      :' , numpy.__version__)
print('Pandas version     :' ,pandas.__version__)
print('Matplotlib version :' ,matplotlib.__version__)
print('Seaborn version    :' , seaborn.__version__)
print('Tensorflow version :' , tensorflow.__version__)
# print('Keras version      :' , keras.__version__)
print("-----------------------------------")
with open('pae/pae_config.yaml','r') as file:
    config = yaml.safe_load(file)
print("Using config file: pae_config.yaml")

import numpy as np
import pandas as pd
pd.set_option('display.max_columns', None)
pd.set_option('display.max_row', None)
import matplotlib.pyplot as plt
plt.rcdefaults()
from pylab import rcParams
import seaborn as sns
import datetime
import matplotlib
matplotlib.use('Agg') # set the backend before importing pyplo. Fix Invalid DISPLAY variable 
from matplotlib import pyplot as plt
####### Deep learning libraries
from tensorflow import keras
from keras.models import Model, load_model
from keras.layers import Input, Dense
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.callbacks import EarlyStopping
from sklearn.preprocessing import  StandardScaler, MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import (confusion_matrix, classification_report, accuracy_score, roc_auc_score, auc,
                             precision_score, recall_score, roc_curve, precision_recall_curve,
                             precision_recall_fscore_support, f1_score,
                             precision_recall_fscore_support)


print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))


RANDOM_SEED = 101

## number of subplots, dimensions of data
sub1 =20
sub2 = 10


# Start log file
if config['output_to_log']:
    log = open(modelName+".log", "a")
    sys.stdout = log
    sys.stderr = log



###### USE DATA OR FAKE SET
use_data = config['use_data']
if ( use_data == True ):
    with open(aeName+"/params.json",'r') as f:
        params = json.load(f)
    emb = np.load(aeName+"/ae_latent_data.npy")
    #plot modulus
    latent_size=params['latent_layer']
    yy=latent_size//10
    mod = latent_size%yy
    print("latent size: ", latent_size, " yy: ", yy, "modulus is: ", mod)
    
    for ii in range(emb.shape[-1]-1):
        #plt.subplot(sub1,sub2, ii+1)
        #plt.scatter(emb[:,ii],emb[:,ii+1])
        if (ii%yy==mod or ii == 59 or ii == 61):
            plt.figure()
            plt.scatter(emb[:,ii],emb[:,ii+1])
            plt.savefig(figsDir+"/initial_dist_"+str(ii)+".png")
    plt.savefig(figsDir+"/initial_dist.png")
    plt.close('all')
    #plt.imsave(figsDir+"initial_dist.png",emb)

else:
    arr = np.load(aeName+"/ae_latent_data.npy")
    amin = np.amin(arr)
    amax = np.amax(arr) 
    mean = (amax+amin) / 2
    scale = (amax-amin)
    ndim0 = config['toy_dims'][0]
    ndim1 = config['toy_dims'][1]
    print("min / max: ", amin, amax)
    emb = (np.random.randn(ndim0,ndim1)).astype(np.float32)
    #emb = np.random.normal(loc=mean, scale=scale ,size=(10000,200))
    #emb = np.random.choice(arr[0],size=(10000,200),replace=True)
    params={}
    params['latent_layer']    = emb.shape[-1]
    params['learning_rate'] = 1e-4
    learning_rate = params['learning_rate']

    #plot modulus
    latent_size=params['latent_layer']
    yy=latent_size//5
    mod = latent_size%yy
    print("latent size: ", latent_size, " yy: ", yy, "modulus is: ", mod)
    for ii in range(emb.shape[-1]-1):
        #plt.subplot(sub1,sub2, ii+1)
        plt.figure(1)
        #plt.scatter(emb[:,ii],emb[:,ii+1])
        if (ii%yy==mod):
            plt.figure()
            plt.scatter(emb[:,ii],emb[:,ii+1])
            plt.savefig(figsDir+"/initial_dist_"+str(ii)+".png")
    plt.figure(1)
    plt.savefig(figsDir+"/initial_dist.png")
    plt.close('all')


emb_shape = emb.shape
print("Data shape: ",emb_shape)
#print("Data values: ", emb)


# splitting into training and validation data
train_size = int(len(emb)*0.3)
#valid_size = int(len(emb)*0.7)
print('sample size of training sample', train_size)
z_sample   = emb[:train_size]
z_valid = emb[train_size:,] 
valid_size = len(z_valid)
print('sample size of validation sample', valid_size)
z_train = z_sample[:]
print("Train size: ", len(z_train),"  Valid size: ", len(z_valid))



### SCALING
scale = config['scale']
################################
###### TRAINING PARAMETERS ######
n_epochs  = config['n_epochs']
batchsize = config['batchsize']
np_dtype = np.float32


if (use_data == True):
    learning_rate = params['learning_rate']

######################
### early stopping ###
patience = config['patience']
min_delta = config['min_delta']

prior = get_prior(latent_size)
#flow, bijector = get_nvp()
maf,bijector = get_nvp()
""" 
chain = []
if config['model']=='MAF':
     for i in range(config['N_MAF_layers']):
        chain.append(tfb.MaskedAutoregressiveFlow(Made(params=2,hidden_units=[latent_size,latent_size],activation='leaky_relu')))
        chain.append(tfb.Permute(permutation=init_once(np.randoml.permutation(latent_size).astype('int32'),name='permutation')))

bijector = tfb.Chain(bijectors=list(reversed(chain)), name='chain_of_maf')

maf = tfd.TransformedDistribution(
    distribution=tfd.Sample(prior, sample_shape=[2]),
    bijector=bijector,
)
     """
n_trainable_variables = len(maf.trainable_variables)
print("#### NUMBER OF TRAINABLE VARIABLES: ",n_trainable_variables)
opt = tf.keras.optimizers.Adam(learning_rate=learning_rate)
#loss = -tf.reduce_mean(flow.log_prob(emb))

######## BEFORE TRAINING PLOTS

#prior_training = bijector.inverse(emb)
# input data in 'Gaussian' space before training ### NOT SURE IF ITS GAUSSIA


print("latent size: ",latent_size)
X = get_prior(latent_size).sample(8000)
samples = [X]
names = [get_prior(latent_size).name]
f, arr = plt.subplots(1, len(samples), figsize=(4 * (len(samples)), 4))
""" 
for bijector in reversed(flow.bijector.bijectors):
    X = bijector.forward(X)
    samples.append(X)
    names.append(bijector.name)



f, arr = plt.subplots(1, len(samples), figsize=(4 * (len(samples)), 4))
X0 = samples[0]

for i in range(len(samples)):
    X1 = samples[i]
    idx = np.logical_and(X0[:, 0] < 0, X0[:, 1] < 0)
    idx =np.invert(idx)
    m0 = np.ma.masked_where(idx,X1[:,0])
    m1 = np.ma.masked_where(idx,X1[:,1])
    arr[i].scatter(m0, m1, s=10, color='red')

    idx = np.logical_and(X0[:, 0] > 0, X0[:, 1] < 0)
    idx =np.invert(idx)
    s0 = np.ma.masked_where(idx,X1[:,0])
    s1 = np.ma.masked_where(idx,X1[:,1])
    arr[i].scatter(s0, s1, s=10, color='green')

    idx = np.logical_and(X0[:, 0] < 0, X0[:, 1] > 0)
    idx =np.invert(idx)
    q0 = np.ma.masked_where(idx,X1[:,0])
    q1 = np.ma.masked_where(idx,X1[:,1])
    arr[i].scatter(q0, q1, s=10, color='blue')
 
    idx = np.logical_and(X0[:, 0] > 0, X0[:, 1] > 0)
    idx =np.invert(idx)
    w0 = np.ma.masked_where(idx,X1[:,0])
    w1 = np.ma.masked_where(idx,X1[:,1])
    arr[i].scatter(w0, w1, s=10, color='black')

    

    arr[i].set_xlim([-10, 10])
    arr[i].set_ylim([-10, 10])
    arr[i].set_title(names[i])
plt.savefig(figsDir+"/bijector_before.png")
plt.close('all')
 """
""" 

model = tf.keras.Model(flow)
model.compile(opt,loss)
results = model.fit(samples,batch_size=batchsize,shuffle=True)


 """



""" 

u_data = bijector.forward(emb)
for ii in range(params['latent_size']-1):
    #fig3 = plt.subplot(sub1,sub2, ii+1)
    plt.figure(1)
    #plt.scatter(u_data[:,ii],u_data[:,ii+1])
    if (ii%yy==mod):
        plt.figure()
        plt.scatter(u_data[:,ii],u_data[:,ii+1])
        plt.savefig(figsDir+"/gaussian_embed_before_"+str(ii)+".png")
plt.figure(1)
plt.savefig(figsDir+"/gaussian_embed_before.png")
plt.close('all')

z_data = bijector.inverse(emb)
for ii in range(params['latent_size']-1):
    #fig3 = plt.subplot(sub1,sub2, ii+1)
    plt.figure(1)
    #plt.scatter(u_data[:,ii],u_data[:,ii+1])
    if (ii%yy==mod):
        plt.figure()
        plt.scatter(z_data[:,ii],z_data[:,ii+1])
        plt.savefig(figsDir+"/z_space_before_"+str(ii)+".png")
plt.figure(1)
plt.savefig(figsDir+"/z_space_before.png")
plt.close('all')

# random samples in z_space (note again, we havent trained the model yet, so the output will not match the embedded data)
for ii in range(params['latent_size']-1):
    #fig4 = plt.subplot(sub1,sub2, ii+1)
    plt.figure(1)
    #plt.scatter(z_sample[:,ii],z_sample[:,ii+1])
    if (ii%yy==mod):
         plt.figure()
         plt.scatter(z_sample[:,ii],z_sample[:,ii+1])
         plt.savefig(figsDir+"/embed_to_latent_before_"+str(ii)+".png")
plt.figure(1)
plt.savefig(figsDir+"/embed_to_latent_before.png")
plt.close('all')


 """


######## EVALUATE LOSS (NEG LOG PROB) AND PERFORM ONE TRAINING STEP

# loss for batch of 16 data points
""" loss_batch = sess.run(loss,  feed_dict={data: emb[0:16]})
print("loss batch 0: ",loss_batch)

# one optimization step (need to pass training data and learning rate)
_  = sess.run(opt_op_nvp,  feed_dict={data: emb[0:16], lr:1e-3})
# loss after optimziation step
one_step = sess.run(loss,  feed_dict={data: emb[0:16]})
print("loss batch 1: ",one_step)

 """
################## TRAINING ####################
print("------------------> COMMENSE TRAINING <-------------------")

nvp_tloss = []
nvp_vloss = []


# split into training and validation data
#SplitSize=0.3
#print("## Data Preprocessing:") 
#print("-> Validation fraction=",SplitSize," Training fraction=",1-SplitSize)
#z_train, z_valid = train_test_split(emb, test_size=SplitSize, random_state = RANDOM_SEED, shuffle=True)
#print('X_train =', X_train.head(5))

start_time = time.perf_counter()
break_count = 0

checkpoint_directory = modelName
checkpoint_prefix = os.path.join(checkpoint_directory, "ckpt")
print("Checkpoint Prefix: ", checkpoint_prefix)

checkpoint = tf.train.Checkpoint(optimizer=opt, model=maf)


global_step = []
train_losses = []
val_losses = []
min_val_loss = tf.convert_to_tensor(np.inf, dtype=tf.float32)  # high value to ensure that first loss < min_loss
min_train_loss = tf.convert_to_tensor(np.inf, dtype=tf.float32)
min_val_epoch = 0
min_train_epoch = 0
ii = 0

while ii<n_epochs:
    e_start=time.perf_counter()
    print('epoch ', ii)

    np.random.shuffle(z_train)
    #x_samples.shuffle(buffer_size=emb.shape[0])
    batch_train_losses = []
    epoch = train_size//batchsize
    jj=0
    while jj < epoch:
        
        batch_loss = train_density_estimation(maf, opt, z_train[jj*batchsize:(jj+1)*batchsize])
        batch_train_losses.append(batch_loss)
        jj+=1
    train_loss = tf.reduce_mean(batch_train_losses)
   
    if ii % int(1) == 0:
        batch_val_losses = []
        nn = 0
        while nn < epoch: 
            batch_loss = nll(maf, z_train[nn*batchsize:(nn+1)*batchsize])
            batch_val_losses.append(batch_loss)
            nn+=1
            
        #val_loss = nll(flow, z_train)
        val_loss = tf.reduce_mean(batch_val_losses)

        global_step.append(ii)
        train_losses.append(train_loss)
        val_losses.append(val_loss)
        print(f"{ii}, train_loss: {train_loss}, val_loss: {val_loss}")

        if train_loss < min_train_loss:
            min_train_loss = train_loss
            min_train_epoch = ii

        if val_loss < min_val_loss:
            min_val_loss = val_loss
            min_val_epoch = ii
            print("saving checkpoint")
            checkpoint.write(file_prefix=checkpoint_prefix)


         #add custom stop loss
        if (train_losses[ii-1]-train_losses[ii] < min_delta):
            print("difference is: ",train_losses[ii-1]-train_losses[ii])
            break_count = break_count+1
        else: break_count = 0
        if ( break_count == patience): 
            print("##### STOP LOSS ACTIVATED #####")
            break
    ii+=1
    ## print heatmap every 100 epochs
    e_end = time.perf_counter()
    print("epoch time: ", ( e_end-e_start ))
    #if ii% int(50)==0:
     #   plot_heatmap_2d(flow,-4,4,-4,4,mesh_count=1000,name=figsDir+'hm_epoch%d'%ii)


end_time = time.perf_counter()
train_time = (end_time-start_time )/60
#nvp_funcs.export
print("Total Training time: ", (end_time-start_time )/60, " minutes")
#### SAVE THE MODEL
nvp_path = modelName+"/"
#if (os.path.exists(modelName)):
#    shutil.rmtree(modelName,ignore_errors=True)
print("Saving Model")
#nvp_funcs.export(nvp_path,sess)
tf.saved_model.save(maf,modelName)
saving = checkpoint.save(modelName+"/ckpt")

# load best model with min validation loss
checkpoint.restore(checkpoint_prefix)
# perform on test dataset
t_start = time.time()

epoch = train_size//batchsize
pp = 0
test_losses = []
print("Start Test Loss")
l = maf.sample(11111)
while pp < epoch:
    batch_loss = nll(maf, z_train[jj*batchsize:(jj+1)*batchsize])
    test_losses.append(batch_loss)
    pp+=1

test_loss = tf.reduce_mean(test_losses)

test_time = time.time() - t_start
print("Loss test time: ", test_time/60 , " minutes")

print(f'Test loss: {test_loss} at epoch: {ii}')
print(f'Average test log likelihood: {-test_loss} at epoch: {ii}')
print(f'Min val loss: {min_val_loss} at epoch: {min_val_epoch}')
print(f'Last val loss: {val_loss} at epoch: {ii}')
print(f'Min train loss: {min_train_loss} at epoch: {min_train_epoch}')
print(f'Last train loss: {train_loss} at epoch: {ii}')
print(f'Training time: {train_time}')
print(f'Test time: {test_time}')

results = {
    'test_loss': float(test_loss),
    'avg_test_logll': float(-test_loss),
    'min_val_loss': float(min_val_loss),
    'min_val_epoch': min_val_epoch,
    'val_loss': float(val_loss),
    'min_train_loss': float(min_train_loss),
    'min_train_epoch': min_train_epoch,
    'train_loss': float(train_loss),
    'train_time': train_time,
    'test_time': test_time,
    'trained_epochs': ii,
    'trainable variables': n_trainable_variables,
    
}

print("Plotting metrics")
print("Plot Losses")
plt.figure()
plt.plot(np.asarray(train_losses),label='training loss')
plt.plot(np.asarray(val_losses),label='validation loss' )
plt.xlabel('# iteration')
plt.ylabel('RealNVP loss')
plt.legend()
plt.savefig(figsDir+"/validation_loss.png")
""" 
##### Plot Log proab
log_prob = maf.log_prob(z_valid[1000:200])
plt.figure()
plt.plot(log_prob)
plt.xlabel('log probability')
plt.savefig(figsDir+"/log_prob.png")
plt.close('all')
 """ 

print("Plot Bijector Mapping")
## After training plots
#X = z_valid#[0:8000,50]
X = get_prior(latent_size).sample(8000)
samples = [X]
names = [get_prior(latent_size).name]
f, arr = plt.subplots(1, len(samples), figsize=(4 * (len(samples)), 4))
""" 
for bijector in reversed(maf.bijector.bijectors):
    X = bijector.forward(X)
    samples.append(X)
    names.append(bijector.name)

X0 = samples[0]
f, arr = plt.subplots(1, len(samples), figsize=(4 * (len(samples)), 4))
for i in range(len(samples)):
    X1 = samples[i]
    idx = np.logical_and(X0[:, 0] < 0, X0[:, 1] < 0)
    idx =np.invert(idx)
    m0 = np.ma.masked_where(idx,X1[:,0])
    m1 = np.ma.masked_where(idx,X1[:,1])
    arr[i].scatter(m0, m1, s=10, color='red')

    idx = np.logical_and(X0[:, 0] > 0, X0[:, 1] < 0)
    idx =np.invert(idx)
    s0 = np.ma.masked_where(idx,X1[:,0])
    s1 = np.ma.masked_where(idx,X1[:,1])
    arr[i].scatter(s0, s1, s=10, color='green')

    idx = np.logical_and(X0[:, 0] < 0, X0[:, 1] > 0)
    idx =np.invert(idx)
    q0 = np.ma.masked_where(idx,X1[:,0])
    q1 = np.ma.masked_where(idx,X1[:,1])
    arr[i].scatter(q0, q1, s=10, color='blue')
 
    idx = np.logical_and(X0[:, 0] > 0, X0[:, 1] > 0)
    idx =np.invert(idx)
    w0 = np.ma.masked_where(idx,X1[:,0])
    w1 = np.ma.masked_where(idx,X1[:,1])
    arr[i].scatter(w0, w1, s=10, color='black')

    

    arr[i].set_xlim([-10, 10])
    arr[i].set_ylim([-10, 10])
    arr[i].set_title(names[i])
plt.savefig(figsDir+"/bijector_after.png")
plt.close('all')
 """
print("Plot Before and After")
z_sample_a = maf.sample(8000)
z_sample_b = np.array(emb)
print("sample shape: ", z_sample_a.shape)
for nn in range(latent_size-1):

    #z_sample_a = flow.bijector.Bijector.inverse(z_sample_b)
    if nn%20 ==0:
        plt.scatter(z_sample_b[:,nn],z_sample_b[:,nn+1],label="before",)
        plt.scatter(z_sample_a[:,nn],z_sample_a[:,nn+1],label="after")
        plt.savefig(figsDir+"/comp"+str(nn)+".png")
        plt.close('all')
######### TEST TO SEE HOW WELL THE MODEL PERFORMED #########


print("Sanity Check")
integrals = []
for ii in range(params['latent_layer']-1):
    integral = sanity_check(maf)
    integrals[ii] = integral
sanity = np.mean(integrals)
print("Sanity check: ",sanity)
""" 
# plot density estimation of the best model
plot_heatmap_2d(flow, -4.0, 4.0, -4.0, 4.0, mesh_count=200, name=figsDir+"hm_final")  

# plot samples of the best model
plot_samples_2d(flow.sample(1000), name=figsDir+"final_dist")  
 """
""" 
### compare before and after
plt.figure()
plt.plot(np.asarray(get_prior(emb)),label='Before')
plt.plot(np.asarray(flow),label='after')
plt.legend()
plt.savefig(figsDir+"/comp_dist.png")
 """



""" 

# remove checkpoint
filelist = [f for f in os.listdir(checkpoint_directory)]
for f in filelist:
    os.remove(os.path.join(checkpoint_directory, f))
os.removedirs(checkpoint_directory)
 """
""" 
print( " ******* HERE 2 ********")
#after_training = bijector.inverse(emb[:,100])
print( " ******* HERE 3 ********")

z_data = flow.sample(1000)
for ii in range(params['latent_size']-1):
    #fig3 = plt.subplot(sub1,sub2, ii+1)
    plt.figure(1)
    #plt.scatter(u_data[:,ii],u_data[:,ii+1])
    if (ii%yy==mod):
        plt.figure()
        plt.scatter(z_data[:,ii],z_data[:,ii+1])
        plt.savefig(figsDir+"/gaussian_embed_before_"+str(ii)+".png")
plt.figure(1)
plt.savefig(figsDir+"/gaussian_embed_before.png")
plt.close('all')
 """
""" 
plt.figure(1)
plt.scatter(prior_training)
#plt.scatter(after_training)
plt.savefig(figsDir+"/comp.png")
plt.close('all')
 """
""" 
for nn in range(params['latent_size']-1):
   # plt.xlim(-12,12)
   # plt.ylim(-12,12)
    if (nn%yy==mod):
         plt.figure(1)
         #plt.scatter(Gaussian_sample[:,nn],Gaussian_sample[:,nn+1], label='prior')
         plt.scatter(prior_training[:,nn],prior_training[:,nn+1],label='prior train')
         plt.scatter(after_training[:,nn],after_training[:,nn+1], label='after train')
         #plt.xlim(-12,12)
         #plt.ylim(-12,12)
         plt.legend()
         plt.savefig(figsDir+"/z_space_dist_comp_"+str(nn)+".png")
         plt.close()
plt.close('all')
 """
""" 
u_data = bijector.forward(emb)
plot_prior = bijector.inverse(emb)
plot_after = flow.sample(1000)
#bwd_train = bijector.inverse(z_train)
#bwd_valid = bijector.inverse(z_valid)
#bwd_train = flow.sample(1000)
#bwd_valid = flow.sample(z_valid)
#Gaussian_sample = sess.run(prior_sample,  feed_dict={bs:len(z_sample)})
print( " ******* HERE 4 ********")
plt.figure(1)
plt.plot(plot_after)
plt.savefig(figsDir+"/sample.png")
plt.close('all')

 """
""" 
plt.figure(figsize=(10,10))
for nn in range(params['latent_size']-1):
    #plt.subplot(2,2, nn+1)
    plt.figure(1)
    plt.xlim(-12,12)
    plt.ylim(-12,12)
    if (nn%yy==mod):
         plt.figure(2)
         #plt.scatter(Gaussian_sample[:,nn],Gaussian_sample[:,nn+1], label='prior')
         plt.scatter(bwd_train[:,nn],bwd_train[:,nn+1],label='encoded train')
         plt.scatter(bwd_valid[:,nn],bwd_valid[:,nn+1], label='encoded valid')
         plt.xlim(-12,12)
         plt.ylim(-12,12)
         plt.legend()
         plt.savefig(figsDir+"/u_space_dist_after_"+str(nn)+".png")
         plt.close()
plt.figure(1)

plt.savefig(figsDir+"/u_space_dist_after.png")
plt.close('all')
 """
########## compare distributions in Z-space
""" 
fwd_sample = sess.run(fwd, feed_dict={latent_sample:Gaussian_sample})
input_data = sess.run(data,  feed_dict={data:emb})

plt.figure(figsize=(10,10))
for nn in range(params['latent_size']-1):
    #plt.subplot(2,2, nn+1)
    plt.figure(1)
    plt.scatter(input_data[:,nn],input_data[:,nn+1], label='data')
    plt.scatter(fwd_sample[:,nn],fwd_sample[:,nn+1], label='generated data')
    if (nn%yy==mod or nn == 59 or nn == 61):
         plt.figure()
         plt.scatter(input_data[:,nn],input_data[:,nn+1], label='data')
         plt.scatter(fwd_sample[:,nn],fwd_sample[:,nn+1], label='generated data')
         plt.legend()
         plt.savefig(figsDir+"/z_space_dist_after_"+str(nn)+".png")
plt.figure(1)
plt.savefig(figsDir+"/z_space_dist_after.png")
plt.close('all')

""" 
print("-----------------> Fin <-----------------")

 