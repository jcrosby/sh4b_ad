# examples use from and documentation can be found here:  https://github.com/VMBoehm/PAE/blob/master/TrainNVP_simplified_and_explained.ipynb


import os,sys
sys.path.append("modules/")
from global_module import *
import shutil
import time
####*IMPORANT*: Have to do this line *before* importing tensorflow
os.environ['PYTHONHASHSEED']=str(1)


print ('Number of arguments:', len(sys.argv), 'arguments.')
print ('Argument List:', str(sys.argv))
n = len(sys.argv)
if (n != 2):
      print ("No arguments!. Need at least 1 model and 1 input file in csv.zip")
      sys.exit()
inputData=sys.argv[1]

# Model name to save
from pathlib import Path

tail = Path(inputData).name+"_NF" 
modelName="./models/"+tail.replace(".csv.gz","")
figsDir ="./figs/"+tail.replace(".csv.gz","")
aetail = Path(inputData).name
aeName = "./models/"+aetail.replace(".csv.gz","")
print("Train model = ",modelName) 
print("Figures in = ", figsDir )

if (os.path.exists(figsDir)):
    shutil.rmtree(figsDir,ignore_errors=True)

if not os.path.exists(figsDir):
    os.makedirs(figsDir)

if (os.path.exists("models/data1percent_NF.log")):
    os.remove( "models/data1percent_NF.log" )


# Data Preprocessing
import pandas
import matplotlib
import yaml
import seaborn
import tensorflow
import tensorflow as tf2
import tensorflow.compat.v1 as tf
tf.disable_eager_execution()
import tensorflow_probability as tfp
import tensorflow_hub as hub
tfd = tfp.distributions
tfb = tfp.bijectors

from pae.bijector_init import get_nvp, nvp_module_spec, get_prior
from pae.flow_class import RealNVP, SplineParams
from pae.pae_functions import nll, train_density_estimation,sanity_check,PDF
from pae.plotting_module import single_scatter, multi_scatter,histogram, histogram2D,plot_pdf, bijector_slices
#from pae.train_utils import train_density_estimation, nll
########## 
import pickle
print('Numpy version      :' , numpy.__version__)
print('Pandas version     :' ,pandas.__version__)
print('Matplotlib version :' ,matplotlib.__version__)
print('Seaborn version    :' , seaborn.__version__)
print('Tensorflow version :' , tensorflow.__version__)
# print('Keras version      :' , keras.__version__)

print("-----------------------------------")
with open('pae/pae_config.yaml','r') as file:
    config = yaml.safe_load(file)
print("Using config file: pae_config.yaml")

import numpy as np
import pandas as pd
from scipy.stats import norm
pd.set_option('display.max_columns', None)
pd.set_option('display.max_row', None)
import matplotlib.pyplot as plt
plt.rcdefaults()
from pylab import rcParams
import seaborn as sns
import datetime
import matplotlib
matplotlib.use('Agg') # set the backend before importing pyplo. Fix Invalid DISPLAY variable 
from matplotlib import pyplot as plt
####### Deep learning libraries
from tensorflow import keras
from keras.models import Model, load_model
from keras.layers import Input, Dense
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.callbacks import EarlyStopping
from sklearn.preprocessing import  StandardScaler, MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import (confusion_matrix, classification_report, accuracy_score, roc_auc_score, auc,
                             precision_score, recall_score, roc_curve, precision_recall_curve,
                             precision_recall_fscore_support, f1_score,
                             precision_recall_fscore_support)


print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))
## remove previously trained model
if (os.path.exists(modelName)):
    shutil.rmtree(modelName,ignore_errors=True)


# Start log file
if config['output_to_log']:
    log = open(modelName+".log", "a")
    sys.stdout = log
    sys.stderr = log


config['seed'] = 101

## number of subplots, dimensions of data
sub1 =20
sub2 = 10


###### USE DATA OR FAKE SET
use_data = config['use_data']
if ( use_data == True ):
    aeName = aeName + config['name_change']
    with open(aeName+"/params.json",'r') as f:
        params = json.load(f)
    print("model folder: ",aeName)
    emb = np.load(aeName+"/ae_latent_data.npy")

    #plot modulus
    latent_size=params['latent_layer']
    yy=latent_size//10
    mod = latent_size%yy
    print("latent size: ", latent_size, " yy: ", yy, "modulus is: ", mod)
    
    single_scatter(emb,latent_size,figsDir,"initial_dist")

else:
    arr = np.load(aeName+"/ae_latent_data.npy")
    amin = np.amin(arr)
    amax = np.amax(arr) 
    mean = (amax+amin) / 2
    scale = (amax-amin)
    ndim0 = config['toy_dims'][0]
    ndim1 = config['toy_dims'][1]
    print("min / max: ", amin, amax)
    #emb = np.exp((np.random.randn(10000,10))).astype(np.float32)
    emb = np.exp(np.random.normal(loc=mean, scale=1.0 ,size=(ndim0,ndim1))).astype(np.float32)
    params={}
    params['latent_layer']    = emb.shape[-1]
    latent_size= params['latent_layer']
    params['learning_rate'] = config['learning_rate']
    print("shape: ", emb.shape)
    for i in range(latent_size-1):
        if i%3==0:
            emb[:,i-1] = emb[:,i-1]*-1
    #emb = np.exp(np.random.choice(arr[0],size=(10000,200),replace=True))
   
    
    learning_rate = params['learning_rate']

    #plot modulus
    latent_size=params['latent_layer']
    yy=latent_size//10
    mod = latent_size%yy
    print("latent size: ", latent_size, " yy: ", yy, "modulus is: ", mod)

    single_scatter(emb,latent_size,figsDir,"initial_dist")

# Drop NaNs just in case
print("Number of NaNs: ",np.isnan(emb).sum())
print("Data shape: ",emb.shape)
#print("Data values: ", emb)

## for log scaling in NVP
weight = config['weight']
#emb = emb*weight

### SCALING
#scale = 0.5
scale = config['scale'] #max(emb.flatten())
################################
###### TRAINING PARAMETERS ######
n_epochs  = config['n_epochs']
batchsize = config['batchsize']
if (use_data == True):
    learning_rate = config['learning_rate']

######################
### early stopping ###
patience = config['patience']
min_delta = config['min_delta']

### Construct the graph of the model. Has not been filled with values yet
# model in good old tf1 style
           
print("---------> Graph of Normalizing Flow model <----------")
tf.reset_default_graph()

# learning rate
lr            = tf.placeholder_with_default(0.001,shape=[])
# input data, z-space
data          = tf.placeholder(shape=[None,params['latent_layer']],dtype=tf.float32)
# input data, u-space
latent_sample = tf.placeholder(shape=[None,params['latent_layer']],dtype=tf.float32)
# batch size (not the other bs...)
bs            = tf.placeholder_with_default(16,shape=[])

optimizer     = tf.train.AdamOptimizer(learning_rate=lr)
prior         = get_prior(params['latent_layer'])

# ----------------------------------module to save the model----------------------------------------------------------#
nvp_spec  = hub.create_module_spec(nvp_module_spec)
nvp_funcs = hub.Module(nvp_spec, name='nvp_funcs',trainable=True)
# this is how you would load the module instead of creating a new one 
## nvp_funcs = hub.Module(params['module_dir'], trainable=True)
#-------------------------------------------------training------------------------------------------------------------#
log_prob       = nvp_funcs({'z_sample':data,'sample_size':1, 'u_sample':np.zeros((1,params['latent_layer']))},as_dict=True)['log_prob']
#prob           = nvp_funcs({'z_sample':data,'sample_size':1, 'u_sample':np.zeros((1,params['latent_layer']))},as_dict=True)['prob']
# the training loss is transformed distribution
loss           = -tf.reduce_mean(log_prob)

# evaluates one training step
opt_op_nvp     = optimizer.minimize(loss)
#---------------------------------------------------------------------------------------------------------------------#


#project into Gaussian space
bwd            = nvp_funcs({'z_sample':data,'sample_size':1, 'u_sample':np.zeros((1,params['latent_layer']))},as_dict=True)['bwd_pass']
#draw from the Gaussian 
prior_sample   = prior.sample(bs)
# fwd model these draws back into the embedding space
fwd            = nvp_funcs({'z_sample':np.zeros((1,params['latent_layer'])),'sample_size':1, 'u_sample':latent_sample},as_dict=True)['fwd_pass']


# initialize the session to evaluate your graph
sess = tf.Session()
sess.run(tf.global_variables_initializer())

######## BEFORE TRAINING PLOTS

# the feed dict is the disctioanry of values that you feed into the graph. here it is the embedded data.

log_prob_of_emb = sess.run(log_prob, feed_dict={data:emb})
# note that this is before we have trained the model
histogram(log_prob_of_emb, latent_size, figsDir,"log_probability_before",bins=100)
plt.hist(log_prob_of_emb, bins=100)
plt.savefig(figsDir+"/log_probability_before.png")
plt.close()

########

u_data = sess.run(bwd, feed_dict={data:emb})

# input data in 'Gaussian' space
single_scatter(u_data,latent_size,figsDir,"gaussian_embed_before_")

########

# draw random sample from normal of size 1024
u_sample = sess.run(prior_sample, feed_dict={bs:1024})
# bring this sanmple into the embedded space of the auto-enocder
z_sample = sess.run(fwd,  feed_dict={latent_sample: u_sample})
# random samples in z_space (note again, we havent trained the model yet, so the output will not match the embedded data)


single_scatter(z_sample,latent_size,figsDir,"embed_to_latent_before")

######## EVALUATE LOSS (NEG LOG PROB) AND PERFORM ONE TRAINING STEP

# loss for batch of 16 data points
loss_batch = sess.run(loss,  feed_dict={data: emb[0:16]})
print("loss batch 0: ",loss_batch)

# one optimization step (need to pass training data and learning rate)
_  = sess.run(opt_op_nvp,  feed_dict={data: emb[0:16], lr:1e-3})
# loss after optimziation step
one_step = sess.run(loss,  feed_dict={data: emb[0:16]})
print("loss batch 1: ",one_step)

### Print Bijector plots before training
ss = tf.Session()
flow,bijector = get_nvp()
Xt = get_prior(latent_size).sample(8000)
X = tf2.convert_to_tensor(Xt)
print("Evaluate prior")
arrayx = X.eval(session=ss)


#bijector_slices(flow,bijector,arrayx,X,Xt,latent_size,ss,figsDir,"bijector_before")

################## TRAINING ####################
print("------------------> COMMENSE TRAINING <-------------------")

nvp_tloss = []
nvp_vloss = []
ii     = 0

# split into training and validation data
#SplitSize=0.3
#print("## Data Preprocessing:") 
#print("-> Validation fraction=",SplitSize," Training fraction=",1-SplitSize)
#z_train, z_valid = train_test_split(emb, test_size=SplitSize, random_state = RANDOM_SEED, shuffle=True)
#print('X_train =', X_train.head(5))


# splitting into training and validation data
train_size = int(len(emb)*0.3)
print('sample size of training sample', train_size)
z_sample   = emb[:train_size]
z_valid = emb[train_size:,] 
valid_size = len(z_valid)
print('sample size of validation sample', valid_size)
z_train = z_sample[:]

start_time = time.perf_counter()
break_count = 0


while ii<n_epochs:
    e_start=time.perf_counter()
    print('epoch ', ii)
    # number of iterations for one epoch given current batchsize
    epoch = train_size//batchsize
    jj    = 0 
    while jj<epoch:
        # take one optimization step and evaluate current loss
        _, ll = sess.run([opt_op_nvp,loss],  feed_dict={lr: learning_rate, data:z_train[jj*batchsize:(jj+1)*batchsize]})
        jj+=1
    
    ll = sess.run(loss,  feed_dict={lr: learning_rate, data:z_train[:len(z_valid)]})
    nvp_tloss.append(ll)
    lv = sess.run(loss,  feed_dict={lr: learning_rate, data:z_valid})
    print('train loss: ', ll, 'val loss: ', lv)
    nvp_vloss.append(lv)
    # reshuffle data before next training epoch
    np.random.shuffle(z_sample) 
   
    if ii%20==0 and ii>0:
      # saves the model at regular intervals
        nvp_path = (modelName+"/epochs/"+'run1_nepoch%d'%ii)
        #try:    
        #print("### epxorting model to ", nvp_path)
        #nvp_funcs.export(nvp_path,sess)
       
        #except:
        #    pass 
     
    #add custom stop loss
    if (nvp_tloss[ii-1]-nvp_tloss[ii] < min_delta):
        print("difference is: ",nvp_tloss[ii-1]-nvp_tloss[ii])
        break_count = break_count+1
    else: break_count = 0
    if math.isnan(nvp_tloss[ii]) and math.isnan(nvp_vloss[ii]):
        print("##### Losses are NAN. Breaking #####")
        break
    if ( break_count == patience): 
        print("##### STOP LOSS ACTIVATED #####")
        break
    e_end = time.perf_counter()
    print("epoch time: ", ( e_end-e_start ))
    ii+=1

end_time = time.perf_counter()
nvp_funcs.export
print("Total Training time: ", (end_time-start_time )/60, " minutes")
#### SAVE THE MODEL
nvp_path = modelName+"/"

print("Saving Model")
nvp_funcs.export(nvp_path,sess)


plt.figure()
plt.plot(np.asarray(nvp_tloss),label='training loss')
plt.plot(np.asarray(nvp_vloss),label='validation loss' )
plt.xlabel('# iteration')
plt.ylabel('Flow loss')
plt.legend()
plt.savefig(figsDir+"/validation_loss.png")

print("copying config to figs")
shutil.copyfile("pae/pae_config.yaml", figsDir+"/pae_config.yaml")

print("-----------------> Fin <-----------------")

