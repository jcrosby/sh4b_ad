
import os,sys
sys.path.append("modules/")
from global_module import *
import shutil
import time
####*IMPORANT*: Have to do this line *before* importing tensorflow
os.environ['PYTHONHASHSEED']=str(1)


print ('Number of arguments:', len(sys.argv), 'arguments.')
print ('Argument List:', str(sys.argv))
n = len(sys.argv)
if (n != 3):
      print ("No arguments!. Need at least 1 model and 1 input file in csv.zip")
      sys.exit()
inputData=sys.argv[2]

# Model name to save
from pathlib import Path

tail = Path(inputData).name+"_NF" 
modelName="./models/"+tail.replace(".csv.gz","")
figsDir ="./figs/"+tail.replace(".csv.gz","")
aetail = Path(inputData).name
aeName = "./models/"+aetail.replace(".csv.gz","")
print("Train model = ",modelName) 
print("Figures in = ", figsDir )

if (os.path.exists(figsDir)):
    shutil.rmtree(figsDir,ignore_errors=True)

if not os.path.exists(figsDir):
    os.makedirs(figsDir)

if (os.path.exists("models/data1percent_NF.log")):
    os.remove( "models/data1percent_NF.log" )

# Start log file
#log = open(modelName+".log", "a")
#sys.stdout = log


# Data Preprocessing
import pandas
import matplotlib
import seaborn
import tensorflow
#import tensorflow as tf
import tensorflow.compat.v1 as tf
tf.disable_eager_execution()
import tensorflow_probability as tfp
import tensorflow_hub as hub
tfd = tfp.distributions
tfb = tfp.bijectors


from pae.flow_catalog import RealNVP, NN_Spline, BatchNorm
from pae.train_utils import train_density_estimation, nll

########## 
import pickle
print('Numpy version      :' , numpy.__version__)
print('Pandas version     :' ,pandas.__version__)
print('Matplotlib version :' ,matplotlib.__version__)
print('Seaborn version    :' , seaborn.__version__)
print('Tensorflow version :' , tensorflow.__version__)
# print('Keras version      :' , keras.__version__)

import numpy as np
import pandas as pd
pd.set_option('display.max_columns', None)
pd.set_option('display.max_row', None)
import matplotlib.pyplot as plt
plt.rcdefaults()
from pylab import rcParams
import seaborn as sns
import datetime
import matplotlib
matplotlib.use('Agg') # set the backend before importing pyplo. Fix Invalid DISPLAY variable 
from matplotlib import pyplot as plt
####### Deep learning libraries
from tensorflow import keras
from keras.models import Model, load_model
from keras.layers import Input, Dense
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.callbacks import EarlyStopping
from sklearn.preprocessing import  StandardScaler, MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import (confusion_matrix, classification_report, accuracy_score, roc_auc_score, auc,
                             precision_score, recall_score, roc_curve, precision_recall_curve,
                             precision_recall_fscore_support, f1_score,
                             precision_recall_fscore_support)


print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))


RANDOM_SEED = 101

sess = tf.InteractiveSession()

use_data = False
if ( use_data == True ):
    with open(aeName+"/params.json",'r') as f:
        params = json.load(f)
    emb = np.load(aeName+"/ae_latent_data.npy")
    #plot modulus
    latent_size=params['latent_size']
    yy=latent_size//10
    mod = latent_size%yy
    print("latent size: ", latent_size, " yy: ", yy, "modulus is: ", mod)
    
    for ii in range(emb.shape[-1]-1):
        #plt.subplot(sub1,sub2, ii+1)
        #plt.scatter(emb[:,ii],emb[:,ii+1])
        if (ii%yy==mod or ii == 59 or ii == 61):
            plt.figure()
            plt.scatter(emb[:,ii],emb[:,ii+1])
            plt.savefig(figsDir+"/initial_dist_"+str(ii)+".png")
    plt.savefig(figsDir+"/initial_dist.png")
    plt.close('all')
    #plt.imsave(figsDir+"initial_dist.png",emb)

else:
    arr = np.load(aeName+"/ae_latent_data.npy")
    amin = np.amin(arr)
    amax = np.amax(arr) 
    mean = (amax+amin) / 2
    scale = (amax-amin)
    print("min / max: ", amin, amax)
    emb = (np.random.randn(10000,2)).astype(np.float32)
    #emb = np.random.normal(loc=mean, scale=scale ,size=(10000,200))
    #emb = np.random.choice(arr[0],size=(10000,200),replace=True)
    params={}
    params['latent_size']    = emb.shape[-1]
    params['learning_rate'] = 1e-4
    learning_rate = params['learning_rate']

    #plot modulus
    latent_size=params['latent_size']
    """ 
    yy=latent_size//5
    mod = latent_size%yy
    print("latent size: ", latent_size, " yy: ", yy, "modulus is: ", mod)
    for ii in range(emb.shape[-1]-1):
        #plt.subplot(sub1,sub2, ii+1)
        plt.figure(1)
        #plt.scatter(emb[:,ii],emb[:,ii+1])
        if (ii%yy==mod):
            plt.figure()
            plt.scatter(emb[:,ii],emb[:,ii+1])
            plt.savefig(figsDir+"/initial_dist_"+str(ii)+".png")
    plt.figure(1)
    plt.savefig(figsDir+"/initial_dist.png")
    plt.close('all')

 """
## Settings
batch_size = 1000
dtype = tf.float32
np_dtype = np.float32
nbijectors = 12
input_shape = latent_size
USE_BATCHNORM = True

## Dataset
dataset = tf.data.Dataset.from_tensor_slices(emb.astype(np_dtype))
dataset = dataset.repeat()
dataset = dataset.shuffle(buffer_size=emb.shape[0])
dataset = dataset.prefetch(3*batch_size)
dataset = dataset.batch(batch_size)
data_iterator = dataset.make_one_shot_iterator()
x_samples = data_iterator.get_next()


## Construct Flow
base_dist = tfd.MultivariateNormalDiag(tf.zeros(latent_size, dtype=tf.float32), scale_diag=None)
#base_dist = tfd.MultivariateNormalDiag(loc=tf.zeros([2], dtype))

num_bijectors = nbijectors
bijectors = []

for i in range(num_bijectors):
    bijectors.append(RealNVP(input_shape=input_shape,n_hidden=[latent_size,latent_size]))
    if USE_BATCHNORM and i%2==0:
        bijectors.append(BatchNorm(name='batch_norm%d' % i))
    bijectors.append(tfb.Permute(permutation=[1,0]))
# Chain bijectors + discard last permutation
chain = tfb.Chain(list(reversed(bijectors[:-1])))

flow = tfd.TransformedDistribution(
    distribution=base_dist,
    bijector=chain
)

## Visualizing Before Training
X = base_dist.sample(8000)
samples = [X]
names = [base_dist.name]
for bijector in reversed(flow.bijector.bijectors):
    lx = bijector.forward(X)

    samples.append(lx)
    names.append(bijector.name)






