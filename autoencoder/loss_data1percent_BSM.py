# Anomaly detection https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.10.3542&rep=rep1&type=pdf
# Stouffer's Z-score

import sys
sys.path.append("modules/")
from AtlasStyle import *
from AtlasUtils import *
from global_module_ana import *

from ROOT import TH1D,TF1,TProfile2D,TEllipse, THStack,TRandom3,TFile,TLatex,TLegend,TPaveText,TGraphErrors,kRed,kBlue,kGreen,kCyan,kAzure,kYellow,kTRUE
import ROOT
""" 
# trigger type
myinput="interactive"
#trig_type="1" 
if (len(sys.argv) ==2):
   trig_type=sys.argv[1]
if (len(sys.argv) ==3):
   trig_type=sys.argv[1]
   myinput = sys.argv[2] 
if (len(sys.argv) == 4):
   trig_type=sys.argv[2]
   myinput = sys.argv[3]
   myinput = sys.argv[4]
 """
dataIn = sys.argv[1]

""" 
print ("Mode=",myinput)
print ("trigger",trig_type)
 """
Tlab="1 lep"
""" # the algoritm  the algorithm: HighsPt cut x 3 
if (int(trig_type)==1):
             Xmin=600
             Tlab="T1:\; MET"
if (int(trig_type)==2): # 1 lepton 
             Xmin=300
             Tlab="T2:\; 1 \ell"
if (int(trig_type)==3): # 2 lepton pT>25 GeV  
             Xmin=300
             Tlab="T3:\; 2 \ell"
if (int(trig_type)==4): #  single photon 
             Xmin=500  # 150*3 
             Tlab="T4:\; 1 \gamma"
if (int(trig_type)==5): #  2 photon    
             Xmin=300  # 160x3 
             Tlab="T5:\; 2 \gamma"
if (int(trig_type)==6): #  single jet (pT>500 GeV)  
             Xmin=1500  #  
             Tlab="T6:\; 1 jet"
if (int(trig_type)==7): #  4 jet  (lead 200 GeV) 
             Xmin=600  #  
             Tlab="T7:\; 4 jets"

 """
print ('Number of arguments:', len(sys.argv), 'arguments.') 
print ('Argument List:', str(sys.argv))
print ('Use as: script.py -b 0 (or 1,2)') 
#print ("Mode=",myinput) 


gROOT.Reset()
figdir="figs/"
fname=os.path.basename(__file__)
fname=fname.replace("loss_data1percent","loss_data1percent");
epsfig=figdir+(fname).replace(".py",".eps")


nameX="log (Loss)"
nameY="Events"
#Ymin=0.1 
Ymax=19000000000
#Ymax=1950000
Ymin=0.002
Xmin=-11.5
Xmax=-2.5

######################################################
gROOT.SetStyle("Plain");
gROOT.SetStyle("ATLAS");

c1=TCanvas("c_massjj","BPRE",10,10,600,500);
#c1.Divide(1,1,0.008,0.007);
ps1 = TPostScript( epsfig,113)

c1.cd(1);
gPad.SetLogy(1)
gPad.SetLogx(0)
gPad.SetTopMargin(0.02)
gPad.SetBottomMargin(0.12)
gPad.SetLeftMargin(0.14)
gPad.SetRightMargin(0.02)


h=gPad.DrawFrame(Xmin,Ymin,Xmax,Ymax);
h.Draw()


name="Loss_"
root_model2use="root/old/analysis_data1percent_ll20_datahalf_10AR.root"
xfile=TFile(root_model2use)
xfile.ls()
data=xfile.Get(name+"data")
data.SetTitle("")
data.SetStats(0)
data.SetLineWidth(2)
data.SetLineColor( 1 )
data.SetMarkerColor( 1 )
data.SetMarkerSize( 0.5 )
data.SetFillColor(0);
data.Draw("same histo pe")
#c1.SaveAs("figs/data.png")
xsum=data.Integral();
nbins = data.GetNbinsX()
print("Finding AR cut")
for ibin in range(nbins):
   binint = data.Integral(ibin,nbins)
   #print("bin: ",ibin, " events: ",binint)
   if binint < 1000000:
      x1bar = data.GetBinCenter(ibin)
      print("1b AR cut is at bin ",ibin, " at X value ",x1bar)
      break
   
for ibin in range(nbins):
   binint = data.Integral(ibin,nbins)
   #print("bin: ",ibin, " events: ",binint)
   if binint < 6200:
      x2bar = data.GetBinCenter(ibin)
      print("2b AR cut is at bin ",ibin, " at X value ",x2bar)
      break
print("Summ=",xsum)


run="x1000_s500"
lcolor=2
bsmname = "Loss_bsm_"
model = "x1000_s500"
bname = bsmname+model
ha2=drawBSM(bname,lcolor,model)
ha2.Draw("same histo")

run=901996
lcolor=3
model = "x3000_s750"
bname = bsmname+model
ha3=drawBSM(bname,lcolor,model)
ha3.Draw("same histo")

lcolor=42
model = "x300_s70"
bname = bsmname+model
ha4=drawBSM(bname,lcolor,model)
ha4.Draw("same histo")

run=901972
lcolor=52
model = "x6000_s1000"
bname = bsmname+model
ha5=drawBSM(bname,lcolor,model)
ha5.Draw("same histo")

run=801648
lcolor=64
model = "x6000_s5000"
bname = bsmname+model
ha6=drawBSM(bname,lcolor,model)
ha6.Draw("same histo")

run=801648
lcolor=64
model = "x750_s250"
bname = bsmname+model
ha7=drawBSM(bname,lcolor,model)
ha7.Draw("same histo")

#900474.QBHPy8EG_QBH_jetphoton_n6_Mth3000 Quantum Black Hole (Mass of threshold of 3000, n is number of extra dimension) https://arxiv.org/pdf/2005.02548.pdf
#901972.QBHPy8EG_QBH_jetelectron_n6_Mth08000
#901996.QBHPy8EG_QBH_jetmuon_n6_Mth08000

# mc20_13TeV:mc20_13TeV.311252.MadGraphPythia8EvtGen_A14NNPDF23LO_Zprime4bM1600G10.deriv.DAOD_PHYS.e7191_a899_r13145_p5631
#run=311252
#lcolor=42
#ha6=drawBSM(name,lcolor,run,trig_type)
#ha6.Draw("same histo")

# mc20_13TeV:mc20_13TeV.426345.Pythia8EvtGen_A14NNPDF23LO_Zprime_tt_flatpT.deriv.DAOD_PHYS.e6880_s3681_r13145_p5631
#run=426345
#lcolor=42
#ha7=drawBSM(name,lcolor,run,trig_type)
#ha7.Draw("same histo")

""" 
# 100 fb
Xcut=-10
xsum14000=data.Integral(data.FindBin(Xcut), data.FindBin(0));
print("Summ=",xsum14000, " for cut=",Xcut)

x1=c1.XtoPad(Xcut)
x2=c1.XtoPad(Xcut)
ar5=TArrow(x1,Ymin,x2,c1.YtoPad(200000),0.05,">");
ar5.SetLineWidth(3)
ar5.SetLineStyle(3)
ar5.SetLineColor(2)
ar5.Draw("same")


Xcut=CutOutlier_1PB
xsum14000=data.Integral(data.FindBin(Xcut), data.FindBin(0));
print("Summ=",xsum14000, " for cut=",Xcut)
x1=c1.XtoPad(Xcut)
x2=c1.XtoPad(Xcut)
ar7=TArrow(x1,Ymin,x2,c1.YtoPad(200000),0.05,">");
ar7.SetLineWidth(3)
ar7.SetLineStyle(2)
ar7.SetLineColor(2)
ar7.Draw("same")
 """

# BSM cross sections at around 400 GeV
# -------------------------------------
# Techicolor model: 1 pb
# SSM:  9 pb
# H+ model:  9 pb
# DM model: 0.9 pb
# Simplified DM mode with W: 1.3 pb
# Radion model: 2.26 pb near 500 GeV
# Composite lepton model 1.4 pb (max)
 
# 10 pb
# 10000*140 = 1400000  #  1.4M
X1bcut= x1bar
xsum14000=data.Integral(data.FindBin(X1bcut), data.FindBin(0));
print("Summ=",xsum14000, " for cut=",X1bcut)
x1=c1.XtoPad(X1bcut)
x2=c1.XtoPad(X1bcut)
ar6=TArrow(x1,Ymin,x2,c1.YtoPad(200000),0.05,">");
ar6.SetLineWidth(3)
ar6.SetLineStyle(1)
ar6.SetLineColor(2)
ar6.Draw("same")

X2bcut= x2bar
xsum2b=data.Integral(data.FindBin(X2bcut), data.FindBin(0));
print("Summ=",xsum2b, " for cut=",X2bcut)
x1=c1.XtoPad(X2bcut)
x2=c1.XtoPad(X2bcut)
ar7=TArrow(x1,Ymin,x2,c1.YtoPad(200000),0.05,">");
ar7.SetLineWidth(3)
ar7.SetLineStyle(9)
ar7.SetLineColor(6)
ar7.Draw("same")


dis=distanceHistograms(data, bsm=[ha2,ha3,ha4,ha5,ha6,ha7])
#dis=distanceHistograms(data, bsm=[data])
dis='%.2f'%( dis )


ax=h.GetXaxis(); ax.SetTitleOffset(0.8)
ax.SetTitle( nameX );
ay=h.GetYaxis(); ay.SetTitleOffset(0.8)
ay.SetTitle( nameY );
ax.SetTitleOffset(1.1); ay.SetTitleOffset(1.5)
ax.Draw("same")
ay.Draw("same")

leg2=TLegend(0.45, 0.63, 0.95, 0.88);
leg2.SetBorderSize(0);
leg2.SetTextFont(62);
leg2.SetFillColor(10);
leg2.SetTextSize(0.032);
leg2.AddEntry(data,"Run2 LR Data (ML cut)","lfp")
leg2.AddEntry(ha2,"801601 x1000_s500","lp")
leg2.AddEntry(ha3,"801644 x3000_s750","lp")
leg2.AddEntry(ha4,"801578 x300_s70","lp")
leg2.AddEntry(ha5,"801907 x6000_s1000","lp")
leg2.AddEntry(ha6,"801911 x6000_s5000","lp")
leg2.AddEntry(ha7,"801590 x750_s250","lp")
leg2.Draw("same")

leg3=TLegend(0.76, 0.47, 0.974, 0.600);
leg3.SetBorderSize(0);
leg3.SetTextFont(62);
leg3.SetFillColor(10);
leg3.SetTextSize(0.035);
leg3.SetHeader("AR:")
leg3.AddEntry(ar6,"1bb AR","l")
leg3.AddEntry(ar7,"2bb AR","l")
leg3.Draw("same")
""" 
myText(0.19,0.82,1,0.04,UsedData0)
myText(0.7,0.9,2,0.06, Tlab)
myText(0.2,0.3,36,0.06, "D="+str(dis) )
 """
ATLASLabel(0.19,0.89,0.14,0.03)

## record ROOT file
rootfile=figdir+fname.replace(".py",".root")
out=rootfile
hfile=TFile(out,"RECREATE","FitResult")
data.SetName("data")
data.SetTitle("data")
data.Write()
hfile.Close()
print("Write=",out)



print (epsfig) 
gPad.RedrawAxis()
c1.Update()
c1.SaveAs("figs/loss_data1percent_nolog_BSM.png")
ps1.Close()
if (myinput != "-b"):
#              if (raw_input("Press any key to exit") != "-9999"):
               if (input("Press any key to exit") != "-9999"):
                 c1.Close(); sys.exit(1);




