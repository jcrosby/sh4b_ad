��	
��
^
AssignVariableOp
resource
value"dtype"
dtypetype"
validate_shapebool( �
�
BiasAdd

value"T	
bias"T
output"T""
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
8
Const
output"dtype"
valuetensor"
dtypetype
$
DisableCopyOnRead
resource�
.
Identity

input"T
output"T"	
Ttype
2
L2Loss
t"T
output"T"
Ttype:
2
\
	LeakyRelu
features"T
activations"T"
alphafloat%��L>"
Ttype0:
2
u
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:
2	
�
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool("
allow_missing_filesbool( �
?
Mul
x"T
y"T
z"T"
Ttype:
2	�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
@
RealDiv
x"T
y"T
z"T"
Ttype:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
d
Shape

input"T&
output"out_type��out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ��
@
StaticRegexFullMatch	
input

output
"
patternstring
�
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
L

StringJoin
inputs*N

output"

Nint("
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.13.02v2.13.0-rc2-7-g1cb1a030a628��
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
|
Adam/v/output/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:d*#
shared_nameAdam/v/output/bias
u
&Adam/v/output/bias/Read/ReadVariableOpReadVariableOpAdam/v/output/bias*
_output_shapes
:d*
dtype0
|
Adam/m/output/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:d*#
shared_nameAdam/m/output/bias
u
&Adam/m/output/bias/Read/ReadVariableOpReadVariableOpAdam/m/output/bias*
_output_shapes
:d*
dtype0
�
Adam/v/output/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:Pd*%
shared_nameAdam/v/output/kernel
}
(Adam/v/output/kernel/Read/ReadVariableOpReadVariableOpAdam/v/output/kernel*
_output_shapes

:Pd*
dtype0
�
Adam/m/output/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:Pd*%
shared_nameAdam/m/output/kernel
}
(Adam/m/output/kernel/Read/ReadVariableOpReadVariableOpAdam/m/output/kernel*
_output_shapes

:Pd*
dtype0
|
Adam/v/layer4/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:P*#
shared_nameAdam/v/layer4/bias
u
&Adam/v/layer4/bias/Read/ReadVariableOpReadVariableOpAdam/v/layer4/bias*
_output_shapes
:P*
dtype0
|
Adam/m/layer4/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:P*#
shared_nameAdam/m/layer4/bias
u
&Adam/m/layer4/bias/Read/ReadVariableOpReadVariableOpAdam/m/layer4/bias*
_output_shapes
:P*
dtype0
�
Adam/v/layer4/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:(P*%
shared_nameAdam/v/layer4/kernel
}
(Adam/v/layer4/kernel/Read/ReadVariableOpReadVariableOpAdam/v/layer4/kernel*
_output_shapes

:(P*
dtype0
�
Adam/m/layer4/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:(P*%
shared_nameAdam/m/layer4/kernel
}
(Adam/m/layer4/kernel/Read/ReadVariableOpReadVariableOpAdam/m/layer4/kernel*
_output_shapes

:(P*
dtype0
|
Adam/v/layer3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:(*#
shared_nameAdam/v/layer3/bias
u
&Adam/v/layer3/bias/Read/ReadVariableOpReadVariableOpAdam/v/layer3/bias*
_output_shapes
:(*
dtype0
|
Adam/m/layer3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:(*#
shared_nameAdam/m/layer3/bias
u
&Adam/m/layer3/bias/Read/ReadVariableOpReadVariableOpAdam/m/layer3/bias*
_output_shapes
:(*
dtype0
�
Adam/v/layer3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:(*%
shared_nameAdam/v/layer3/kernel
}
(Adam/v/layer3/kernel/Read/ReadVariableOpReadVariableOpAdam/v/layer3/kernel*
_output_shapes

:(*
dtype0
�
Adam/m/layer3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:(*%
shared_nameAdam/m/layer3/kernel
}
(Adam/m/layer3/kernel/Read/ReadVariableOpReadVariableOpAdam/m/layer3/kernel*
_output_shapes

:(*
dtype0
|
Adam/v/latent/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/v/latent/bias
u
&Adam/v/latent/bias/Read/ReadVariableOpReadVariableOpAdam/v/latent/bias*
_output_shapes
:*
dtype0
|
Adam/m/latent/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/m/latent/bias
u
&Adam/m/latent/bias/Read/ReadVariableOpReadVariableOpAdam/m/latent/bias*
_output_shapes
:*
dtype0
�
Adam/v/latent/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:(*%
shared_nameAdam/v/latent/kernel
}
(Adam/v/latent/kernel/Read/ReadVariableOpReadVariableOpAdam/v/latent/kernel*
_output_shapes

:(*
dtype0
�
Adam/m/latent/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:(*%
shared_nameAdam/m/latent/kernel
}
(Adam/m/latent/kernel/Read/ReadVariableOpReadVariableOpAdam/m/latent/kernel*
_output_shapes

:(*
dtype0
|
Adam/v/layer2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:(*#
shared_nameAdam/v/layer2/bias
u
&Adam/v/layer2/bias/Read/ReadVariableOpReadVariableOpAdam/v/layer2/bias*
_output_shapes
:(*
dtype0
|
Adam/m/layer2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:(*#
shared_nameAdam/m/layer2/bias
u
&Adam/m/layer2/bias/Read/ReadVariableOpReadVariableOpAdam/m/layer2/bias*
_output_shapes
:(*
dtype0
�
Adam/v/layer2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:P(*%
shared_nameAdam/v/layer2/kernel
}
(Adam/v/layer2/kernel/Read/ReadVariableOpReadVariableOpAdam/v/layer2/kernel*
_output_shapes

:P(*
dtype0
�
Adam/m/layer2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:P(*%
shared_nameAdam/m/layer2/kernel
}
(Adam/m/layer2/kernel/Read/ReadVariableOpReadVariableOpAdam/m/layer2/kernel*
_output_shapes

:P(*
dtype0
|
Adam/v/layer1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:P*#
shared_nameAdam/v/layer1/bias
u
&Adam/v/layer1/bias/Read/ReadVariableOpReadVariableOpAdam/v/layer1/bias*
_output_shapes
:P*
dtype0
|
Adam/m/layer1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:P*#
shared_nameAdam/m/layer1/bias
u
&Adam/m/layer1/bias/Read/ReadVariableOpReadVariableOpAdam/m/layer1/bias*
_output_shapes
:P*
dtype0
�
Adam/v/layer1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:dP*%
shared_nameAdam/v/layer1/kernel
}
(Adam/v/layer1/kernel/Read/ReadVariableOpReadVariableOpAdam/v/layer1/kernel*
_output_shapes

:dP*
dtype0
�
Adam/m/layer1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:dP*%
shared_nameAdam/m/layer1/kernel
}
(Adam/m/layer1/kernel/Read/ReadVariableOpReadVariableOpAdam/m/layer1/kernel*
_output_shapes

:dP*
dtype0
n
learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namelearning_rate
g
!learning_rate/Read/ReadVariableOpReadVariableOplearning_rate*
_output_shapes
: *
dtype0
f
	iterationVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	iteration
_
iteration/Read/ReadVariableOpReadVariableOp	iteration*
_output_shapes
: *
dtype0	
n
output/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:d*
shared_nameoutput/bias
g
output/bias/Read/ReadVariableOpReadVariableOpoutput/bias*
_output_shapes
:d*
dtype0
v
output/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:Pd*
shared_nameoutput/kernel
o
!output/kernel/Read/ReadVariableOpReadVariableOpoutput/kernel*
_output_shapes

:Pd*
dtype0
n
layer4/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:P*
shared_namelayer4/bias
g
layer4/bias/Read/ReadVariableOpReadVariableOplayer4/bias*
_output_shapes
:P*
dtype0
v
layer4/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:(P*
shared_namelayer4/kernel
o
!layer4/kernel/Read/ReadVariableOpReadVariableOplayer4/kernel*
_output_shapes

:(P*
dtype0
n
layer3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:(*
shared_namelayer3/bias
g
layer3/bias/Read/ReadVariableOpReadVariableOplayer3/bias*
_output_shapes
:(*
dtype0
v
layer3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:(*
shared_namelayer3/kernel
o
!layer3/kernel/Read/ReadVariableOpReadVariableOplayer3/kernel*
_output_shapes

:(*
dtype0
n
latent/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namelatent/bias
g
latent/bias/Read/ReadVariableOpReadVariableOplatent/bias*
_output_shapes
:*
dtype0
v
latent/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:(*
shared_namelatent/kernel
o
!latent/kernel/Read/ReadVariableOpReadVariableOplatent/kernel*
_output_shapes

:(*
dtype0
n
layer2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:(*
shared_namelayer2/bias
g
layer2/bias/Read/ReadVariableOpReadVariableOplayer2/bias*
_output_shapes
:(*
dtype0
v
layer2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:P(*
shared_namelayer2/kernel
o
!layer2/kernel/Read/ReadVariableOpReadVariableOplayer2/kernel*
_output_shapes

:P(*
dtype0
n
layer1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:P*
shared_namelayer1/bias
g
layer1/bias/Read/ReadVariableOpReadVariableOplayer1/bias*
_output_shapes
:P*
dtype0
v
layer1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:dP*
shared_namelayer1/kernel
o
!layer1/kernel/Read/ReadVariableOpReadVariableOplayer1/kernel*
_output_shapes

:dP*
dtype0
x
serving_default_inputPlaceholder*'
_output_shapes
:���������d*
dtype0*
shape:���������d
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_inputlayer1/kernellayer1/biaslayer2/kernellayer2/biaslatent/kernellatent/biaslayer3/kernellayer3/biaslayer4/kernellayer4/biasoutput/kerneloutput/bias*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������d*.
_read_only_resource_inputs
	
*1
config_proto!

CPU

GPU (2J 8� *-
f(R&
$__inference_signature_wrapper_668901

NoOpNoOp
�F
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�F
value�FB�F B�F
�
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer_with_weights-3
layer-4
layer_with_weights-4
layer-5
layer_with_weights-5
layer-6
	variables
	trainable_variables

regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses
_default_save_signature
	optimizer

signatures*
* 
�
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses

kernel
bias*
�
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses

kernel
 bias*
�
!	variables
"trainable_variables
#regularization_losses
$	keras_api
%__call__
*&&call_and_return_all_conditional_losses

'kernel
(bias*
�
)	variables
*trainable_variables
+regularization_losses
,	keras_api
-__call__
*.&call_and_return_all_conditional_losses

/kernel
0bias*
�
1	variables
2trainable_variables
3regularization_losses
4	keras_api
5__call__
*6&call_and_return_all_conditional_losses

7kernel
8bias*
�
9	variables
:trainable_variables
;regularization_losses
<	keras_api
=__call__
*>&call_and_return_all_conditional_losses

?kernel
@bias*
Z
0
1
2
 3
'4
(5
/6
07
78
89
?10
@11*
Z
0
1
2
 3
'4
(5
/6
07
78
89
?10
@11*
* 
�
Anon_trainable_variables

Blayers
Cmetrics
Dlayer_regularization_losses
Elayer_metrics
	variables
	trainable_variables

regularization_losses
__call__
_default_save_signature
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses*

Ftrace_0
Gtrace_1* 

Htrace_0
Itrace_1* 
* 
�
J
_variables
K_iterations
L_learning_rate
M_index_dict
N
_momentums
O_velocities
P_update_step_xla*

Qserving_default* 

0
1*

0
1*
* 
�
Rnon_trainable_variables

Slayers
Tmetrics
Ulayer_regularization_losses
Vlayer_metrics
	variables
trainable_variables
regularization_losses
__call__
Wactivity_regularizer_fn
*&call_and_return_all_conditional_losses
&X"call_and_return_conditional_losses*

Ytrace_0* 

Ztrace_0* 
]W
VARIABLE_VALUElayer1/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE*
YS
VARIABLE_VALUElayer1/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE*

0
 1*

0
 1*
* 
�
[non_trainable_variables

\layers
]metrics
^layer_regularization_losses
_layer_metrics
	variables
trainable_variables
regularization_losses
__call__
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses*

`trace_0* 

atrace_0* 
]W
VARIABLE_VALUElayer2/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE*
YS
VARIABLE_VALUElayer2/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE*

'0
(1*

'0
(1*
* 
�
bnon_trainable_variables

clayers
dmetrics
elayer_regularization_losses
flayer_metrics
!	variables
"trainable_variables
#regularization_losses
%__call__
*&&call_and_return_all_conditional_losses
&&"call_and_return_conditional_losses*

gtrace_0* 

htrace_0* 
]W
VARIABLE_VALUElatent/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE*
YS
VARIABLE_VALUElatent/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE*

/0
01*

/0
01*
* 
�
inon_trainable_variables

jlayers
kmetrics
llayer_regularization_losses
mlayer_metrics
)	variables
*trainable_variables
+regularization_losses
-__call__
*.&call_and_return_all_conditional_losses
&."call_and_return_conditional_losses*

ntrace_0* 

otrace_0* 
]W
VARIABLE_VALUElayer3/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE*
YS
VARIABLE_VALUElayer3/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE*

70
81*

70
81*
* 
�
pnon_trainable_variables

qlayers
rmetrics
slayer_regularization_losses
tlayer_metrics
1	variables
2trainable_variables
3regularization_losses
5__call__
*6&call_and_return_all_conditional_losses
&6"call_and_return_conditional_losses*

utrace_0* 

vtrace_0* 
]W
VARIABLE_VALUElayer4/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE*
YS
VARIABLE_VALUElayer4/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE*

?0
@1*

?0
@1*
* 
�
wnon_trainable_variables

xlayers
ymetrics
zlayer_regularization_losses
{layer_metrics
9	variables
:trainable_variables
;regularization_losses
=__call__
*>&call_and_return_all_conditional_losses
&>"call_and_return_conditional_losses*

|trace_0* 

}trace_0* 
]W
VARIABLE_VALUEoutput/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE*
YS
VARIABLE_VALUEoutput/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE*
* 
5
0
1
2
3
4
5
6*

~0*
* 
* 
* 
* 
* 
* 
�
K0
1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11
�12
�13
�14
�15
�16
�17
�18
�19
�20
�21
�22
�23
�24*
SM
VARIABLE_VALUE	iteration0optimizer/_iterations/.ATTRIBUTES/VARIABLE_VALUE*
ZT
VARIABLE_VALUElearning_rate3optimizer/_learning_rate/.ATTRIBUTES/VARIABLE_VALUE*
* 
e
0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11*
f
�0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11*
* 
* 
* 
* 
* 
* 
* 

�trace_0* 

�trace_0* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
<
�	variables
�	keras_api

�total

�count*
_Y
VARIABLE_VALUEAdam/m/layer1/kernel1optimizer/_variables/1/.ATTRIBUTES/VARIABLE_VALUE*
_Y
VARIABLE_VALUEAdam/v/layer1/kernel1optimizer/_variables/2/.ATTRIBUTES/VARIABLE_VALUE*
]W
VARIABLE_VALUEAdam/m/layer1/bias1optimizer/_variables/3/.ATTRIBUTES/VARIABLE_VALUE*
]W
VARIABLE_VALUEAdam/v/layer1/bias1optimizer/_variables/4/.ATTRIBUTES/VARIABLE_VALUE*
_Y
VARIABLE_VALUEAdam/m/layer2/kernel1optimizer/_variables/5/.ATTRIBUTES/VARIABLE_VALUE*
_Y
VARIABLE_VALUEAdam/v/layer2/kernel1optimizer/_variables/6/.ATTRIBUTES/VARIABLE_VALUE*
]W
VARIABLE_VALUEAdam/m/layer2/bias1optimizer/_variables/7/.ATTRIBUTES/VARIABLE_VALUE*
]W
VARIABLE_VALUEAdam/v/layer2/bias1optimizer/_variables/8/.ATTRIBUTES/VARIABLE_VALUE*
_Y
VARIABLE_VALUEAdam/m/latent/kernel1optimizer/_variables/9/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/v/latent/kernel2optimizer/_variables/10/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEAdam/m/latent/bias2optimizer/_variables/11/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEAdam/v/latent/bias2optimizer/_variables/12/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/m/layer3/kernel2optimizer/_variables/13/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/v/layer3/kernel2optimizer/_variables/14/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEAdam/m/layer3/bias2optimizer/_variables/15/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEAdam/v/layer3/bias2optimizer/_variables/16/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/m/layer4/kernel2optimizer/_variables/17/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/v/layer4/kernel2optimizer/_variables/18/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEAdam/m/layer4/bias2optimizer/_variables/19/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEAdam/v/layer4/bias2optimizer/_variables/20/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/m/output/kernel2optimizer/_variables/21/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/v/output/kernel2optimizer/_variables/22/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEAdam/m/output/bias2optimizer/_variables/23/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEAdam/v/output/bias2optimizer/_variables/24/.ATTRIBUTES/VARIABLE_VALUE*
* 
* 

�0
�1*

�	variables*
SM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE*
SM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE*
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filenamelayer1/kernellayer1/biaslayer2/kernellayer2/biaslatent/kernellatent/biaslayer3/kernellayer3/biaslayer4/kernellayer4/biasoutput/kerneloutput/bias	iterationlearning_rateAdam/m/layer1/kernelAdam/v/layer1/kernelAdam/m/layer1/biasAdam/v/layer1/biasAdam/m/layer2/kernelAdam/v/layer2/kernelAdam/m/layer2/biasAdam/v/layer2/biasAdam/m/latent/kernelAdam/v/latent/kernelAdam/m/latent/biasAdam/v/latent/biasAdam/m/layer3/kernelAdam/v/layer3/kernelAdam/m/layer3/biasAdam/v/layer3/biasAdam/m/layer4/kernelAdam/v/layer4/kernelAdam/m/layer4/biasAdam/v/layer4/biasAdam/m/output/kernelAdam/v/output/kernelAdam/m/output/biasAdam/v/output/biastotalcountConst*5
Tin.
,2**
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *1
config_proto!

CPU

GPU (2J 8� *(
f#R!
__inference__traced_save_669294
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamelayer1/kernellayer1/biaslayer2/kernellayer2/biaslatent/kernellatent/biaslayer3/kernellayer3/biaslayer4/kernellayer4/biasoutput/kerneloutput/bias	iterationlearning_rateAdam/m/layer1/kernelAdam/v/layer1/kernelAdam/m/layer1/biasAdam/v/layer1/biasAdam/m/layer2/kernelAdam/v/layer2/kernelAdam/m/layer2/biasAdam/v/layer2/biasAdam/m/latent/kernelAdam/v/latent/kernelAdam/m/latent/biasAdam/v/latent/biasAdam/m/layer3/kernelAdam/v/layer3/kernelAdam/m/layer3/biasAdam/v/layer3/biasAdam/m/layer4/kernelAdam/v/layer4/kernelAdam/m/layer4/biasAdam/v/layer4/biasAdam/m/output/kernelAdam/v/output/kernelAdam/m/output/biasAdam/v/output/biastotalcount*4
Tin-
+2)*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *1
config_proto!

CPU

GPU (2J 8� *+
f&R$
"__inference__traced_restore_669423��
�
�
'__inference_layer3_layer_call_fn_668981

inputs
unknown:(
	unknown_0:(
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������(*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_layer3_layer_call_and_return_conditional_losses_668661o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������(<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:&"
 
_user_specified_name668977:&"
 
_user_specified_name668975:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�

�
B__inference_layer1_layer_call_and_return_conditional_losses_668932

inputs0
matmul_readvariableop_resource:dP-
biasadd_readvariableop_resource:P
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:dP*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Pr
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:P*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������PQ
	LeakyRelu	LeakyReluBiasAdd:output:0*'
_output_shapes
:���������Pf
IdentityIdentityLeakyRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������PS
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������d: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:O K
'
_output_shapes
:���������d
 
_user_specified_nameinputs
�
�
'__inference_output_layer_call_fn_669021

inputs
unknown:Pd
	unknown_0:d
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������d*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_output_layer_call_and_return_conditional_losses_668693o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������d<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������P: : 22
StatefulPartitionedCallStatefulPartitionedCall:&"
 
_user_specified_name669017:&"
 
_user_specified_name669015:O K
'
_output_shapes
:���������P
 
_user_specified_nameinputs
�

�
B__inference_latent_layer_call_and_return_conditional_losses_668645

inputs0
matmul_readvariableop_resource:(-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:(*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Q
	LeakyRelu	LeakyReluBiasAdd:output:0*'
_output_shapes
:���������f
IdentityIdentityLeakyRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������S
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������(: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:O K
'
_output_shapes
:���������(
 
_user_specified_nameinputs
�
�
'__inference_layer2_layer_call_fn_668941

inputs
unknown:P(
	unknown_0:(
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������(*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_layer2_layer_call_and_return_conditional_losses_668629o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������(<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������P: : 22
StatefulPartitionedCallStatefulPartitionedCall:&"
 
_user_specified_name668937:&"
 
_user_specified_name668935:O K
'
_output_shapes
:���������P
 
_user_specified_nameinputs
�

�
B__inference_layer1_layer_call_and_return_conditional_losses_668605

inputs0
matmul_readvariableop_resource:dP-
biasadd_readvariableop_resource:P
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:dP*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Pr
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:P*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������PQ
	LeakyRelu	LeakyReluBiasAdd:output:0*'
_output_shapes
:���������Pf
IdentityIdentityLeakyRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������PS
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������d: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:O K
'
_output_shapes
:���������d
 
_user_specified_nameinputs
�
�
&__inference_model_layer_call_fn_668774	
input
unknown:dP
	unknown_0:P
	unknown_1:P(
	unknown_2:(
	unknown_3:(
	unknown_4:
	unknown_5:(
	unknown_6:(
	unknown_7:(P
	unknown_8:P
	unknown_9:Pd

unknown_10:d
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 *)
_output_shapes
:���������d: *.
_read_only_resource_inputs
	
*1
config_proto!

CPU

GPU (2J 8� *J
fERC
A__inference_model_layer_call_and_return_conditional_losses_668701o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������d<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*>
_input_shapes-
+:���������d: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:&"
 
_user_specified_name668769:&"
 
_user_specified_name668767:&
"
 
_user_specified_name668765:&	"
 
_user_specified_name668763:&"
 
_user_specified_name668761:&"
 
_user_specified_name668759:&"
 
_user_specified_name668757:&"
 
_user_specified_name668755:&"
 
_user_specified_name668753:&"
 
_user_specified_name668751:&"
 
_user_specified_name668749:&"
 
_user_specified_name668747:N J
'
_output_shapes
:���������d

_user_specified_nameinput
�
�
'__inference_layer1_layer_call_fn_668910

inputs
unknown:dP
	unknown_0:P
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������P*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_layer1_layer_call_and_return_conditional_losses_668605o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������P<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������d: : 22
StatefulPartitionedCallStatefulPartitionedCall:&"
 
_user_specified_name668906:&"
 
_user_specified_name668904:O K
'
_output_shapes
:���������d
 
_user_specified_nameinputs
�

�
B__inference_layer2_layer_call_and_return_conditional_losses_668629

inputs0
matmul_readvariableop_resource:P(-
biasadd_readvariableop_resource:(
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:P(*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������(r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:(*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������(Q
	LeakyRelu	LeakyReluBiasAdd:output:0*'
_output_shapes
:���������(f
IdentityIdentityLeakyRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������(S
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������P: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:O K
'
_output_shapes
:���������P
 
_user_specified_nameinputs
�
�
"__inference__traced_restore_669423
file_prefix0
assignvariableop_layer1_kernel:dP,
assignvariableop_1_layer1_bias:P2
 assignvariableop_2_layer2_kernel:P(,
assignvariableop_3_layer2_bias:(2
 assignvariableop_4_latent_kernel:(,
assignvariableop_5_latent_bias:2
 assignvariableop_6_layer3_kernel:(,
assignvariableop_7_layer3_bias:(2
 assignvariableop_8_layer4_kernel:(P,
assignvariableop_9_layer4_bias:P3
!assignvariableop_10_output_kernel:Pd-
assignvariableop_11_output_bias:d'
assignvariableop_12_iteration:	 +
!assignvariableop_13_learning_rate: :
(assignvariableop_14_adam_m_layer1_kernel:dP:
(assignvariableop_15_adam_v_layer1_kernel:dP4
&assignvariableop_16_adam_m_layer1_bias:P4
&assignvariableop_17_adam_v_layer1_bias:P:
(assignvariableop_18_adam_m_layer2_kernel:P(:
(assignvariableop_19_adam_v_layer2_kernel:P(4
&assignvariableop_20_adam_m_layer2_bias:(4
&assignvariableop_21_adam_v_layer2_bias:(:
(assignvariableop_22_adam_m_latent_kernel:(:
(assignvariableop_23_adam_v_latent_kernel:(4
&assignvariableop_24_adam_m_latent_bias:4
&assignvariableop_25_adam_v_latent_bias::
(assignvariableop_26_adam_m_layer3_kernel:(:
(assignvariableop_27_adam_v_layer3_kernel:(4
&assignvariableop_28_adam_m_layer3_bias:(4
&assignvariableop_29_adam_v_layer3_bias:(:
(assignvariableop_30_adam_m_layer4_kernel:(P:
(assignvariableop_31_adam_v_layer4_kernel:(P4
&assignvariableop_32_adam_m_layer4_bias:P4
&assignvariableop_33_adam_v_layer4_bias:P:
(assignvariableop_34_adam_m_output_kernel:Pd:
(assignvariableop_35_adam_v_output_kernel:Pd4
&assignvariableop_36_adam_m_output_bias:d4
&assignvariableop_37_adam_v_output_bias:d#
assignvariableop_38_total: #
assignvariableop_39_count: 
identity_41��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_33�AssignVariableOp_34�AssignVariableOp_35�AssignVariableOp_36�AssignVariableOp_37�AssignVariableOp_38�AssignVariableOp_39�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:)*
dtype0*�
value�B�)B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB0optimizer/_iterations/.ATTRIBUTES/VARIABLE_VALUEB3optimizer/_learning_rate/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/1/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/2/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/3/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/4/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/5/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/6/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/7/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/8/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/9/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/10/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/11/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/12/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/13/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/14/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/15/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/16/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/17/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/18/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/19/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/20/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/21/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/22/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/23/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/24/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:)*
dtype0*e
value\BZ)B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B �
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�:::::::::::::::::::::::::::::::::::::::::*7
dtypes-
+2)	[
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOpAssignVariableOpassignvariableop_layer1_kernelIdentity:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_1AssignVariableOpassignvariableop_1_layer1_biasIdentity_1:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_2AssignVariableOp assignvariableop_2_layer2_kernelIdentity_2:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_3AssignVariableOpassignvariableop_3_layer2_biasIdentity_3:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_4AssignVariableOp assignvariableop_4_latent_kernelIdentity_4:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_5AssignVariableOpassignvariableop_5_latent_biasIdentity_5:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_6AssignVariableOp assignvariableop_6_layer3_kernelIdentity_6:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_7AssignVariableOpassignvariableop_7_layer3_biasIdentity_7:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_8AssignVariableOp assignvariableop_8_layer4_kernelIdentity_8:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_9AssignVariableOpassignvariableop_9_layer4_biasIdentity_9:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_10AssignVariableOp!assignvariableop_10_output_kernelIdentity_10:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_11AssignVariableOpassignvariableop_11_output_biasIdentity_11:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0	*
_output_shapes
:�
AssignVariableOp_12AssignVariableOpassignvariableop_12_iterationIdentity_12:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0	_
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_13AssignVariableOp!assignvariableop_13_learning_rateIdentity_13:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_14AssignVariableOp(assignvariableop_14_adam_m_layer1_kernelIdentity_14:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_15AssignVariableOp(assignvariableop_15_adam_v_layer1_kernelIdentity_15:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_16AssignVariableOp&assignvariableop_16_adam_m_layer1_biasIdentity_16:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_17AssignVariableOp&assignvariableop_17_adam_v_layer1_biasIdentity_17:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_18AssignVariableOp(assignvariableop_18_adam_m_layer2_kernelIdentity_18:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_19AssignVariableOp(assignvariableop_19_adam_v_layer2_kernelIdentity_19:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_20AssignVariableOp&assignvariableop_20_adam_m_layer2_biasIdentity_20:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_21AssignVariableOp&assignvariableop_21_adam_v_layer2_biasIdentity_21:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_22AssignVariableOp(assignvariableop_22_adam_m_latent_kernelIdentity_22:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_23AssignVariableOp(assignvariableop_23_adam_v_latent_kernelIdentity_23:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_24AssignVariableOp&assignvariableop_24_adam_m_latent_biasIdentity_24:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_25AssignVariableOp&assignvariableop_25_adam_v_latent_biasIdentity_25:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_26AssignVariableOp(assignvariableop_26_adam_m_layer3_kernelIdentity_26:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_27AssignVariableOp(assignvariableop_27_adam_v_layer3_kernelIdentity_27:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_28AssignVariableOp&assignvariableop_28_adam_m_layer3_biasIdentity_28:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_29AssignVariableOp&assignvariableop_29_adam_v_layer3_biasIdentity_29:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_30IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_30AssignVariableOp(assignvariableop_30_adam_m_layer4_kernelIdentity_30:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_31IdentityRestoreV2:tensors:31"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_31AssignVariableOp(assignvariableop_31_adam_v_layer4_kernelIdentity_31:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_32IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_32AssignVariableOp&assignvariableop_32_adam_m_layer4_biasIdentity_32:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_33IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_33AssignVariableOp&assignvariableop_33_adam_v_layer4_biasIdentity_33:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_34IdentityRestoreV2:tensors:34"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_34AssignVariableOp(assignvariableop_34_adam_m_output_kernelIdentity_34:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_35IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_35AssignVariableOp(assignvariableop_35_adam_v_output_kernelIdentity_35:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_36IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_36AssignVariableOp&assignvariableop_36_adam_m_output_biasIdentity_36:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_37IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_37AssignVariableOp&assignvariableop_37_adam_v_output_biasIdentity_37:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_38IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_38AssignVariableOpassignvariableop_38_totalIdentity_38:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_39IdentityRestoreV2:tensors:39"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_39AssignVariableOpassignvariableop_39_countIdentity_39:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0Y
NoOpNoOp"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 �
Identity_40Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: W
Identity_41IdentityIdentity_40:output:0^NoOp_1*
T0*
_output_shapes
: �
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*
_output_shapes
 "#
identity_41Identity_41:output:0*(
_construction_contextkEagerRuntime*e
_input_shapesT
R: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92$
AssignVariableOpAssignVariableOp:%(!

_user_specified_namecount:%'!

_user_specified_nametotal:2&.
,
_user_specified_nameAdam/v/output/bias:2%.
,
_user_specified_nameAdam/m/output/bias:4$0
.
_user_specified_nameAdam/v/output/kernel:4#0
.
_user_specified_nameAdam/m/output/kernel:2".
,
_user_specified_nameAdam/v/layer4/bias:2!.
,
_user_specified_nameAdam/m/layer4/bias:4 0
.
_user_specified_nameAdam/v/layer4/kernel:40
.
_user_specified_nameAdam/m/layer4/kernel:2.
,
_user_specified_nameAdam/v/layer3/bias:2.
,
_user_specified_nameAdam/m/layer3/bias:40
.
_user_specified_nameAdam/v/layer3/kernel:40
.
_user_specified_nameAdam/m/layer3/kernel:2.
,
_user_specified_nameAdam/v/latent/bias:2.
,
_user_specified_nameAdam/m/latent/bias:40
.
_user_specified_nameAdam/v/latent/kernel:40
.
_user_specified_nameAdam/m/latent/kernel:2.
,
_user_specified_nameAdam/v/layer2/bias:2.
,
_user_specified_nameAdam/m/layer2/bias:40
.
_user_specified_nameAdam/v/layer2/kernel:40
.
_user_specified_nameAdam/m/layer2/kernel:2.
,
_user_specified_nameAdam/v/layer1/bias:2.
,
_user_specified_nameAdam/m/layer1/bias:40
.
_user_specified_nameAdam/v/layer1/kernel:40
.
_user_specified_nameAdam/m/layer1/kernel:-)
'
_user_specified_namelearning_rate:)%
#
_user_specified_name	iteration:+'
%
_user_specified_nameoutput/bias:-)
'
_user_specified_nameoutput/kernel:+
'
%
_user_specified_namelayer4/bias:-	)
'
_user_specified_namelayer4/kernel:+'
%
_user_specified_namelayer3/bias:-)
'
_user_specified_namelayer3/kernel:+'
%
_user_specified_namelatent/bias:-)
'
_user_specified_namelatent/kernel:+'
%
_user_specified_namelayer2/bias:-)
'
_user_specified_namelayer2/kernel:+'
%
_user_specified_namelayer1/bias:-)
'
_user_specified_namelayer1/kernel:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�

�
B__inference_latent_layer_call_and_return_conditional_losses_668972

inputs0
matmul_readvariableop_resource:(-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:(*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Q
	LeakyRelu	LeakyReluBiasAdd:output:0*'
_output_shapes
:���������f
IdentityIdentityLeakyRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������S
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������(: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:O K
'
_output_shapes
:���������(
 
_user_specified_nameinputs
�
E
.__inference_layer1_activity_regularizer_668592
x
identity4
L2LossL2Lossx*
T0*
_output_shapes
: J
mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *���3L
mulMulmul/x:output:0L2Loss:output:0*
T0*
_output_shapes
: >
IdentityIdentitymul:z:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
::; 7

_output_shapes
:

_user_specified_namex
�

�
B__inference_output_layer_call_and_return_conditional_losses_668693

inputs0
matmul_readvariableop_resource:Pd-
biasadd_readvariableop_resource:d
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:Pd*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������dr
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:d*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������dQ
	LeakyRelu	LeakyReluBiasAdd:output:0*'
_output_shapes
:���������df
IdentityIdentityLeakyRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������dS
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������P: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:O K
'
_output_shapes
:���������P
 
_user_specified_nameinputs
�

�
B__inference_layer3_layer_call_and_return_conditional_losses_668661

inputs0
matmul_readvariableop_resource:(-
biasadd_readvariableop_resource:(
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:(*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������(r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:(*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������(Q
	LeakyRelu	LeakyReluBiasAdd:output:0*'
_output_shapes
:���������(f
IdentityIdentityLeakyRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������(S
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�

�
B__inference_layer4_layer_call_and_return_conditional_losses_669012

inputs0
matmul_readvariableop_resource:(P-
biasadd_readvariableop_resource:P
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:(P*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Pr
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:P*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������PQ
	LeakyRelu	LeakyReluBiasAdd:output:0*'
_output_shapes
:���������Pf
IdentityIdentityLeakyRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������PS
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������(: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:O K
'
_output_shapes
:���������(
 
_user_specified_nameinputs
�

�
B__inference_output_layer_call_and_return_conditional_losses_669032

inputs0
matmul_readvariableop_resource:Pd-
biasadd_readvariableop_resource:d
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:Pd*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������dr
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:d*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������dQ
	LeakyRelu	LeakyReluBiasAdd:output:0*'
_output_shapes
:���������df
IdentityIdentityLeakyRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������dS
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������P: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:O K
'
_output_shapes
:���������P
 
_user_specified_nameinputs
�
�
'__inference_latent_layer_call_fn_668961

inputs
unknown:(
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_latent_layer_call_and_return_conditional_losses_668645o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������(: : 22
StatefulPartitionedCallStatefulPartitionedCall:&"
 
_user_specified_name668957:&"
 
_user_specified_name668955:O K
'
_output_shapes
:���������(
 
_user_specified_nameinputs
�
�
F__inference_layer1_layer_call_and_return_all_conditional_losses_668921

inputs
unknown:dP
	unknown_0:P
identity

identity_1��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������P*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_layer1_layer_call_and_return_conditional_losses_668605�
PartitionedCallPartitionedCall StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *1
config_proto!

CPU

GPU (2J 8� *7
f2R0
.__inference_layer1_activity_regularizer_668592o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������PX

Identity_1IdentityPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: <
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "!

identity_1Identity_1:output:0"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������d: : 22
StatefulPartitionedCallStatefulPartitionedCall:&"
 
_user_specified_name668915:&"
 
_user_specified_name668913:O K
'
_output_shapes
:���������d
 
_user_specified_nameinputs
�
�
&__inference_model_layer_call_fn_668804	
input
unknown:dP
	unknown_0:P
	unknown_1:P(
	unknown_2:(
	unknown_3:(
	unknown_4:
	unknown_5:(
	unknown_6:(
	unknown_7:(P
	unknown_8:P
	unknown_9:Pd

unknown_10:d
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 *)
_output_shapes
:���������d: *.
_read_only_resource_inputs
	
*1
config_proto!

CPU

GPU (2J 8� *J
fERC
A__inference_model_layer_call_and_return_conditional_losses_668744o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������d<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*>
_input_shapes-
+:���������d: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:&"
 
_user_specified_name668799:&"
 
_user_specified_name668797:&
"
 
_user_specified_name668795:&	"
 
_user_specified_name668793:&"
 
_user_specified_name668791:&"
 
_user_specified_name668789:&"
 
_user_specified_name668787:&"
 
_user_specified_name668785:&"
 
_user_specified_name668783:&"
 
_user_specified_name668781:&"
 
_user_specified_name668779:&"
 
_user_specified_name668777:N J
'
_output_shapes
:���������d

_user_specified_nameinput
�

�
B__inference_layer3_layer_call_and_return_conditional_losses_668992

inputs0
matmul_readvariableop_resource:(-
biasadd_readvariableop_resource:(
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:(*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������(r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:(*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������(Q
	LeakyRelu	LeakyReluBiasAdd:output:0*'
_output_shapes
:���������(f
IdentityIdentityLeakyRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������(S
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�

�
B__inference_layer2_layer_call_and_return_conditional_losses_668952

inputs0
matmul_readvariableop_resource:P(-
biasadd_readvariableop_resource:(
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:P(*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������(r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:(*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������(Q
	LeakyRelu	LeakyReluBiasAdd:output:0*'
_output_shapes
:���������(f
IdentityIdentityLeakyRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������(S
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������P: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:O K
'
_output_shapes
:���������P
 
_user_specified_nameinputs
�/
�
A__inference_model_layer_call_and_return_conditional_losses_668701	
input
layer1_668606:dP
layer1_668608:P
layer2_668630:P(
layer2_668632:(
latent_668646:(
latent_668648:
layer3_668662:(
layer3_668664:(
layer4_668678:(P
layer4_668680:P
output_668694:Pd
output_668696:d
identity

identity_1��latent/StatefulPartitionedCall�layer1/StatefulPartitionedCall�layer2/StatefulPartitionedCall�layer3/StatefulPartitionedCall�layer4/StatefulPartitionedCall�output/StatefulPartitionedCall�
layer1/StatefulPartitionedCallStatefulPartitionedCallinputlayer1_668606layer1_668608*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������P*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_layer1_layer_call_and_return_conditional_losses_668605�
*layer1/ActivityRegularizer/PartitionedCallPartitionedCall'layer1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *1
config_proto!

CPU

GPU (2J 8� *7
f2R0
.__inference_layer1_activity_regularizer_668592�
 layer1/ActivityRegularizer/ShapeShape'layer1/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��x
.layer1/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: z
0layer1/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:z
0layer1/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
(layer1/ActivityRegularizer/strided_sliceStridedSlice)layer1/ActivityRegularizer/Shape:output:07layer1/ActivityRegularizer/strided_slice/stack:output:09layer1/ActivityRegularizer/strided_slice/stack_1:output:09layer1/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
layer1/ActivityRegularizer/CastCast1layer1/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
"layer1/ActivityRegularizer/truedivRealDiv3layer1/ActivityRegularizer/PartitionedCall:output:0#layer1/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
layer2/StatefulPartitionedCallStatefulPartitionedCall'layer1/StatefulPartitionedCall:output:0layer2_668630layer2_668632*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������(*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_layer2_layer_call_and_return_conditional_losses_668629�
latent/StatefulPartitionedCallStatefulPartitionedCall'layer2/StatefulPartitionedCall:output:0latent_668646latent_668648*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_latent_layer_call_and_return_conditional_losses_668645�
layer3/StatefulPartitionedCallStatefulPartitionedCall'latent/StatefulPartitionedCall:output:0layer3_668662layer3_668664*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������(*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_layer3_layer_call_and_return_conditional_losses_668661�
layer4/StatefulPartitionedCallStatefulPartitionedCall'layer3/StatefulPartitionedCall:output:0layer4_668678layer4_668680*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������P*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_layer4_layer_call_and_return_conditional_losses_668677�
output/StatefulPartitionedCallStatefulPartitionedCall'layer4/StatefulPartitionedCall:output:0output_668694output_668696*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������d*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_output_layer_call_and_return_conditional_losses_668693v
IdentityIdentity'output/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������df

Identity_1Identity&layer1/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: �
NoOpNoOp^latent/StatefulPartitionedCall^layer1/StatefulPartitionedCall^layer2/StatefulPartitionedCall^layer3/StatefulPartitionedCall^layer4/StatefulPartitionedCall^output/StatefulPartitionedCall*
_output_shapes
 "!

identity_1Identity_1:output:0"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*>
_input_shapes-
+:���������d: : : : : : : : : : : : 2@
latent/StatefulPartitionedCalllatent/StatefulPartitionedCall2@
layer1/StatefulPartitionedCalllayer1/StatefulPartitionedCall2@
layer2/StatefulPartitionedCalllayer2/StatefulPartitionedCall2@
layer3/StatefulPartitionedCalllayer3/StatefulPartitionedCall2@
layer4/StatefulPartitionedCalllayer4/StatefulPartitionedCall2@
output/StatefulPartitionedCalloutput/StatefulPartitionedCall:&"
 
_user_specified_name668696:&"
 
_user_specified_name668694:&
"
 
_user_specified_name668680:&	"
 
_user_specified_name668678:&"
 
_user_specified_name668664:&"
 
_user_specified_name668662:&"
 
_user_specified_name668648:&"
 
_user_specified_name668646:&"
 
_user_specified_name668632:&"
 
_user_specified_name668630:&"
 
_user_specified_name668608:&"
 
_user_specified_name668606:N J
'
_output_shapes
:���������d

_user_specified_nameinput
�

�
B__inference_layer4_layer_call_and_return_conditional_losses_668677

inputs0
matmul_readvariableop_resource:(P-
biasadd_readvariableop_resource:P
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:(P*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Pr
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:P*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������PQ
	LeakyRelu	LeakyReluBiasAdd:output:0*'
_output_shapes
:���������Pf
IdentityIdentityLeakyRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������PS
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������(: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:O K
'
_output_shapes
:���������(
 
_user_specified_nameinputs
�
�
'__inference_layer4_layer_call_fn_669001

inputs
unknown:(P
	unknown_0:P
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������P*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_layer4_layer_call_and_return_conditional_losses_668677o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������P<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������(: : 22
StatefulPartitionedCallStatefulPartitionedCall:&"
 
_user_specified_name668997:&"
 
_user_specified_name668995:O K
'
_output_shapes
:���������(
 
_user_specified_nameinputs
�
�
$__inference_signature_wrapper_668901	
input
unknown:dP
	unknown_0:P
	unknown_1:P(
	unknown_2:(
	unknown_3:(
	unknown_4:
	unknown_5:(
	unknown_6:(
	unknown_7:(P
	unknown_8:P
	unknown_9:Pd

unknown_10:d
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������d*.
_read_only_resource_inputs
	
*1
config_proto!

CPU

GPU (2J 8� **
f%R#
!__inference__wrapped_model_668585o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������d<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*>
_input_shapes-
+:���������d: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:&"
 
_user_specified_name668897:&"
 
_user_specified_name668895:&
"
 
_user_specified_name668893:&	"
 
_user_specified_name668891:&"
 
_user_specified_name668889:&"
 
_user_specified_name668887:&"
 
_user_specified_name668885:&"
 
_user_specified_name668883:&"
 
_user_specified_name668881:&"
 
_user_specified_name668879:&"
 
_user_specified_name668877:&"
 
_user_specified_name668875:N J
'
_output_shapes
:���������d

_user_specified_nameinput
��
�#
__inference__traced_save_669294
file_prefix6
$read_disablecopyonread_layer1_kernel:dP2
$read_1_disablecopyonread_layer1_bias:P8
&read_2_disablecopyonread_layer2_kernel:P(2
$read_3_disablecopyonread_layer2_bias:(8
&read_4_disablecopyonread_latent_kernel:(2
$read_5_disablecopyonread_latent_bias:8
&read_6_disablecopyonread_layer3_kernel:(2
$read_7_disablecopyonread_layer3_bias:(8
&read_8_disablecopyonread_layer4_kernel:(P2
$read_9_disablecopyonread_layer4_bias:P9
'read_10_disablecopyonread_output_kernel:Pd3
%read_11_disablecopyonread_output_bias:d-
#read_12_disablecopyonread_iteration:	 1
'read_13_disablecopyonread_learning_rate: @
.read_14_disablecopyonread_adam_m_layer1_kernel:dP@
.read_15_disablecopyonread_adam_v_layer1_kernel:dP:
,read_16_disablecopyonread_adam_m_layer1_bias:P:
,read_17_disablecopyonread_adam_v_layer1_bias:P@
.read_18_disablecopyonread_adam_m_layer2_kernel:P(@
.read_19_disablecopyonread_adam_v_layer2_kernel:P(:
,read_20_disablecopyonread_adam_m_layer2_bias:(:
,read_21_disablecopyonread_adam_v_layer2_bias:(@
.read_22_disablecopyonread_adam_m_latent_kernel:(@
.read_23_disablecopyonread_adam_v_latent_kernel:(:
,read_24_disablecopyonread_adam_m_latent_bias::
,read_25_disablecopyonread_adam_v_latent_bias:@
.read_26_disablecopyonread_adam_m_layer3_kernel:(@
.read_27_disablecopyonread_adam_v_layer3_kernel:(:
,read_28_disablecopyonread_adam_m_layer3_bias:(:
,read_29_disablecopyonread_adam_v_layer3_bias:(@
.read_30_disablecopyonread_adam_m_layer4_kernel:(P@
.read_31_disablecopyonread_adam_v_layer4_kernel:(P:
,read_32_disablecopyonread_adam_m_layer4_bias:P:
,read_33_disablecopyonread_adam_v_layer4_bias:P@
.read_34_disablecopyonread_adam_m_output_kernel:Pd@
.read_35_disablecopyonread_adam_v_output_kernel:Pd:
,read_36_disablecopyonread_adam_m_output_bias:d:
,read_37_disablecopyonread_adam_v_output_bias:d)
read_38_disablecopyonread_total: )
read_39_disablecopyonread_count: 
savev2_const
identity_81��MergeV2Checkpoints�Read/DisableCopyOnRead�Read/ReadVariableOp�Read_1/DisableCopyOnRead�Read_1/ReadVariableOp�Read_10/DisableCopyOnRead�Read_10/ReadVariableOp�Read_11/DisableCopyOnRead�Read_11/ReadVariableOp�Read_12/DisableCopyOnRead�Read_12/ReadVariableOp�Read_13/DisableCopyOnRead�Read_13/ReadVariableOp�Read_14/DisableCopyOnRead�Read_14/ReadVariableOp�Read_15/DisableCopyOnRead�Read_15/ReadVariableOp�Read_16/DisableCopyOnRead�Read_16/ReadVariableOp�Read_17/DisableCopyOnRead�Read_17/ReadVariableOp�Read_18/DisableCopyOnRead�Read_18/ReadVariableOp�Read_19/DisableCopyOnRead�Read_19/ReadVariableOp�Read_2/DisableCopyOnRead�Read_2/ReadVariableOp�Read_20/DisableCopyOnRead�Read_20/ReadVariableOp�Read_21/DisableCopyOnRead�Read_21/ReadVariableOp�Read_22/DisableCopyOnRead�Read_22/ReadVariableOp�Read_23/DisableCopyOnRead�Read_23/ReadVariableOp�Read_24/DisableCopyOnRead�Read_24/ReadVariableOp�Read_25/DisableCopyOnRead�Read_25/ReadVariableOp�Read_26/DisableCopyOnRead�Read_26/ReadVariableOp�Read_27/DisableCopyOnRead�Read_27/ReadVariableOp�Read_28/DisableCopyOnRead�Read_28/ReadVariableOp�Read_29/DisableCopyOnRead�Read_29/ReadVariableOp�Read_3/DisableCopyOnRead�Read_3/ReadVariableOp�Read_30/DisableCopyOnRead�Read_30/ReadVariableOp�Read_31/DisableCopyOnRead�Read_31/ReadVariableOp�Read_32/DisableCopyOnRead�Read_32/ReadVariableOp�Read_33/DisableCopyOnRead�Read_33/ReadVariableOp�Read_34/DisableCopyOnRead�Read_34/ReadVariableOp�Read_35/DisableCopyOnRead�Read_35/ReadVariableOp�Read_36/DisableCopyOnRead�Read_36/ReadVariableOp�Read_37/DisableCopyOnRead�Read_37/ReadVariableOp�Read_38/DisableCopyOnRead�Read_38/ReadVariableOp�Read_39/DisableCopyOnRead�Read_39/ReadVariableOp�Read_4/DisableCopyOnRead�Read_4/ReadVariableOp�Read_5/DisableCopyOnRead�Read_5/ReadVariableOp�Read_6/DisableCopyOnRead�Read_6/ReadVariableOp�Read_7/DisableCopyOnRead�Read_7/ReadVariableOp�Read_8/DisableCopyOnRead�Read_8/ReadVariableOp�Read_9/DisableCopyOnRead�Read_9/ReadVariableOpw
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*Z
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.parta
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: f

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: L

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :f
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : �
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: v
Read/DisableCopyOnReadDisableCopyOnRead$read_disablecopyonread_layer1_kernel"/device:CPU:0*
_output_shapes
 �
Read/ReadVariableOpReadVariableOp$read_disablecopyonread_layer1_kernel^Read/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:dP*
dtype0i
IdentityIdentityRead/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:dPa

Identity_1IdentityIdentity:output:0"/device:CPU:0*
T0*
_output_shapes

:dPx
Read_1/DisableCopyOnReadDisableCopyOnRead$read_1_disablecopyonread_layer1_bias"/device:CPU:0*
_output_shapes
 �
Read_1/ReadVariableOpReadVariableOp$read_1_disablecopyonread_layer1_bias^Read_1/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:P*
dtype0i

Identity_2IdentityRead_1/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:P_

Identity_3IdentityIdentity_2:output:0"/device:CPU:0*
T0*
_output_shapes
:Pz
Read_2/DisableCopyOnReadDisableCopyOnRead&read_2_disablecopyonread_layer2_kernel"/device:CPU:0*
_output_shapes
 �
Read_2/ReadVariableOpReadVariableOp&read_2_disablecopyonread_layer2_kernel^Read_2/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:P(*
dtype0m

Identity_4IdentityRead_2/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:P(c

Identity_5IdentityIdentity_4:output:0"/device:CPU:0*
T0*
_output_shapes

:P(x
Read_3/DisableCopyOnReadDisableCopyOnRead$read_3_disablecopyonread_layer2_bias"/device:CPU:0*
_output_shapes
 �
Read_3/ReadVariableOpReadVariableOp$read_3_disablecopyonread_layer2_bias^Read_3/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:(*
dtype0i

Identity_6IdentityRead_3/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:(_

Identity_7IdentityIdentity_6:output:0"/device:CPU:0*
T0*
_output_shapes
:(z
Read_4/DisableCopyOnReadDisableCopyOnRead&read_4_disablecopyonread_latent_kernel"/device:CPU:0*
_output_shapes
 �
Read_4/ReadVariableOpReadVariableOp&read_4_disablecopyonread_latent_kernel^Read_4/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:(*
dtype0m

Identity_8IdentityRead_4/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:(c

Identity_9IdentityIdentity_8:output:0"/device:CPU:0*
T0*
_output_shapes

:(x
Read_5/DisableCopyOnReadDisableCopyOnRead$read_5_disablecopyonread_latent_bias"/device:CPU:0*
_output_shapes
 �
Read_5/ReadVariableOpReadVariableOp$read_5_disablecopyonread_latent_bias^Read_5/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0j
Identity_10IdentityRead_5/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_11IdentityIdentity_10:output:0"/device:CPU:0*
T0*
_output_shapes
:z
Read_6/DisableCopyOnReadDisableCopyOnRead&read_6_disablecopyonread_layer3_kernel"/device:CPU:0*
_output_shapes
 �
Read_6/ReadVariableOpReadVariableOp&read_6_disablecopyonread_layer3_kernel^Read_6/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:(*
dtype0n
Identity_12IdentityRead_6/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:(e
Identity_13IdentityIdentity_12:output:0"/device:CPU:0*
T0*
_output_shapes

:(x
Read_7/DisableCopyOnReadDisableCopyOnRead$read_7_disablecopyonread_layer3_bias"/device:CPU:0*
_output_shapes
 �
Read_7/ReadVariableOpReadVariableOp$read_7_disablecopyonread_layer3_bias^Read_7/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:(*
dtype0j
Identity_14IdentityRead_7/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:(a
Identity_15IdentityIdentity_14:output:0"/device:CPU:0*
T0*
_output_shapes
:(z
Read_8/DisableCopyOnReadDisableCopyOnRead&read_8_disablecopyonread_layer4_kernel"/device:CPU:0*
_output_shapes
 �
Read_8/ReadVariableOpReadVariableOp&read_8_disablecopyonread_layer4_kernel^Read_8/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:(P*
dtype0n
Identity_16IdentityRead_8/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:(Pe
Identity_17IdentityIdentity_16:output:0"/device:CPU:0*
T0*
_output_shapes

:(Px
Read_9/DisableCopyOnReadDisableCopyOnRead$read_9_disablecopyonread_layer4_bias"/device:CPU:0*
_output_shapes
 �
Read_9/ReadVariableOpReadVariableOp$read_9_disablecopyonread_layer4_bias^Read_9/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:P*
dtype0j
Identity_18IdentityRead_9/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:Pa
Identity_19IdentityIdentity_18:output:0"/device:CPU:0*
T0*
_output_shapes
:P|
Read_10/DisableCopyOnReadDisableCopyOnRead'read_10_disablecopyonread_output_kernel"/device:CPU:0*
_output_shapes
 �
Read_10/ReadVariableOpReadVariableOp'read_10_disablecopyonread_output_kernel^Read_10/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:Pd*
dtype0o
Identity_20IdentityRead_10/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:Pde
Identity_21IdentityIdentity_20:output:0"/device:CPU:0*
T0*
_output_shapes

:Pdz
Read_11/DisableCopyOnReadDisableCopyOnRead%read_11_disablecopyonread_output_bias"/device:CPU:0*
_output_shapes
 �
Read_11/ReadVariableOpReadVariableOp%read_11_disablecopyonread_output_bias^Read_11/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:d*
dtype0k
Identity_22IdentityRead_11/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:da
Identity_23IdentityIdentity_22:output:0"/device:CPU:0*
T0*
_output_shapes
:dx
Read_12/DisableCopyOnReadDisableCopyOnRead#read_12_disablecopyonread_iteration"/device:CPU:0*
_output_shapes
 �
Read_12/ReadVariableOpReadVariableOp#read_12_disablecopyonread_iteration^Read_12/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0	g
Identity_24IdentityRead_12/ReadVariableOp:value:0"/device:CPU:0*
T0	*
_output_shapes
: ]
Identity_25IdentityIdentity_24:output:0"/device:CPU:0*
T0	*
_output_shapes
: |
Read_13/DisableCopyOnReadDisableCopyOnRead'read_13_disablecopyonread_learning_rate"/device:CPU:0*
_output_shapes
 �
Read_13/ReadVariableOpReadVariableOp'read_13_disablecopyonread_learning_rate^Read_13/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0g
Identity_26IdentityRead_13/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: ]
Identity_27IdentityIdentity_26:output:0"/device:CPU:0*
T0*
_output_shapes
: �
Read_14/DisableCopyOnReadDisableCopyOnRead.read_14_disablecopyonread_adam_m_layer1_kernel"/device:CPU:0*
_output_shapes
 �
Read_14/ReadVariableOpReadVariableOp.read_14_disablecopyonread_adam_m_layer1_kernel^Read_14/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:dP*
dtype0o
Identity_28IdentityRead_14/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:dPe
Identity_29IdentityIdentity_28:output:0"/device:CPU:0*
T0*
_output_shapes

:dP�
Read_15/DisableCopyOnReadDisableCopyOnRead.read_15_disablecopyonread_adam_v_layer1_kernel"/device:CPU:0*
_output_shapes
 �
Read_15/ReadVariableOpReadVariableOp.read_15_disablecopyonread_adam_v_layer1_kernel^Read_15/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:dP*
dtype0o
Identity_30IdentityRead_15/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:dPe
Identity_31IdentityIdentity_30:output:0"/device:CPU:0*
T0*
_output_shapes

:dP�
Read_16/DisableCopyOnReadDisableCopyOnRead,read_16_disablecopyonread_adam_m_layer1_bias"/device:CPU:0*
_output_shapes
 �
Read_16/ReadVariableOpReadVariableOp,read_16_disablecopyonread_adam_m_layer1_bias^Read_16/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:P*
dtype0k
Identity_32IdentityRead_16/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:Pa
Identity_33IdentityIdentity_32:output:0"/device:CPU:0*
T0*
_output_shapes
:P�
Read_17/DisableCopyOnReadDisableCopyOnRead,read_17_disablecopyonread_adam_v_layer1_bias"/device:CPU:0*
_output_shapes
 �
Read_17/ReadVariableOpReadVariableOp,read_17_disablecopyonread_adam_v_layer1_bias^Read_17/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:P*
dtype0k
Identity_34IdentityRead_17/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:Pa
Identity_35IdentityIdentity_34:output:0"/device:CPU:0*
T0*
_output_shapes
:P�
Read_18/DisableCopyOnReadDisableCopyOnRead.read_18_disablecopyonread_adam_m_layer2_kernel"/device:CPU:0*
_output_shapes
 �
Read_18/ReadVariableOpReadVariableOp.read_18_disablecopyonread_adam_m_layer2_kernel^Read_18/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:P(*
dtype0o
Identity_36IdentityRead_18/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:P(e
Identity_37IdentityIdentity_36:output:0"/device:CPU:0*
T0*
_output_shapes

:P(�
Read_19/DisableCopyOnReadDisableCopyOnRead.read_19_disablecopyonread_adam_v_layer2_kernel"/device:CPU:0*
_output_shapes
 �
Read_19/ReadVariableOpReadVariableOp.read_19_disablecopyonread_adam_v_layer2_kernel^Read_19/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:P(*
dtype0o
Identity_38IdentityRead_19/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:P(e
Identity_39IdentityIdentity_38:output:0"/device:CPU:0*
T0*
_output_shapes

:P(�
Read_20/DisableCopyOnReadDisableCopyOnRead,read_20_disablecopyonread_adam_m_layer2_bias"/device:CPU:0*
_output_shapes
 �
Read_20/ReadVariableOpReadVariableOp,read_20_disablecopyonread_adam_m_layer2_bias^Read_20/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:(*
dtype0k
Identity_40IdentityRead_20/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:(a
Identity_41IdentityIdentity_40:output:0"/device:CPU:0*
T0*
_output_shapes
:(�
Read_21/DisableCopyOnReadDisableCopyOnRead,read_21_disablecopyonread_adam_v_layer2_bias"/device:CPU:0*
_output_shapes
 �
Read_21/ReadVariableOpReadVariableOp,read_21_disablecopyonread_adam_v_layer2_bias^Read_21/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:(*
dtype0k
Identity_42IdentityRead_21/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:(a
Identity_43IdentityIdentity_42:output:0"/device:CPU:0*
T0*
_output_shapes
:(�
Read_22/DisableCopyOnReadDisableCopyOnRead.read_22_disablecopyonread_adam_m_latent_kernel"/device:CPU:0*
_output_shapes
 �
Read_22/ReadVariableOpReadVariableOp.read_22_disablecopyonread_adam_m_latent_kernel^Read_22/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:(*
dtype0o
Identity_44IdentityRead_22/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:(e
Identity_45IdentityIdentity_44:output:0"/device:CPU:0*
T0*
_output_shapes

:(�
Read_23/DisableCopyOnReadDisableCopyOnRead.read_23_disablecopyonread_adam_v_latent_kernel"/device:CPU:0*
_output_shapes
 �
Read_23/ReadVariableOpReadVariableOp.read_23_disablecopyonread_adam_v_latent_kernel^Read_23/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:(*
dtype0o
Identity_46IdentityRead_23/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:(e
Identity_47IdentityIdentity_46:output:0"/device:CPU:0*
T0*
_output_shapes

:(�
Read_24/DisableCopyOnReadDisableCopyOnRead,read_24_disablecopyonread_adam_m_latent_bias"/device:CPU:0*
_output_shapes
 �
Read_24/ReadVariableOpReadVariableOp,read_24_disablecopyonread_adam_m_latent_bias^Read_24/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0k
Identity_48IdentityRead_24/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_49IdentityIdentity_48:output:0"/device:CPU:0*
T0*
_output_shapes
:�
Read_25/DisableCopyOnReadDisableCopyOnRead,read_25_disablecopyonread_adam_v_latent_bias"/device:CPU:0*
_output_shapes
 �
Read_25/ReadVariableOpReadVariableOp,read_25_disablecopyonread_adam_v_latent_bias^Read_25/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0k
Identity_50IdentityRead_25/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_51IdentityIdentity_50:output:0"/device:CPU:0*
T0*
_output_shapes
:�
Read_26/DisableCopyOnReadDisableCopyOnRead.read_26_disablecopyonread_adam_m_layer3_kernel"/device:CPU:0*
_output_shapes
 �
Read_26/ReadVariableOpReadVariableOp.read_26_disablecopyonread_adam_m_layer3_kernel^Read_26/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:(*
dtype0o
Identity_52IdentityRead_26/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:(e
Identity_53IdentityIdentity_52:output:0"/device:CPU:0*
T0*
_output_shapes

:(�
Read_27/DisableCopyOnReadDisableCopyOnRead.read_27_disablecopyonread_adam_v_layer3_kernel"/device:CPU:0*
_output_shapes
 �
Read_27/ReadVariableOpReadVariableOp.read_27_disablecopyonread_adam_v_layer3_kernel^Read_27/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:(*
dtype0o
Identity_54IdentityRead_27/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:(e
Identity_55IdentityIdentity_54:output:0"/device:CPU:0*
T0*
_output_shapes

:(�
Read_28/DisableCopyOnReadDisableCopyOnRead,read_28_disablecopyonread_adam_m_layer3_bias"/device:CPU:0*
_output_shapes
 �
Read_28/ReadVariableOpReadVariableOp,read_28_disablecopyonread_adam_m_layer3_bias^Read_28/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:(*
dtype0k
Identity_56IdentityRead_28/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:(a
Identity_57IdentityIdentity_56:output:0"/device:CPU:0*
T0*
_output_shapes
:(�
Read_29/DisableCopyOnReadDisableCopyOnRead,read_29_disablecopyonread_adam_v_layer3_bias"/device:CPU:0*
_output_shapes
 �
Read_29/ReadVariableOpReadVariableOp,read_29_disablecopyonread_adam_v_layer3_bias^Read_29/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:(*
dtype0k
Identity_58IdentityRead_29/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:(a
Identity_59IdentityIdentity_58:output:0"/device:CPU:0*
T0*
_output_shapes
:(�
Read_30/DisableCopyOnReadDisableCopyOnRead.read_30_disablecopyonread_adam_m_layer4_kernel"/device:CPU:0*
_output_shapes
 �
Read_30/ReadVariableOpReadVariableOp.read_30_disablecopyonread_adam_m_layer4_kernel^Read_30/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:(P*
dtype0o
Identity_60IdentityRead_30/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:(Pe
Identity_61IdentityIdentity_60:output:0"/device:CPU:0*
T0*
_output_shapes

:(P�
Read_31/DisableCopyOnReadDisableCopyOnRead.read_31_disablecopyonread_adam_v_layer4_kernel"/device:CPU:0*
_output_shapes
 �
Read_31/ReadVariableOpReadVariableOp.read_31_disablecopyonread_adam_v_layer4_kernel^Read_31/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:(P*
dtype0o
Identity_62IdentityRead_31/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:(Pe
Identity_63IdentityIdentity_62:output:0"/device:CPU:0*
T0*
_output_shapes

:(P�
Read_32/DisableCopyOnReadDisableCopyOnRead,read_32_disablecopyonread_adam_m_layer4_bias"/device:CPU:0*
_output_shapes
 �
Read_32/ReadVariableOpReadVariableOp,read_32_disablecopyonread_adam_m_layer4_bias^Read_32/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:P*
dtype0k
Identity_64IdentityRead_32/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:Pa
Identity_65IdentityIdentity_64:output:0"/device:CPU:0*
T0*
_output_shapes
:P�
Read_33/DisableCopyOnReadDisableCopyOnRead,read_33_disablecopyonread_adam_v_layer4_bias"/device:CPU:0*
_output_shapes
 �
Read_33/ReadVariableOpReadVariableOp,read_33_disablecopyonread_adam_v_layer4_bias^Read_33/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:P*
dtype0k
Identity_66IdentityRead_33/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:Pa
Identity_67IdentityIdentity_66:output:0"/device:CPU:0*
T0*
_output_shapes
:P�
Read_34/DisableCopyOnReadDisableCopyOnRead.read_34_disablecopyonread_adam_m_output_kernel"/device:CPU:0*
_output_shapes
 �
Read_34/ReadVariableOpReadVariableOp.read_34_disablecopyonread_adam_m_output_kernel^Read_34/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:Pd*
dtype0o
Identity_68IdentityRead_34/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:Pde
Identity_69IdentityIdentity_68:output:0"/device:CPU:0*
T0*
_output_shapes

:Pd�
Read_35/DisableCopyOnReadDisableCopyOnRead.read_35_disablecopyonread_adam_v_output_kernel"/device:CPU:0*
_output_shapes
 �
Read_35/ReadVariableOpReadVariableOp.read_35_disablecopyonread_adam_v_output_kernel^Read_35/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:Pd*
dtype0o
Identity_70IdentityRead_35/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:Pde
Identity_71IdentityIdentity_70:output:0"/device:CPU:0*
T0*
_output_shapes

:Pd�
Read_36/DisableCopyOnReadDisableCopyOnRead,read_36_disablecopyonread_adam_m_output_bias"/device:CPU:0*
_output_shapes
 �
Read_36/ReadVariableOpReadVariableOp,read_36_disablecopyonread_adam_m_output_bias^Read_36/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:d*
dtype0k
Identity_72IdentityRead_36/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:da
Identity_73IdentityIdentity_72:output:0"/device:CPU:0*
T0*
_output_shapes
:d�
Read_37/DisableCopyOnReadDisableCopyOnRead,read_37_disablecopyonread_adam_v_output_bias"/device:CPU:0*
_output_shapes
 �
Read_37/ReadVariableOpReadVariableOp,read_37_disablecopyonread_adam_v_output_bias^Read_37/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:d*
dtype0k
Identity_74IdentityRead_37/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:da
Identity_75IdentityIdentity_74:output:0"/device:CPU:0*
T0*
_output_shapes
:dt
Read_38/DisableCopyOnReadDisableCopyOnReadread_38_disablecopyonread_total"/device:CPU:0*
_output_shapes
 �
Read_38/ReadVariableOpReadVariableOpread_38_disablecopyonread_total^Read_38/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0g
Identity_76IdentityRead_38/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: ]
Identity_77IdentityIdentity_76:output:0"/device:CPU:0*
T0*
_output_shapes
: t
Read_39/DisableCopyOnReadDisableCopyOnReadread_39_disablecopyonread_count"/device:CPU:0*
_output_shapes
 �
Read_39/ReadVariableOpReadVariableOpread_39_disablecopyonread_count^Read_39/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0g
Identity_78IdentityRead_39/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: ]
Identity_79IdentityIdentity_78:output:0"/device:CPU:0*
T0*
_output_shapes
: �
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:)*
dtype0*�
value�B�)B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB0optimizer/_iterations/.ATTRIBUTES/VARIABLE_VALUEB3optimizer/_learning_rate/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/1/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/2/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/3/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/4/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/5/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/6/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/7/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/8/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/9/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/10/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/11/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/12/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/13/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/14/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/15/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/16/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/17/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/18/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/19/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/20/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/21/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/22/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/23/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/24/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:)*
dtype0*e
value\BZ)B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B �
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0Identity_1:output:0Identity_3:output:0Identity_5:output:0Identity_7:output:0Identity_9:output:0Identity_11:output:0Identity_13:output:0Identity_15:output:0Identity_17:output:0Identity_19:output:0Identity_21:output:0Identity_23:output:0Identity_25:output:0Identity_27:output:0Identity_29:output:0Identity_31:output:0Identity_33:output:0Identity_35:output:0Identity_37:output:0Identity_39:output:0Identity_41:output:0Identity_43:output:0Identity_45:output:0Identity_47:output:0Identity_49:output:0Identity_51:output:0Identity_53:output:0Identity_55:output:0Identity_57:output:0Identity_59:output:0Identity_61:output:0Identity_63:output:0Identity_65:output:0Identity_67:output:0Identity_69:output:0Identity_71:output:0Identity_73:output:0Identity_75:output:0Identity_77:output:0Identity_79:output:0savev2_const"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *7
dtypes-
+2)	�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 i
Identity_80Identityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: U
Identity_81IdentityIdentity_80:output:0^NoOp*
T0*
_output_shapes
: �
NoOpNoOp^MergeV2Checkpoints^Read/DisableCopyOnRead^Read/ReadVariableOp^Read_1/DisableCopyOnRead^Read_1/ReadVariableOp^Read_10/DisableCopyOnRead^Read_10/ReadVariableOp^Read_11/DisableCopyOnRead^Read_11/ReadVariableOp^Read_12/DisableCopyOnRead^Read_12/ReadVariableOp^Read_13/DisableCopyOnRead^Read_13/ReadVariableOp^Read_14/DisableCopyOnRead^Read_14/ReadVariableOp^Read_15/DisableCopyOnRead^Read_15/ReadVariableOp^Read_16/DisableCopyOnRead^Read_16/ReadVariableOp^Read_17/DisableCopyOnRead^Read_17/ReadVariableOp^Read_18/DisableCopyOnRead^Read_18/ReadVariableOp^Read_19/DisableCopyOnRead^Read_19/ReadVariableOp^Read_2/DisableCopyOnRead^Read_2/ReadVariableOp^Read_20/DisableCopyOnRead^Read_20/ReadVariableOp^Read_21/DisableCopyOnRead^Read_21/ReadVariableOp^Read_22/DisableCopyOnRead^Read_22/ReadVariableOp^Read_23/DisableCopyOnRead^Read_23/ReadVariableOp^Read_24/DisableCopyOnRead^Read_24/ReadVariableOp^Read_25/DisableCopyOnRead^Read_25/ReadVariableOp^Read_26/DisableCopyOnRead^Read_26/ReadVariableOp^Read_27/DisableCopyOnRead^Read_27/ReadVariableOp^Read_28/DisableCopyOnRead^Read_28/ReadVariableOp^Read_29/DisableCopyOnRead^Read_29/ReadVariableOp^Read_3/DisableCopyOnRead^Read_3/ReadVariableOp^Read_30/DisableCopyOnRead^Read_30/ReadVariableOp^Read_31/DisableCopyOnRead^Read_31/ReadVariableOp^Read_32/DisableCopyOnRead^Read_32/ReadVariableOp^Read_33/DisableCopyOnRead^Read_33/ReadVariableOp^Read_34/DisableCopyOnRead^Read_34/ReadVariableOp^Read_35/DisableCopyOnRead^Read_35/ReadVariableOp^Read_36/DisableCopyOnRead^Read_36/ReadVariableOp^Read_37/DisableCopyOnRead^Read_37/ReadVariableOp^Read_38/DisableCopyOnRead^Read_38/ReadVariableOp^Read_39/DisableCopyOnRead^Read_39/ReadVariableOp^Read_4/DisableCopyOnRead^Read_4/ReadVariableOp^Read_5/DisableCopyOnRead^Read_5/ReadVariableOp^Read_6/DisableCopyOnRead^Read_6/ReadVariableOp^Read_7/DisableCopyOnRead^Read_7/ReadVariableOp^Read_8/DisableCopyOnRead^Read_8/ReadVariableOp^Read_9/DisableCopyOnRead^Read_9/ReadVariableOp*
_output_shapes
 "#
identity_81Identity_81:output:0*(
_construction_contextkEagerRuntime*g
_input_shapesV
T: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2(
MergeV2CheckpointsMergeV2Checkpoints20
Read/DisableCopyOnReadRead/DisableCopyOnRead2*
Read/ReadVariableOpRead/ReadVariableOp24
Read_1/DisableCopyOnReadRead_1/DisableCopyOnRead2.
Read_1/ReadVariableOpRead_1/ReadVariableOp26
Read_10/DisableCopyOnReadRead_10/DisableCopyOnRead20
Read_10/ReadVariableOpRead_10/ReadVariableOp26
Read_11/DisableCopyOnReadRead_11/DisableCopyOnRead20
Read_11/ReadVariableOpRead_11/ReadVariableOp26
Read_12/DisableCopyOnReadRead_12/DisableCopyOnRead20
Read_12/ReadVariableOpRead_12/ReadVariableOp26
Read_13/DisableCopyOnReadRead_13/DisableCopyOnRead20
Read_13/ReadVariableOpRead_13/ReadVariableOp26
Read_14/DisableCopyOnReadRead_14/DisableCopyOnRead20
Read_14/ReadVariableOpRead_14/ReadVariableOp26
Read_15/DisableCopyOnReadRead_15/DisableCopyOnRead20
Read_15/ReadVariableOpRead_15/ReadVariableOp26
Read_16/DisableCopyOnReadRead_16/DisableCopyOnRead20
Read_16/ReadVariableOpRead_16/ReadVariableOp26
Read_17/DisableCopyOnReadRead_17/DisableCopyOnRead20
Read_17/ReadVariableOpRead_17/ReadVariableOp26
Read_18/DisableCopyOnReadRead_18/DisableCopyOnRead20
Read_18/ReadVariableOpRead_18/ReadVariableOp26
Read_19/DisableCopyOnReadRead_19/DisableCopyOnRead20
Read_19/ReadVariableOpRead_19/ReadVariableOp24
Read_2/DisableCopyOnReadRead_2/DisableCopyOnRead2.
Read_2/ReadVariableOpRead_2/ReadVariableOp26
Read_20/DisableCopyOnReadRead_20/DisableCopyOnRead20
Read_20/ReadVariableOpRead_20/ReadVariableOp26
Read_21/DisableCopyOnReadRead_21/DisableCopyOnRead20
Read_21/ReadVariableOpRead_21/ReadVariableOp26
Read_22/DisableCopyOnReadRead_22/DisableCopyOnRead20
Read_22/ReadVariableOpRead_22/ReadVariableOp26
Read_23/DisableCopyOnReadRead_23/DisableCopyOnRead20
Read_23/ReadVariableOpRead_23/ReadVariableOp26
Read_24/DisableCopyOnReadRead_24/DisableCopyOnRead20
Read_24/ReadVariableOpRead_24/ReadVariableOp26
Read_25/DisableCopyOnReadRead_25/DisableCopyOnRead20
Read_25/ReadVariableOpRead_25/ReadVariableOp26
Read_26/DisableCopyOnReadRead_26/DisableCopyOnRead20
Read_26/ReadVariableOpRead_26/ReadVariableOp26
Read_27/DisableCopyOnReadRead_27/DisableCopyOnRead20
Read_27/ReadVariableOpRead_27/ReadVariableOp26
Read_28/DisableCopyOnReadRead_28/DisableCopyOnRead20
Read_28/ReadVariableOpRead_28/ReadVariableOp26
Read_29/DisableCopyOnReadRead_29/DisableCopyOnRead20
Read_29/ReadVariableOpRead_29/ReadVariableOp24
Read_3/DisableCopyOnReadRead_3/DisableCopyOnRead2.
Read_3/ReadVariableOpRead_3/ReadVariableOp26
Read_30/DisableCopyOnReadRead_30/DisableCopyOnRead20
Read_30/ReadVariableOpRead_30/ReadVariableOp26
Read_31/DisableCopyOnReadRead_31/DisableCopyOnRead20
Read_31/ReadVariableOpRead_31/ReadVariableOp26
Read_32/DisableCopyOnReadRead_32/DisableCopyOnRead20
Read_32/ReadVariableOpRead_32/ReadVariableOp26
Read_33/DisableCopyOnReadRead_33/DisableCopyOnRead20
Read_33/ReadVariableOpRead_33/ReadVariableOp26
Read_34/DisableCopyOnReadRead_34/DisableCopyOnRead20
Read_34/ReadVariableOpRead_34/ReadVariableOp26
Read_35/DisableCopyOnReadRead_35/DisableCopyOnRead20
Read_35/ReadVariableOpRead_35/ReadVariableOp26
Read_36/DisableCopyOnReadRead_36/DisableCopyOnRead20
Read_36/ReadVariableOpRead_36/ReadVariableOp26
Read_37/DisableCopyOnReadRead_37/DisableCopyOnRead20
Read_37/ReadVariableOpRead_37/ReadVariableOp26
Read_38/DisableCopyOnReadRead_38/DisableCopyOnRead20
Read_38/ReadVariableOpRead_38/ReadVariableOp26
Read_39/DisableCopyOnReadRead_39/DisableCopyOnRead20
Read_39/ReadVariableOpRead_39/ReadVariableOp24
Read_4/DisableCopyOnReadRead_4/DisableCopyOnRead2.
Read_4/ReadVariableOpRead_4/ReadVariableOp24
Read_5/DisableCopyOnReadRead_5/DisableCopyOnRead2.
Read_5/ReadVariableOpRead_5/ReadVariableOp24
Read_6/DisableCopyOnReadRead_6/DisableCopyOnRead2.
Read_6/ReadVariableOpRead_6/ReadVariableOp24
Read_7/DisableCopyOnReadRead_7/DisableCopyOnRead2.
Read_7/ReadVariableOpRead_7/ReadVariableOp24
Read_8/DisableCopyOnReadRead_8/DisableCopyOnRead2.
Read_8/ReadVariableOpRead_8/ReadVariableOp24
Read_9/DisableCopyOnReadRead_9/DisableCopyOnRead2.
Read_9/ReadVariableOpRead_9/ReadVariableOp:=)9

_output_shapes
: 

_user_specified_nameConst:%(!

_user_specified_namecount:%'!

_user_specified_nametotal:2&.
,
_user_specified_nameAdam/v/output/bias:2%.
,
_user_specified_nameAdam/m/output/bias:4$0
.
_user_specified_nameAdam/v/output/kernel:4#0
.
_user_specified_nameAdam/m/output/kernel:2".
,
_user_specified_nameAdam/v/layer4/bias:2!.
,
_user_specified_nameAdam/m/layer4/bias:4 0
.
_user_specified_nameAdam/v/layer4/kernel:40
.
_user_specified_nameAdam/m/layer4/kernel:2.
,
_user_specified_nameAdam/v/layer3/bias:2.
,
_user_specified_nameAdam/m/layer3/bias:40
.
_user_specified_nameAdam/v/layer3/kernel:40
.
_user_specified_nameAdam/m/layer3/kernel:2.
,
_user_specified_nameAdam/v/latent/bias:2.
,
_user_specified_nameAdam/m/latent/bias:40
.
_user_specified_nameAdam/v/latent/kernel:40
.
_user_specified_nameAdam/m/latent/kernel:2.
,
_user_specified_nameAdam/v/layer2/bias:2.
,
_user_specified_nameAdam/m/layer2/bias:40
.
_user_specified_nameAdam/v/layer2/kernel:40
.
_user_specified_nameAdam/m/layer2/kernel:2.
,
_user_specified_nameAdam/v/layer1/bias:2.
,
_user_specified_nameAdam/m/layer1/bias:40
.
_user_specified_nameAdam/v/layer1/kernel:40
.
_user_specified_nameAdam/m/layer1/kernel:-)
'
_user_specified_namelearning_rate:)%
#
_user_specified_name	iteration:+'
%
_user_specified_nameoutput/bias:-)
'
_user_specified_nameoutput/kernel:+
'
%
_user_specified_namelayer4/bias:-	)
'
_user_specified_namelayer4/kernel:+'
%
_user_specified_namelayer3/bias:-)
'
_user_specified_namelayer3/kernel:+'
%
_user_specified_namelatent/bias:-)
'
_user_specified_namelatent/kernel:+'
%
_user_specified_namelayer2/bias:-)
'
_user_specified_namelayer2/kernel:+'
%
_user_specified_namelayer1/bias:-)
'
_user_specified_namelayer1/kernel:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�/
�
A__inference_model_layer_call_and_return_conditional_losses_668744	
input
layer1_668704:dP
layer1_668706:P
layer2_668717:P(
layer2_668719:(
latent_668722:(
latent_668724:
layer3_668727:(
layer3_668729:(
layer4_668732:(P
layer4_668734:P
output_668737:Pd
output_668739:d
identity

identity_1��latent/StatefulPartitionedCall�layer1/StatefulPartitionedCall�layer2/StatefulPartitionedCall�layer3/StatefulPartitionedCall�layer4/StatefulPartitionedCall�output/StatefulPartitionedCall�
layer1/StatefulPartitionedCallStatefulPartitionedCallinputlayer1_668704layer1_668706*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������P*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_layer1_layer_call_and_return_conditional_losses_668605�
*layer1/ActivityRegularizer/PartitionedCallPartitionedCall'layer1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *1
config_proto!

CPU

GPU (2J 8� *7
f2R0
.__inference_layer1_activity_regularizer_668592�
 layer1/ActivityRegularizer/ShapeShape'layer1/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��x
.layer1/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: z
0layer1/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:z
0layer1/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
(layer1/ActivityRegularizer/strided_sliceStridedSlice)layer1/ActivityRegularizer/Shape:output:07layer1/ActivityRegularizer/strided_slice/stack:output:09layer1/ActivityRegularizer/strided_slice/stack_1:output:09layer1/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
layer1/ActivityRegularizer/CastCast1layer1/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
"layer1/ActivityRegularizer/truedivRealDiv3layer1/ActivityRegularizer/PartitionedCall:output:0#layer1/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
layer2/StatefulPartitionedCallStatefulPartitionedCall'layer1/StatefulPartitionedCall:output:0layer2_668717layer2_668719*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������(*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_layer2_layer_call_and_return_conditional_losses_668629�
latent/StatefulPartitionedCallStatefulPartitionedCall'layer2/StatefulPartitionedCall:output:0latent_668722latent_668724*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_latent_layer_call_and_return_conditional_losses_668645�
layer3/StatefulPartitionedCallStatefulPartitionedCall'latent/StatefulPartitionedCall:output:0layer3_668727layer3_668729*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������(*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_layer3_layer_call_and_return_conditional_losses_668661�
layer4/StatefulPartitionedCallStatefulPartitionedCall'layer3/StatefulPartitionedCall:output:0layer4_668732layer4_668734*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������P*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_layer4_layer_call_and_return_conditional_losses_668677�
output/StatefulPartitionedCallStatefulPartitionedCall'layer4/StatefulPartitionedCall:output:0output_668737output_668739*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������d*$
_read_only_resource_inputs
*1
config_proto!

CPU

GPU (2J 8� *K
fFRD
B__inference_output_layer_call_and_return_conditional_losses_668693v
IdentityIdentity'output/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������df

Identity_1Identity&layer1/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: �
NoOpNoOp^latent/StatefulPartitionedCall^layer1/StatefulPartitionedCall^layer2/StatefulPartitionedCall^layer3/StatefulPartitionedCall^layer4/StatefulPartitionedCall^output/StatefulPartitionedCall*
_output_shapes
 "!

identity_1Identity_1:output:0"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*>
_input_shapes-
+:���������d: : : : : : : : : : : : 2@
latent/StatefulPartitionedCalllatent/StatefulPartitionedCall2@
layer1/StatefulPartitionedCalllayer1/StatefulPartitionedCall2@
layer2/StatefulPartitionedCalllayer2/StatefulPartitionedCall2@
layer3/StatefulPartitionedCalllayer3/StatefulPartitionedCall2@
layer4/StatefulPartitionedCalllayer4/StatefulPartitionedCall2@
output/StatefulPartitionedCalloutput/StatefulPartitionedCall:&"
 
_user_specified_name668739:&"
 
_user_specified_name668737:&
"
 
_user_specified_name668734:&	"
 
_user_specified_name668732:&"
 
_user_specified_name668729:&"
 
_user_specified_name668727:&"
 
_user_specified_name668724:&"
 
_user_specified_name668722:&"
 
_user_specified_name668719:&"
 
_user_specified_name668717:&"
 
_user_specified_name668706:&"
 
_user_specified_name668704:N J
'
_output_shapes
:���������d

_user_specified_nameinput
�I
�	
!__inference__wrapped_model_668585	
input=
+model_layer1_matmul_readvariableop_resource:dP:
,model_layer1_biasadd_readvariableop_resource:P=
+model_layer2_matmul_readvariableop_resource:P(:
,model_layer2_biasadd_readvariableop_resource:(=
+model_latent_matmul_readvariableop_resource:(:
,model_latent_biasadd_readvariableop_resource:=
+model_layer3_matmul_readvariableop_resource:(:
,model_layer3_biasadd_readvariableop_resource:(=
+model_layer4_matmul_readvariableop_resource:(P:
,model_layer4_biasadd_readvariableop_resource:P=
+model_output_matmul_readvariableop_resource:Pd:
,model_output_biasadd_readvariableop_resource:d
identity��#model/latent/BiasAdd/ReadVariableOp�"model/latent/MatMul/ReadVariableOp�#model/layer1/BiasAdd/ReadVariableOp�"model/layer1/MatMul/ReadVariableOp�#model/layer2/BiasAdd/ReadVariableOp�"model/layer2/MatMul/ReadVariableOp�#model/layer3/BiasAdd/ReadVariableOp�"model/layer3/MatMul/ReadVariableOp�#model/layer4/BiasAdd/ReadVariableOp�"model/layer4/MatMul/ReadVariableOp�#model/output/BiasAdd/ReadVariableOp�"model/output/MatMul/ReadVariableOp�
"model/layer1/MatMul/ReadVariableOpReadVariableOp+model_layer1_matmul_readvariableop_resource*
_output_shapes

:dP*
dtype0�
model/layer1/MatMulMatMulinput*model/layer1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������P�
#model/layer1/BiasAdd/ReadVariableOpReadVariableOp,model_layer1_biasadd_readvariableop_resource*
_output_shapes
:P*
dtype0�
model/layer1/BiasAddBiasAddmodel/layer1/MatMul:product:0+model/layer1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Pk
model/layer1/LeakyRelu	LeakyRelumodel/layer1/BiasAdd:output:0*'
_output_shapes
:���������Px
'model/layer1/ActivityRegularizer/L2LossL2Loss$model/layer1/LeakyRelu:activations:0*
T0*
_output_shapes
: k
&model/layer1/ActivityRegularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *���3�
$model/layer1/ActivityRegularizer/mulMul/model/layer1/ActivityRegularizer/mul/x:output:00model/layer1/ActivityRegularizer/L2Loss:output:0*
T0*
_output_shapes
: �
&model/layer1/ActivityRegularizer/ShapeShape$model/layer1/LeakyRelu:activations:0*
T0*
_output_shapes
::��~
4model/layer1/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6model/layer1/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6model/layer1/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.model/layer1/ActivityRegularizer/strided_sliceStridedSlice/model/layer1/ActivityRegularizer/Shape:output:0=model/layer1/ActivityRegularizer/strided_slice/stack:output:0?model/layer1/ActivityRegularizer/strided_slice/stack_1:output:0?model/layer1/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
%model/layer1/ActivityRegularizer/CastCast7model/layer1/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
(model/layer1/ActivityRegularizer/truedivRealDiv(model/layer1/ActivityRegularizer/mul:z:0)model/layer1/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
"model/layer2/MatMul/ReadVariableOpReadVariableOp+model_layer2_matmul_readvariableop_resource*
_output_shapes

:P(*
dtype0�
model/layer2/MatMulMatMul$model/layer1/LeakyRelu:activations:0*model/layer2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������(�
#model/layer2/BiasAdd/ReadVariableOpReadVariableOp,model_layer2_biasadd_readvariableop_resource*
_output_shapes
:(*
dtype0�
model/layer2/BiasAddBiasAddmodel/layer2/MatMul:product:0+model/layer2/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������(k
model/layer2/LeakyRelu	LeakyRelumodel/layer2/BiasAdd:output:0*'
_output_shapes
:���������(�
"model/latent/MatMul/ReadVariableOpReadVariableOp+model_latent_matmul_readvariableop_resource*
_output_shapes

:(*
dtype0�
model/latent/MatMulMatMul$model/layer2/LeakyRelu:activations:0*model/latent/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
#model/latent/BiasAdd/ReadVariableOpReadVariableOp,model_latent_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
model/latent/BiasAddBiasAddmodel/latent/MatMul:product:0+model/latent/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������k
model/latent/LeakyRelu	LeakyRelumodel/latent/BiasAdd:output:0*'
_output_shapes
:����������
"model/layer3/MatMul/ReadVariableOpReadVariableOp+model_layer3_matmul_readvariableop_resource*
_output_shapes

:(*
dtype0�
model/layer3/MatMulMatMul$model/latent/LeakyRelu:activations:0*model/layer3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������(�
#model/layer3/BiasAdd/ReadVariableOpReadVariableOp,model_layer3_biasadd_readvariableop_resource*
_output_shapes
:(*
dtype0�
model/layer3/BiasAddBiasAddmodel/layer3/MatMul:product:0+model/layer3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������(k
model/layer3/LeakyRelu	LeakyRelumodel/layer3/BiasAdd:output:0*'
_output_shapes
:���������(�
"model/layer4/MatMul/ReadVariableOpReadVariableOp+model_layer4_matmul_readvariableop_resource*
_output_shapes

:(P*
dtype0�
model/layer4/MatMulMatMul$model/layer3/LeakyRelu:activations:0*model/layer4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������P�
#model/layer4/BiasAdd/ReadVariableOpReadVariableOp,model_layer4_biasadd_readvariableop_resource*
_output_shapes
:P*
dtype0�
model/layer4/BiasAddBiasAddmodel/layer4/MatMul:product:0+model/layer4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Pk
model/layer4/LeakyRelu	LeakyRelumodel/layer4/BiasAdd:output:0*'
_output_shapes
:���������P�
"model/output/MatMul/ReadVariableOpReadVariableOp+model_output_matmul_readvariableop_resource*
_output_shapes

:Pd*
dtype0�
model/output/MatMulMatMul$model/layer4/LeakyRelu:activations:0*model/output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������d�
#model/output/BiasAdd/ReadVariableOpReadVariableOp,model_output_biasadd_readvariableop_resource*
_output_shapes
:d*
dtype0�
model/output/BiasAddBiasAddmodel/output/MatMul:product:0+model/output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������dk
model/output/LeakyRelu	LeakyRelumodel/output/BiasAdd:output:0*'
_output_shapes
:���������ds
IdentityIdentity$model/output/LeakyRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������d�
NoOpNoOp$^model/latent/BiasAdd/ReadVariableOp#^model/latent/MatMul/ReadVariableOp$^model/layer1/BiasAdd/ReadVariableOp#^model/layer1/MatMul/ReadVariableOp$^model/layer2/BiasAdd/ReadVariableOp#^model/layer2/MatMul/ReadVariableOp$^model/layer3/BiasAdd/ReadVariableOp#^model/layer3/MatMul/ReadVariableOp$^model/layer4/BiasAdd/ReadVariableOp#^model/layer4/MatMul/ReadVariableOp$^model/output/BiasAdd/ReadVariableOp#^model/output/MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*>
_input_shapes-
+:���������d: : : : : : : : : : : : 2J
#model/latent/BiasAdd/ReadVariableOp#model/latent/BiasAdd/ReadVariableOp2H
"model/latent/MatMul/ReadVariableOp"model/latent/MatMul/ReadVariableOp2J
#model/layer1/BiasAdd/ReadVariableOp#model/layer1/BiasAdd/ReadVariableOp2H
"model/layer1/MatMul/ReadVariableOp"model/layer1/MatMul/ReadVariableOp2J
#model/layer2/BiasAdd/ReadVariableOp#model/layer2/BiasAdd/ReadVariableOp2H
"model/layer2/MatMul/ReadVariableOp"model/layer2/MatMul/ReadVariableOp2J
#model/layer3/BiasAdd/ReadVariableOp#model/layer3/BiasAdd/ReadVariableOp2H
"model/layer3/MatMul/ReadVariableOp"model/layer3/MatMul/ReadVariableOp2J
#model/layer4/BiasAdd/ReadVariableOp#model/layer4/BiasAdd/ReadVariableOp2H
"model/layer4/MatMul/ReadVariableOp"model/layer4/MatMul/ReadVariableOp2J
#model/output/BiasAdd/ReadVariableOp#model/output/BiasAdd/ReadVariableOp2H
"model/output/MatMul/ReadVariableOp"model/output/MatMul/ReadVariableOp:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:(
$
"
_user_specified_name
resource:(	$
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:N J
'
_output_shapes
:���������d

_user_specified_nameinput"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
7
input.
serving_default_input:0���������d:
output0
StatefulPartitionedCall:0���������dtensorflow/serving/predict:��
�
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer_with_weights-3
layer-4
layer_with_weights-4
layer-5
layer_with_weights-5
layer-6
	variables
	trainable_variables

regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses
_default_save_signature
	optimizer

signatures"
_tf_keras_network
"
_tf_keras_input_layer
�
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses

kernel
bias"
_tf_keras_layer
�
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses

kernel
 bias"
_tf_keras_layer
�
!	variables
"trainable_variables
#regularization_losses
$	keras_api
%__call__
*&&call_and_return_all_conditional_losses

'kernel
(bias"
_tf_keras_layer
�
)	variables
*trainable_variables
+regularization_losses
,	keras_api
-__call__
*.&call_and_return_all_conditional_losses

/kernel
0bias"
_tf_keras_layer
�
1	variables
2trainable_variables
3regularization_losses
4	keras_api
5__call__
*6&call_and_return_all_conditional_losses

7kernel
8bias"
_tf_keras_layer
�
9	variables
:trainable_variables
;regularization_losses
<	keras_api
=__call__
*>&call_and_return_all_conditional_losses

?kernel
@bias"
_tf_keras_layer
v
0
1
2
 3
'4
(5
/6
07
78
89
?10
@11"
trackable_list_wrapper
v
0
1
2
 3
'4
(5
/6
07
78
89
?10
@11"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Anon_trainable_variables

Blayers
Cmetrics
Dlayer_regularization_losses
Elayer_metrics
	variables
	trainable_variables

regularization_losses
__call__
_default_save_signature
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses"
_generic_user_object
�
Ftrace_0
Gtrace_12�
&__inference_model_layer_call_fn_668774
&__inference_model_layer_call_fn_668804�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zFtrace_0zGtrace_1
�
Htrace_0
Itrace_12�
A__inference_model_layer_call_and_return_conditional_losses_668701
A__inference_model_layer_call_and_return_conditional_losses_668744�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zHtrace_0zItrace_1
�B�
!__inference__wrapped_model_668585input"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�
J
_variables
K_iterations
L_learning_rate
M_index_dict
N
_momentums
O_velocities
P_update_step_xla"
experimentalOptimizer
,
Qserving_default"
signature_map
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Rnon_trainable_variables

Slayers
Tmetrics
Ulayer_regularization_losses
Vlayer_metrics
	variables
trainable_variables
regularization_losses
__call__
Wactivity_regularizer_fn
*&call_and_return_all_conditional_losses
&X"call_and_return_conditional_losses"
_generic_user_object
�
Ytrace_02�
'__inference_layer1_layer_call_fn_668910�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zYtrace_0
�
Ztrace_02�
F__inference_layer1_layer_call_and_return_all_conditional_losses_668921�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zZtrace_0
:dP2layer1/kernel
:P2layer1/bias
.
0
 1"
trackable_list_wrapper
.
0
 1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
[non_trainable_variables

\layers
]metrics
^layer_regularization_losses
_layer_metrics
	variables
trainable_variables
regularization_losses
__call__
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses"
_generic_user_object
�
`trace_02�
'__inference_layer2_layer_call_fn_668941�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z`trace_0
�
atrace_02�
B__inference_layer2_layer_call_and_return_conditional_losses_668952�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zatrace_0
:P(2layer2/kernel
:(2layer2/bias
.
'0
(1"
trackable_list_wrapper
.
'0
(1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
bnon_trainable_variables

clayers
dmetrics
elayer_regularization_losses
flayer_metrics
!	variables
"trainable_variables
#regularization_losses
%__call__
*&&call_and_return_all_conditional_losses
&&"call_and_return_conditional_losses"
_generic_user_object
�
gtrace_02�
'__inference_latent_layer_call_fn_668961�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zgtrace_0
�
htrace_02�
B__inference_latent_layer_call_and_return_conditional_losses_668972�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zhtrace_0
:(2latent/kernel
:2latent/bias
.
/0
01"
trackable_list_wrapper
.
/0
01"
trackable_list_wrapper
 "
trackable_list_wrapper
�
inon_trainable_variables

jlayers
kmetrics
llayer_regularization_losses
mlayer_metrics
)	variables
*trainable_variables
+regularization_losses
-__call__
*.&call_and_return_all_conditional_losses
&."call_and_return_conditional_losses"
_generic_user_object
�
ntrace_02�
'__inference_layer3_layer_call_fn_668981�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zntrace_0
�
otrace_02�
B__inference_layer3_layer_call_and_return_conditional_losses_668992�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zotrace_0
:(2layer3/kernel
:(2layer3/bias
.
70
81"
trackable_list_wrapper
.
70
81"
trackable_list_wrapper
 "
trackable_list_wrapper
�
pnon_trainable_variables

qlayers
rmetrics
slayer_regularization_losses
tlayer_metrics
1	variables
2trainable_variables
3regularization_losses
5__call__
*6&call_and_return_all_conditional_losses
&6"call_and_return_conditional_losses"
_generic_user_object
�
utrace_02�
'__inference_layer4_layer_call_fn_669001�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zutrace_0
�
vtrace_02�
B__inference_layer4_layer_call_and_return_conditional_losses_669012�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zvtrace_0
:(P2layer4/kernel
:P2layer4/bias
.
?0
@1"
trackable_list_wrapper
.
?0
@1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
wnon_trainable_variables

xlayers
ymetrics
zlayer_regularization_losses
{layer_metrics
9	variables
:trainable_variables
;regularization_losses
=__call__
*>&call_and_return_all_conditional_losses
&>"call_and_return_conditional_losses"
_generic_user_object
�
|trace_02�
'__inference_output_layer_call_fn_669021�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z|trace_0
�
}trace_02�
B__inference_output_layer_call_and_return_conditional_losses_669032�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z}trace_0
:Pd2output/kernel
:d2output/bias
 "
trackable_list_wrapper
Q
0
1
2
3
4
5
6"
trackable_list_wrapper
'
~0"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
&__inference_model_layer_call_fn_668774input"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
&__inference_model_layer_call_fn_668804input"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
A__inference_model_layer_call_and_return_conditional_losses_668701input"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
A__inference_model_layer_call_and_return_conditional_losses_668744input"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�
K0
1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11
�12
�13
�14
�15
�16
�17
�18
�19
�20
�21
�22
�23
�24"
trackable_list_wrapper
:	 2	iteration
: 2learning_rate
 "
trackable_dict_wrapper
�
0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11"
trackable_list_wrapper
�
�0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11"
trackable_list_wrapper
�2��
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 0
�B�
$__inference_signature_wrapper_668901input"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�
�trace_02�
.__inference_layer1_activity_regularizer_668592�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�z�trace_0
�
�trace_02�
B__inference_layer1_layer_call_and_return_conditional_losses_668932�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�B�
'__inference_layer1_layer_call_fn_668910inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
F__inference_layer1_layer_call_and_return_all_conditional_losses_668921inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
'__inference_layer2_layer_call_fn_668941inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
B__inference_layer2_layer_call_and_return_conditional_losses_668952inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
'__inference_latent_layer_call_fn_668961inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
B__inference_latent_layer_call_and_return_conditional_losses_668972inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
'__inference_layer3_layer_call_fn_668981inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
B__inference_layer3_layer_call_and_return_conditional_losses_668992inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
'__inference_layer4_layer_call_fn_669001inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
B__inference_layer4_layer_call_and_return_conditional_losses_669012inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
'__inference_output_layer_call_fn_669021inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
B__inference_output_layer_call_and_return_conditional_losses_669032inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
R
�	variables
�	keras_api

�total

�count"
_tf_keras_metric
$:"dP2Adam/m/layer1/kernel
$:"dP2Adam/v/layer1/kernel
:P2Adam/m/layer1/bias
:P2Adam/v/layer1/bias
$:"P(2Adam/m/layer2/kernel
$:"P(2Adam/v/layer2/kernel
:(2Adam/m/layer2/bias
:(2Adam/v/layer2/bias
$:"(2Adam/m/latent/kernel
$:"(2Adam/v/latent/kernel
:2Adam/m/latent/bias
:2Adam/v/latent/bias
$:"(2Adam/m/layer3/kernel
$:"(2Adam/v/layer3/kernel
:(2Adam/m/layer3/bias
:(2Adam/v/layer3/bias
$:"(P2Adam/m/layer4/kernel
$:"(P2Adam/v/layer4/kernel
:P2Adam/m/layer4/bias
:P2Adam/v/layer4/bias
$:"Pd2Adam/m/output/kernel
$:"Pd2Adam/v/output/kernel
:d2Adam/m/output/bias
:d2Adam/v/output/bias
�B�
.__inference_layer1_activity_regularizer_668592x"�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
�B�
B__inference_layer1_layer_call_and_return_conditional_losses_668932inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count�
!__inference__wrapped_model_668585o '(/078?@.�+
$�!
�
input���������d
� "/�,
*
output �
output���������d�
B__inference_latent_layer_call_and_return_conditional_losses_668972c'(/�,
%�"
 �
inputs���������(
� ",�)
"�
tensor_0���������
� �
'__inference_latent_layer_call_fn_668961X'(/�,
%�"
 �
inputs���������(
� "!�
unknown���������a
.__inference_layer1_activity_regularizer_668592/�
�
�	
x
� "�
unknown �
F__inference_layer1_layer_call_and_return_all_conditional_losses_668921x/�,
%�"
 �
inputs���������d
� "A�>
"�
tensor_0���������P
�
�

tensor_1_0 �
B__inference_layer1_layer_call_and_return_conditional_losses_668932c/�,
%�"
 �
inputs���������d
� ",�)
"�
tensor_0���������P
� �
'__inference_layer1_layer_call_fn_668910X/�,
%�"
 �
inputs���������d
� "!�
unknown���������P�
B__inference_layer2_layer_call_and_return_conditional_losses_668952c /�,
%�"
 �
inputs���������P
� ",�)
"�
tensor_0���������(
� �
'__inference_layer2_layer_call_fn_668941X /�,
%�"
 �
inputs���������P
� "!�
unknown���������(�
B__inference_layer3_layer_call_and_return_conditional_losses_668992c/0/�,
%�"
 �
inputs���������
� ",�)
"�
tensor_0���������(
� �
'__inference_layer3_layer_call_fn_668981X/0/�,
%�"
 �
inputs���������
� "!�
unknown���������(�
B__inference_layer4_layer_call_and_return_conditional_losses_669012c78/�,
%�"
 �
inputs���������(
� ",�)
"�
tensor_0���������P
� �
'__inference_layer4_layer_call_fn_669001X78/�,
%�"
 �
inputs���������(
� "!�
unknown���������P�
A__inference_model_layer_call_and_return_conditional_losses_668701� '(/078?@6�3
,�)
�
input���������d
p

 
� "A�>
"�
tensor_0���������d
�
�

tensor_1_0 �
A__inference_model_layer_call_and_return_conditional_losses_668744� '(/078?@6�3
,�)
�
input���������d
p 

 
� "A�>
"�
tensor_0���������d
�
�

tensor_1_0 �
&__inference_model_layer_call_fn_668774i '(/078?@6�3
,�)
�
input���������d
p

 
� "!�
unknown���������d�
&__inference_model_layer_call_fn_668804i '(/078?@6�3
,�)
�
input���������d
p 

 
� "!�
unknown���������d�
B__inference_output_layer_call_and_return_conditional_losses_669032c?@/�,
%�"
 �
inputs���������P
� ",�)
"�
tensor_0���������d
� �
'__inference_output_layer_call_fn_669021X?@/�,
%�"
 �
inputs���������P
� "!�
unknown���������d�
$__inference_signature_wrapper_668901x '(/078?@7�4
� 
-�*
(
input�
input���������d"/�,
*
output �
output���������d