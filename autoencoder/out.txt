maxNumber= 5  maxTypes= 2  mSize= 10
Number of arguments: 1 arguments.
Argument List: ['loss_data1percent_sigBack.py']
Use as: script.py -b 0 (or 1,2)
Events Before AR cut for X in data: 106392151.0
Events after AR cut for X in data: 96378633.0
Finding AR cut
1b AR cut is at bin  87  at X value  2811.0
2b AR cut is at bin  117  at X value  5226.0
Summ= 96378633.0
Events Before AR cut for X in model x1000_s500:  11135.0
Events after AR cut for X in model x1000_s500:  10720.0
S/B before AR cut:  0.00010465997627964116
S/B after AR cut:  0.00011122797311308618
S/B increase after AR:  1.0627555734954113
Events Before AR cut for X in model x3000_s750:  46881.0
Events after AR cut for X in model x3000_s750:  46877.0
S/B before AR cut:  0.0004406434079897492
S/B after AR cut:  0.0004863837402632594
S/B increase after AR:  1.103803509695473
Events Before AR cut for X in model x300_s70:  2810.0
Events after AR cut for X in model x300_s70:  2687.0
S/B before AR cut:  2.6411722797107467e-05
S/B after AR cut:  2.787962348459539e-05
S/B increase after AR:  1.0555776197851312
Events Before AR cut for X in model x6000_s1000:  47899.0
Events after AR cut for X in model x6000_s1000:  47899.0
S/B before AR cut:  0.0004502117830101959
S/B after AR cut:  0.0004969877503865405
S/B increase after AR:  1.103897696909646
Events Before AR cut for X in model x6000_s5000:  49285.0
Events after AR cut for X in model x6000_s5000:  49285.0
S/B before AR cut:  0.00046323905980620697
S/B after AR cut:  0.0005113685312386616
S/B increase after AR:  1.103897696909646
Events Before AR cut for X in model x750_s250:  4192.0
Events after AR cut for X in model x750_s250:  4111.0
S/B before AR cut:  3.9401402834688435e-05
S/B after AR cut:  4.265468259961728e-05
S/B increase after AR:  1.0825676125943593
figs/loss_data1percent_sigBack.eps
