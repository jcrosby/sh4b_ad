

import json
import yaml
import tensorflow
import tensorflow as tf2
import tensorflow.compat.v1 as tf
tf.disable_eager_execution()
import tensorflow_probability as tfp
import tensorflow_hub as hub
tfd = tfp.distributions
tfb = tfp.bijectors

from pae.flow_class import RealNVP, SplineParams, Made,BatchNorm
from pae.flow_catalog import NeuralSplineFlow

import numpy as np
import pandas as pd
pd.set_option('display.max_columns', None)
pd.set_option('display.max_row', None)
import matplotlib.pyplot as plt
plt.rcdefaults()
import matplotlib
matplotlib.use('Agg') # set the backend before importing pyplo. Fix Invalid DISPLAY variable 
from matplotlib import pyplot as plt
####### Deep learning libraries
""" 
from tensorflow import keras
from keras.models import Model, load_model
from keras.layers import Input, Dense
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.callbacks import EarlyStopping
from sklearn.preprocessing import  StandardScaler, MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import (confusion_matrix, classification_report, accuracy_score, roc_auc_score, auc,
                             precision_score, recall_score, roc_curve, precision_recall_curve,
                             precision_recall_fscore_support, f1_score,
                             precision_recall_fscore_support)
 """
### Load files and initiate latent space
with open('pae/pae_config.yaml','r') as file:
    config = yaml.safe_load(file)

with open(config['param_path']['path']+config['param_path']['file'],'r') as f:
        params = json.load(f)

if config['use_data']:
    latent_size=params['latent_layer']
else:
    arr = np.load(config['latent_file']['path']+config['latent_file']['file'])
    amin = np.amin(arr)
    amax = np.amax(arr) 
    mean = (amax+amin) / 2
    scale = (amax-amin)
    ndim0 = config['toy_dims'][0]
    ndim1 = config['toy_dims'][1]
    emb = np.exp(np.random.normal(loc=mean, scale=1.0 ,size=(ndim0,ndim1))).astype(np.float32)
    params={}
    params['latent_layer']    = emb.shape[-1]
    latent_size= params['latent_layer']
scale = config['scale']
emb = np.load(config['latent_file']['path']+config['latent_file']['file'])
if scale == "max":
    scale = max(emb.flatten())
    print("Scaling by max: ", scale)

lr = config['learning_rate']
#### Design Normalizing Flow

# number of transformations in the bijection
N_spline_layers = config['N_spline_layers']
N_rescale_shift_layers = config['N_rescale_shift_layers']
N_shift_layers = config['N_shift_layers']
N_MAF_layers = config['N_MAF_layers']

if config['model']=='MAF':
     nvp_depth = N_MAF_layers
else:
    nvp_depth = N_spline_layers+N_shift_layers+N_rescale_shift_layers+N_shift_layers

dims         = np.asarray([params['latent_layer'] for ii in range(nvp_depth)]) 
nvp_size     = np.arange(nvp_depth) 
indices      = np.arange(params['latent_layer'])

# Neural Spline architecture
NSnn = config['Neural_Spline_Architecture']


def trainable_lu_factorization(
    event_size, batch_shape=(), seed=None, dtype=tf.float32, name=None):
    with tf.name_scope(name or 'trainable_lu_factorization'):
        event_size = tf.convert_to_tensor(
            event_size, dtype_hint=tf.int32, name='event_size')
        batch_shape = tf.convert_to_tensor(
            batch_shape, dtype_hint=event_size.dtype, name='batch_shape')
        random_matrix = tf.random.uniform(
            shape=tf.concat([batch_shape, [event_size, event_size]], axis=0),
            dtype=dtype,
            seed=seed)
        random_orthonormal = tf.linalg.qr(random_matrix)[0]
        lower_upper, permutation = tf.linalg.lu(random_orthonormal)
        lower_upper = tf.Variable(
            initial_value=lower_upper,
            trainable=True,
            name='lower_upper')
        # Initialize a non-trainable variable for the permutation indices so
        # that its value isn't re-sampled from run-to-run.
        permutation = tf.Variable(
            initial_value=permutation,
            trainable=False,
            name='permutation')
        return lower_upper, permutation
    
def init_once(x, name):
    return tf.get_variable(name, initializer=x, trainable=False)

# target distribution
def get_prior(latent_size):
    return tfd.MultivariateNormalDiag(tf.zeros(latent_size, dtype=tf.float32), scale_diag=None)

def get_nvp():
    prior             = get_prior(params['latent_layer'])
    chain             = []
    reduction         = (params['latent_layer']-dims)/params['latent_layer']
    current_size      = []
    perms_swap        = []
    perms_rand        = []
    perms_train       = []
    splines           = []
    size              = []
    input_shape       = latent_size

    for i,s in enumerate(nvp_size):
        current_size  = int(params['latent_layer']*(1-reduction[i]))
        swapping      = np.concatenate((np.arange(current_size//2,current_size),np.arange(0, current_size//2)))
        perms_swap.append(tfb.Permute(permutation=init_once(swapping,name="perm_swap%d"%i)))
        perms_train.append(tfb.ScaleMatvecLU(*trainable_lu_factorization(current_size),validate_args=True, name="perms_train%d"%i))
        splines.append(NeuralSplineFlow(latent_size,d_dim=2,b_interval=[-1,1],nn_layers=NSnn))
        #splines.append(SplineParams(NSnn[0],arch=NSnn))
        size.append(current_size)
        print("NVP Size: ",current_size, size)


# model can be composed of different element of GLOW, NSF, Real NVP, use the if statements for modifications
    for i,s in enumerate(nvp_size):
      
        if config['model']=="MAF":
             if config['batch_normalization']:
               chain.append(tfb.BatchNormalization())
             bijector1 = tfb.MaskedAutoregressiveFlow(Made(params=2,hidden_units=[latent_size,latent_size],activation='leaky_relu'))
             #bijector1 = tfb.MaskedAutoregressiveFlow(shift_and_log_scale_fn=tfb.AutoregressiveNetwork(params=2, hidden_units=[latent_size,latent_size]))
             
             if config['n_perms_train'] == 0:
                chain.append(tfb.Blockwise(bijectors=[perms_swap[i],tfb.Identity()],block_sizes=[size[i],params['latent_layer']-size[i]],name='Permutation'))
                if config['batch_normalization'] and i%2==0:
                        chain.append(tfb.BatchNormalization())
             if config['n_perms_train'] == 1:
                chain.append(tfb.Blockwise(bijectors=[perms_train[i],tfb.Identity()],block_sizes=[size[i],params['latent_layer']-size[i]],name='Permutation'))
                if config['batch_normalization'] and i%2==0:
                        chain.append(tfb.BatchNormalization())
             if config['n_perms_train'] == 2:
                if i%2==0:
                    chain.append(tfb.Blockwise(bijectors=[perms_train[i],tfb.Identity()],block_sizes=[size[i],params['latent_layer']-size[i]],name='Permutation'))
                    #if config['batch_normalization']:
                        #chain.append(tfb.BatchNormalization())
                else:
                    chain.append(tfb.Blockwise(bijectors=[perms_swap[i],tfb.Identity()],block_sizes=[size[i],params['latent_layer']-size[i]],name='Permutation'))
             
        else:
        
            if config['batch_normalization']:
                chain.append(tfb.BatchNormalization())    
            if i<N_spline_layers: 
                # spline bijector
                bijector1 = tfb.RealNVP(num_masked=size[i]-size[i]//2,bijector_fn=splines[i],name='Neural_Spline')
            if config['N_rescale_shift_layers']!=0 and i>N_spline_layers and i<N_shift_layers+N_spline_layers: 
                # shift and scale 
                #if config['batch_normalization']:
                #   chain.append(tfb.BatchNormalization())
                bijector1 = tfb.RealNVP(num_masked=size[i]-size[i]//2,shift_and_log_scale_fn=tfb.real_nvp_default_template(hidden_layers=[params['latent_layer'],params['latent_layer']]),name='NVP_shit_n_scale') 
            else:
                #bijector1 = RealNVP(input_shape=input_shape,n_hidden=[latent_size,latent_size])
                bijector1 = tfb.RealNVP(num_masked=size[i]-size[i]//2,shift_and_log_scale_fn=tfb.real_nvp_default_template(hidden_layers=[params['latent_layer'],params['latent_layer']],shift_only=True),name='NVP_shift')  
            
            if config['n_perms_train'] == 0:
                chain.append(tfb.Blockwise(bijectors=[perms_swap[i],tfb.Identity()],block_sizes=[size[i],params['latent_layer']-size[i]],name='Permutation'))
                #if config['batch_normalization'] and i%2==0:
                 #       chain.append(tfb.BatchNormalization())
            if config['n_perms_train'] == 1:
                chain.append(tfb.Blockwise(bijectors=[perms_train[i],tfb.Identity()],block_sizes=[size[i],params['latent_layer']-size[i]],name='Permutation'))
                #if config['batch_normalization'] and i%2==0:
                #        chain.append(tfb.BatchNormalization())
            if config['n_perms_train'] == 2:
                if i%2==0:
                    chain.append(tfb.Blockwise(bijectors=[perms_train[i],tfb.Identity()],block_sizes=[size[i],params['latent_layer']-size[i]],name='Permutation'))
                    #if config['batch_normalization']:
                        #chain.append(tfb.BatchNormalization())
                else:
                    chain.append(tfb.Blockwise(bijectors=[perms_swap[i],tfb.Identity()],block_sizes=[size[i],params['latent_layer']-size[i]],name='Permutation'))
                #if config['batch_normalization'] and i%2==0:
                #        chain.append(tfb.BatchNormalization())
       
        bijector2 = tfb.Identity()
        chain.append(tfb.Blockwise(bijectors=[bijector1, bijector2], block_sizes=[size[i],params['latent_layer']-size[i]]))
    # Neural spline flow needs to act on interval [-1,1], hence the scaling, you might have to adapt this
    if config['scale'] != 1:
        chain.append(tfb.Scale(scale))
    #if config['batch_normalization']:
    #    chain.append(tfb.BatchNormalization())
    print(config['model']+" chain Size: ", len(chain))
    if config['bij_reverse']:
        chain = list(reversed(chain))

    # p(x) is Gaussian, chain: y=g(x)
    # bwd goes from last to first in the chain (inverse)
    flow=tfd.TransformedDistribution(distribution=prior,bijector=tfb.Chain(chain),name='myflow')
                 
    return flow, chain

def nvp_module_spec():
    z_sample     = tf.placeholder(tf.float32, shape=[None,params['latent_layer']])
    sample_size  = tf.placeholder(tf.int32, shape=[])
    u_sample     = tf.placeholder(tf.float32, shape=[None,params['latent_layer']])
    nvp_, bijector     = get_nvp()
    log_prob = nvp_.log_prob(z_sample)
    prob     = nvp_.prob(z_sample)
    nvp_sample   = nvp_.sample(sample_size)
    nvp_fwd_pass = nvp_.bijector.forward(u_sample)
    nvp_bwd_pass = nvp_.bijector.inverse(z_sample)
    hub.add_signature(inputs={'z_sample':z_sample,'sample_size':sample_size, 'u_sample':u_sample}\
                      ,outputs={'log_prob':log_prob, 'prob':prob, 'sample':nvp_sample, 'fwd_pass': nvp_fwd_pass, 'bwd_pass': nvp_bwd_pass})  
 
