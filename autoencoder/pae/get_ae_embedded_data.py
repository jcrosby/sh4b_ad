## Creates embedded data from AE latent layer to use as input to NF

import pandas as pd
import matplotlib
import seaborn
import keras
import tensorflow
import tensorflow as tf
import json
import numpy as np
from pathlib import Path

import sys

from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input, Dense
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard

from sklearn.model_selection import train_test_split

print ('Number of arguments:', len(sys.argv), 'arguments.')
print ('Argument List:', str(sys.argv))
n = len(sys.argv)
if (n != 4):
      print ("No arguments!. Need at least 1 model, 1 weight file and 1 input file in csv.zip")
      sys.exit()



load_model=sys.argv[1]
load_weights=sys.argv[2]
inputData=sys.argv[3]

aetail = Path(inputData).name
aeName = "./models/"+aetail.replace(".csv.gz","")
with open(aeName+"/params.json",'r') as f:
    params = json.load(f)

RANDOM_SEED = 101

predict_full=True
if (predict_full):
    print("##### Predicting Full Dataset #####")


### Get Data 

print("Reading=",inputData)
df = pd.read_csv(inputData, compression='gzip', header=0, sep=',')
print("DF size=",df.size," DF shape=",df.shape," DF dimension=",df.ndim)
#df = df.astype(np.float16)
#print(" Type=",df.dtypes)
#print("Convert to range= Min",numpy.finfo(numpy.float16).min," Max=",numpy.finfo(numpy.float16).max)


print("Skip run, event and weight columns..") 
#df.drop(['Run', 'Event', 'Weight'], axis = 1)
del df['Run']
del df['Event']
del df['Weight']
print("DF size=",df.size," DF shape=",df.shape," DF dimension=",df.ndim)


# do you want to drop columns based on common  data?
IsReadCommonEmptyColumns=1

# 1 drop colums based on common vector
# 2 drop colums as found by the current dataframe
# 0 do not drop anything

if (IsReadCommonEmptyColumns==1):
   file0="columns_with_0.txt"
   print("Read common 0 columns from ",file0)
   dcol0=pd.read_csv(file0,header = None)
   print ("-> Experimental: Drop columns with 0")
   col0=dcol0[dcol0.columns[0]]
   df=df.drop(col0, axis = 1)
   print("Total zero-cells removed=",len(dcol0))
elif (IsReadCommonEmptyColumns==2):
   print ("Experimental: find all columns with 0")
   col0 = df.columns[(df == 0).all()]
   print("COL=0 size=",col0.size," DF shape=",col0.shape," DF dimension=",col0.ndim)
   print(col0)
   #print ("Experimental: Drop columns with 0")
   #df=df.drop(col0, axis = 1)
   file0=modelName+"/columns_with_0.txt"
   print ("Experimental: Save columns with 0 in ",file0)
   print(type(col0))
   pd.Series(col0,index=col0).to_csv(file0, header=False, index=False)
   print ("Experimental: Restore columns with 0 from ",file0)
   dcol0=pd.read_csv(file0,header = None)
   col0=dcol0[dcol0.columns[0]]
   print ("-> Experimental: Drop columns with 0")
   df=df.drop(col0, axis = 1)
else:
   pass

#print(" -> Shuffle the DataFrame rows")
#df = df.sample(frac = 1)

#xhead=df.head()
#print(xhead)

print("")
SplitSize=0.3
print("## Data Preprocessing:") 
print("-> Validation fraction=",SplitSize," Training fraction=",1-SplitSize)
X_train, X_valid = train_test_split(df, test_size=SplitSize, random_state = RANDOM_SEED, shuffle=True)

# If you want to remove rows with some label (0) 
# X_train = X_train[X_train['Label'] == 0]
full = df.drop(['Label'], axis=1)
full = full.values
X_train = X_train.drop(['Label'], axis=1)
y_test  = X_valid['Label']
X_valid  = X_valid.drop(['Label'], axis=1)
X_train = X_train.values
X_valid  = X_valid.values
print('Training data size   :', X_train.shape)
print('Validation data size :', X_valid.shape)

input_dim = X_train.shape[1]


### Load AE model
model = tf.keras.models.load_model( load_model )
print("--> Loaded keras model from "+ load_model)
model.summary()

### Load model parameters
n_layers = params['n_layers']
enc_len = int((n_layers-1)/2)
dec_len = int((n_layers-1)/2)

enc_layers = []
dec_layers = []
for i in range(enc_len):
    nl = 'layer'+str(i+1)
    enc_layers.append(params[nl])
    dl = 'layer'+str(i+enc_len+1)
    dec_layers.append(params[dl])

enc_layer_names = []
dec_layer_names = []
for i in range(enc_len):
    nl = 'layer'+str(i+1)
    enc_layer_names.append(nl)
    dl = 'layer'+str(i+enc_len+1)
    dec_layer_names.append(dl)

## Get weights of AE layers up to the latent layer
layer_w = []
for p in range(enc_len):
    layer_w.append(model.get_layer(enc_layer_names[p]).get_weights())

latent_w = model.get_layer('latent').get_weights()

## Get layer dimensions
layer_dim = []
for p in range(enc_len):
    layer_dim.append(enc_layers[p])

latent_dim=params['latent_layer']

learning_rate=params['learning_rate']

#### Initiate Encoder
## [0] = weights
initializer = []
for i in range(enc_len):
    initializer.append(keras.initializers.Constant(layer_w[i][0]))

latent_initializer = keras.initializers.Constant(latent_w[0])

## [1] = bias
bias = []
for i in range(enc_len):
    bias.append(keras.initializers.Constant(layer_w[i][1]))

latent_bias = keras.initializers.Constant(latent_w[1])

#input Layer
input_layer = Input(shape=(input_dim, ),name="input")
encoder = tf.keras.layers.Dense(layer_dim[0], activation=tf.nn.leaky_relu,activity_regularizer=tf.keras.regularizers.l2(learning_rate),bias_initializer=bias[0],kernel_initializer=initializer[0],name=enc_layer_names[0])(input_layer)
for i in range(enc_len-1):
    encoder = tf.keras.layers.Dense(layer_dim[i+1], activation=tf.nn.leaky_relu,bias_initializer=bias[i+1],kernel_initializer=initializer[i+1],name=enc_layer_names[i+1])(encoder)
# encoder=tf.keras.layers.Dropout(0.2)(encoder)
#latent space
latent = tf.keras.layers.Dense(latent_dim, activation=tf.nn.leaky_relu,bias_initializer=latent_bias,kernel_initializer=latent_initializer,name="latent")(encoder)
#Our model
latent_encoder = keras.Model(inputs=input_layer,outputs=latent)

print("-->  Encoder to Latent Space Model")
# Check model
latent_encoder.summary()
# Compile model
latent_encoder.compile(optimizer='adam', loss='mse')

### SAVE ENCODER
latent_encoder.save(aeName+"/encoder", save_format='tf')
latent_encoder.save(aeName+"/encoder"+"/saved_model.keras")
latent_encoder.save_weights(aeName+"/encoder"+"/saved_weights.h5")

### Here we get our predictions
if (predict_full):
   scores = latent_encoder.predict(full,verbose=0)
else:
   scores = latent_encoder.predict(X_valid,verbose=0)

# Save as numpy array
print("##### SAVING LATENT OUTPUT #####")
np.save(aeName+"/ae_latent_data.npy",scores)
print("Saved!")
#### Initiate decoder to save
print("#### INITIATING DECODER ####")

## Get weights of AE layers up to the latent layer
layer_w_dec = []
for p in range(enc_len):
    layer_w_dec.append(model.get_layer(dec_layer_names[p]).get_weights())

decoder_output_w = model.get_layer('output').get_weights()

## Get layer dimensions
layer_dim_dec = []
for p in range(enc_len):
    layer_dim_dec.append(dec_layers[p])

output_dim=params['output_size']

learning_rate=params['learning_rate']

#### Initiate Decoder
## [0] = weights
initializer_dec = []
for i in range(enc_len):
    initializer_dec.append(keras.initializers.Constant(layer_w_dec[i][0]))

initializer_output = keras.initializers.Constant(decoder_output_w[0])

## [1] = bias
bias_dec = []
for i in range(dec_len):
    bias_dec.append(keras.initializers.Constant(layer_w_dec[i][1]))

output_bias = keras.initializers.Constant(decoder_output_w[1])

#decoder input Layer
decoder_input_layer = Input(shape=(latent_dim, ),name="decoder_input")
decoder = tf.keras.layers.Dense(layer_dim_dec[0], activation=tf.nn.leaky_relu,activity_regularizer=tf.keras.regularizers.l2(learning_rate),bias_initializer=bias_dec[0],kernel_initializer=initializer_dec[0],name=dec_layer_names[0])(decoder_input_layer)
for i in range(dec_len-1):
    decoder = tf.keras.layers.Dense(layer_dim_dec[i+1], activation=tf.nn.leaky_relu,bias_initializer=bias_dec[i+1],kernel_initializer=initializer_dec[i+1],name=dec_layer_names[i+1])(decoder)
#decoder output
output = tf.keras.layers.Dense(output_dim, activation=tf.nn.leaky_relu,bias_initializer=output_bias,kernel_initializer=initializer_output,name="output")(decoder)
#Our model
decoder_model = keras.Model(inputs=decoder_input_layer,outputs=output)

print("-->  Decoder to Output Model")
# Check model
decoder_model.summary()
# Compile model
decoder_model.compile(optimizer='adam', loss='mse')


### SAVE ENCODER
decoder_model.save(aeName+"/decoder", save_format='tf')
decoder_model.save(aeName+"/decoder"+"/saved_model.keras")
decoder_model.save_weights(aeName+"/decoder"+"/saved_weights.h5")


print("*------------------> Fin <------------------*")
