
import math
import numpy as np
from scipy.integrate import simps
#import tensorflow as tf
import tensorflow.compat.v1 as tf
import tensorflow_probability as tfp
tfd = tfp.distributions
tfb = tfp.bijectors

def get_prior(latent_size):
    return tfd.MultivariateNormalDiag(tf.zeros(latent_size), scale_identity_multiplier=0.25)


def get_posterior(encoder):

    def posterior(x):
        mu, sigma        = tf.split(encoder({'x':x},as_dict=True)['z'], 2, axis=-1)
        sigma            = tf.nn.softplus(sigma) + 1e-6
        approx_posterior = tfd.MultivariateNormalDiag(loc=mu, scale_diag=sigma)
        return approx_posterior

    return posterior


def get_likelihood(decoder,params):

    with tf.compat.v1.variable_scope("likelihood", reuse=tf.compat.v1.AUTO_REUSE):
        sigma = tf.compat.v1.get_variable(name='sigma', use_resource=False, initializer=tf.ones([np.prod(params['output_size'])])*params['sigma'])

    def likelihood(z):
        mean = decoder({'z':z},as_dict=True)['x']
        mean = tf.reshape(mean,[params['batch_size'],-1])
        return tfd.Independent(tfd.MultivariateNormalDiag(loc=mean,scale_diag=sigma))
        
    return likelihood


'''---------------------------------------- Negative Log Likelihood -------------------------------------------------'''

@tf.function
def nll(nvp, data):
    """
    Computes the negative log liklihood loss for a given distribution and given data.
    :param distribution: TensorFlow distribution, e.g. tf.TransformedDistribution.
    :param data: Data or a batch from data.
    :return: Negative Log Likelihodd loss.
    """
    log_prob = nvp.log_prob(data)
    loss = -tf.reduce_mean(log_prob)
    return log_prob,loss


@tf.function
def train_density_estimation(distribution, optimizer, batch):
    """
    Train function for density estimation normalizing flows.
    :param distribution: TensorFlow distribution, e.g. tf.TransformedDistribution.
    :param optimizer: TensorFlow keras optimizer, e.g. tf.keras.optimizers.Adam(..)
    :param batch: Batch of the train data.
    :return: loss.
    """
    with tf.GradientTape() as tape:
        tape.watch(distribution.trainable_variables)
        loss = -tf.reduce_mean(distribution.log_prob(batch))  # negative log likelihood
    gradients = tape.gradient(loss, distribution.trainable_variables)
    optimizer.apply_gradients(zip(gradients, distribution.trainable_variables))

    return loss


class PDF():
    def __init__(self,data,mu=0, sigma=1):
        self.mean = mu
        self.stdev = sigma
        self.data = data

    def calculate_mean(self):
        self.mean = sum(self.data) // len(self.data)
        return self.mean

    def calculate_stdev(self,mean,sample=True):
        if sample:
            n = len(self.data)-1
        else:
            n = len(self.data)
       # mean = self.mean
        sigma = 0
        for el in self.data:
            sigma += (el - mean)**2
        sigma = math.sqrt(sigma / n)
        self.stdev = sigma
        return self.stdev

    def pdf(self, x):
        return (1.0 / (self.stdev * math.sqrt(2*math.pi))) * math.exp(-0.5*((x - self.mean) / self.stdev) ** 2)
 

'''----------------- Sanity check: after training the integral of the pdf has to sum up to one ----------------------'''


def sanity_check(dist, xmin=-4.0, xmax=4.0, ymin=-4.0, ymax=4.0, mesh_count=1000):
    '''
    Implementation of a approximated integral over a mesh grid from [xmin, xmax, ymin, ymax].
    The higher mesh_count, the more accurate the approximation.

    :param dist: Tensorflow distribution (tfp.distribution).
    :param xmin: Min x value of mesh grid (float32).
    :param xmax: Max x value of mesh grid (float32).
    :param ymin: Min y value of mesh grid (float32).
    :param ymax: Max y value of mesh grid (float32).
    :param mesh_count: Number of samples per axis of the mesh_grid (int).
    :return: Approximated integral of dist over a mesh grid (should be close to 1).
    '''

    # create 2D mesh grid with mesh_count samples
    pdf1 = PDF(dist)

    x =   np.linspace(xmin, xmax, mesh_count)
    y = np.linspace(ymin, ymax, mesh_count)
    X, Y = tf.meshgrid(x, y)

    # concatenate the coordinates in an array
    concatenated_mesh_coordinates = tf.transpose(tf.stack([tf.reshape(Y, [-1]), tf.reshape(X, [-1])]))

    # calculate dA for the integral
    
    dA = ((xmax - xmin) * (ymax - ymin)) / (mesh_count ** 2)

    # calculate the probabilities of the concatenated samples and return the approximated integral
    #pm = dist.prob(concatenated_mesh_coordinates)
    sess = tf.Session()
    concatenated_mesh_coordinates.eval(session=sess)
    print("coords: ",concatenated_mesh_coordinates)
    pm = pdf1.pdf(concatenated_mesh_coordinates)

    integral = simps(simps(fx,x),y)
    return tf.reduce_sum(pm) * dA


