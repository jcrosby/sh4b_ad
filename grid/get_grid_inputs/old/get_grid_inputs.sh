# this parses dids_output.txt and creates grid_inputs.txt with desired recon

RECON='data17'
rm grid_inputs.txt

cat dids_output.txt | while read line || [[ -n $line ]];
do
  if grep "$RECON" <<< $line; then
     echo $line >> grid_inputs.txt
  fi
done
