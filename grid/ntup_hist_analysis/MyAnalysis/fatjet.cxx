//////////////////////////////////////////////////////////
// This class has been written by Bill Murray    5th April 2018
// Adapted for ZGamma Calibration October 2018
//////////////////////////////////////////////////////////

#define fatjet_cxx

#include "fatjet.h"
#include <iostream>

fatjet::fatjet() {
}

fatjet::~fatjet() {
}

bool fatjet::isSelected() {
  if (Pt() < 250.) return false;
  if (Eta() > 2. || Eta() < -2.) return false;
  if (M() < 40.) return false;
  //if (2*M()/Pt() > 1.) return false;
 
  return true;
}
bool fatjet::inLSB() {
LSB = false;
if (M() < 50.|| M() > 105.) return false;
//if (M() > 50.|| M() < 105.) LSB=true;
//if (2*M()/Pt() > 1.) return false;

//return LSB;
return true;
}

bool fatjet::inHwindow() {
if (M() < 105.|| M() > 140.) return false;
//Hwind = false;
//if (M() > 105.|| M() < 140.) Hwind=true;
//if (2*M()/Pt() > 1.) return false;

return true;
}


bool fatjet::inHSB() {
HSB = false;
if (M() < 140.|| M() > 200.) return false;
//if (2*M()/Pt() > 1.) return false;

return true;
//return HSB;
}
bool fatjet::isRatio() {
  if (2*M()/Pt() > 1.) return false;
 
  return true;
}

