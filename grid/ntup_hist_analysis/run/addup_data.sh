date=`date +%Y%m%d`
dir=Nominal_20220411_data_Xbbv3_Ftest #Nominal"_"${date}"_"data
outdir=Templates"_Data_"${date}_Ftest_45mlrj150
#outdir_year=Templates"_NewTuples_Data_Year_"${date}

if [[ -e $outdir ]] ; then
    echo dir $outdir already exists
    echo I quit
    exit
fi

mkdir ${outdir}
#mkdir ${outdir_year}

echo $

## Data add up by year
#hadd ${outdir_year}/data1516_13TeV.root ${dir}/Nominal-data15* ${dir}/Nominal-data16*
#hadd ${outdir_year}/data17_13TeV.root ${dir}/Nominal-data17*
#hadd ${outdir_year}/data18_13TeV.root ${dir}/Nominal-data18*

## Data add up
hadd ${outdir}/data.root ${dir}/Nominal-data*
