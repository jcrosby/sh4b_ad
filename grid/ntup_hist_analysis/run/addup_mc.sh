date=`date +%Y%m%d`
dir=Nominal_20230119_SH4b_all_bkgs #Nominal_all_sig_SH4B_20230119 #Nominal_20221215_SH4b_mc16e_trigEff #Nominal_20221214_SH4b_mc16e_trigEff #Nominal_20221129_SH4b_mc16e_trigEff #Nominal_20221128_SH4b_mc16e_trigEff #Nominal_20221124_SH4b_mc16e_trigEff #Nominal"_"${date}_SH4b
outdir=Templates"_SH_MC_"${date}_all
outdir_year=Templates"_NewTuples_MC_Year_"${date}

if [[ -e $outdir ]] ; then
    echo dir $outdir already exists
    echo I quit
    exit
fi

mkdir ${outdir}
#mkdir ${outdir_year}

echo $

# Full templates
hadd ${outdir}/sh_x200_s70.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X200_S70_*

hadd ${outdir}/sh_x300_s70.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X300_S70_*
hadd ${outdir}/sh_x300_s100.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X300_S100_*
hadd ${outdir}/sh_x300_s170.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X300_S170_*

hadd ${outdir}/sh_x400_s70.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X400_S70_*
hadd ${outdir}/sh_x400_s100.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X400_S100_*
hadd ${outdir}/sh_x400_s170.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X400_S170_*
hadd ${outdir}/sh_x400_s200.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X400_S200_*
hadd ${outdir}/sh_x400_s250.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X400_S250_*

hadd ${outdir}/sh_x750_s70.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X750_S70_*
hadd ${outdir}/sh_x750_s100.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X750_S100_*
hadd ${outdir}/sh_x750_s170.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X750_S170_*
hadd ${outdir}/sh_x750_s200.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X750_S200_*
hadd ${outdir}/sh_x750_s250.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X750_S250_*
hadd ${outdir}/sh_x750_s300.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X750_S300_*
hadd ${outdir}/sh_x750_s400.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X750_S400_*
hadd ${outdir}/sh_x750_s500.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X750_S500_*

hadd ${outdir}/sh_x1000_s70.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1000_S70_*
hadd ${outdir}/sh_x1000_s100.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1000_S100_*
hadd ${outdir}/sh_x1000_s170.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1000_S170_*
hadd ${outdir}/sh_x1000_s200.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1000_S200_*
hadd ${outdir}/sh_x1000_s250.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1000_S250_*
hadd ${outdir}/sh_x1000_s300.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1000_S300_*
hadd ${outdir}/sh_x1000_s400.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1000_S400_*
hadd ${outdir}/sh_x1000_s500.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1000_S500_*
hadd ${outdir}/sh_x1000_s750.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1000_S750_*

hadd ${outdir}/sh_x1500_s70.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1500_S70_*
hadd ${outdir}/sh_x1500_s100.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1500_S100_*
hadd ${outdir}/sh_x1500_s170.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1500_S170_*
hadd ${outdir}/sh_x1500_s200.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1500_S200_*
hadd ${outdir}/sh_x1500_s250.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1500_S250_*
hadd ${outdir}/sh_x1500_s300.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1500_S300_*
hadd ${outdir}/sh_x1500_s400.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1500_S400_*
hadd ${outdir}/sh_x1500_s500.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1500_S500_*
hadd ${outdir}/sh_x1500_s750.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1500_S750_*
hadd ${outdir}/sh_x1500_s1000.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X1500_S1000_*

hadd ${outdir}/sh_x2000_s70.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2000_S70_*
hadd ${outdir}/sh_x2000_s100.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2000_S100_*
hadd ${outdir}/sh_x2000_s170.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2000_S170_*
hadd ${outdir}/sh_x2000_s200.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2000_S200_*
hadd ${outdir}/sh_x2000_s250.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2000_S250_*
hadd ${outdir}/sh_x2000_s300.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2000_S300_*
hadd ${outdir}/sh_x2000_s400.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2000_S400_*
hadd ${outdir}/sh_x2000_s500.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2000_S500_*
hadd ${outdir}/sh_x2000_s750.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2000_S750_*
hadd ${outdir}/sh_x2000_s1000.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2000_S1000_*
hadd ${outdir}/sh_x2000_s1500.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2000_S1500_*

hadd ${outdir}/sh_x2500_s70.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2500_S70_*
hadd ${outdir}/sh_x2500_s100.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2500_S100_*
hadd ${outdir}/sh_x2500_s170.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2500_S170_*
hadd ${outdir}/sh_x2500_s200.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2500_S200_*
hadd ${outdir}/sh_x2500_s250.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2500_S250_*
hadd ${outdir}/sh_x2500_s300.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2500_S300_*
hadd ${outdir}/sh_x2500_s400.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2500_S400_*
hadd ${outdir}/sh_x2500_s500.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2500_S500_*
hadd ${outdir}/sh_x2500_s750.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2500_S750_*
hadd ${outdir}/sh_x2500_s1000.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2500_S1000_*
hadd ${outdir}/sh_x2500_s1500.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2500_S1500_*
hadd ${outdir}/sh_x2500_s2000.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X2500_S2000_*

hadd ${outdir}/sh_x3000_s70.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X3000_S70_*
hadd ${outdir}/sh_x3000_s100.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X3000_S100_*
hadd ${outdir}/sh_x3000_s170.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X3000_S170_*
hadd ${outdir}/sh_x3000_s200.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X3000_S200_*
hadd ${outdir}/sh_x3000_s250.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X3000_S250_*
hadd ${outdir}/sh_x3000_s300.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X3000_S300_*
hadd ${outdir}/sh_x3000_s400.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X3000_S400_*
hadd ${outdir}/sh_x3000_s500.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X3000_S500_*
hadd ${outdir}/sh_x3000_s750.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X3000_S750_*
hadd ${outdir}/sh_x3000_s1000.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X3000_S1000_*
hadd ${outdir}/sh_x3000_s1500.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X3000_S1500_*
hadd ${outdir}/sh_x3000_s2000.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X3000_S2000_*
hadd ${outdir}/sh_x3000_s2500.root ${dir}/nominal-Py8EG_A14NNPDF23LO_XHS_X3000_S2500_*
# QCD
hadd ${outdir}/qcd.root ${dir}/nominal-Pythia8EvtGen_A14NNPDF23LO_jetjet*
# ttbar
hadd ${outdir}/ttbar.root ${dir}/nominal-PhPy8EG_A14_ttbar*
