prun \
--inDS=user.dabattul.ntup_bkg_24022023.364704.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4WithSW.e7142_s3681_r13145_p5511_TREE \
--outDS=user.jcrosby.364704.e7142_s3681_r13145_p5511.sh4b_test1_hist1 \
--useAthenaPackages --cmtConfig=x86_64-centos7-gcc11-opt \
--writeInputToTxt=IN:in.txt \
--outputs=output_root:output.root \
--exec="mc_nosysts_mc20e in.txt 364704"
