## Making RMM Input

The base code should run out of the box to generate inputs for the SH4b analysis autoencoder, only the shell macros and location of data needs to be changed w.r.t. what you're running over. The locations to change are:

Script: `setup.sh`
```
export STORAGE="/mnt/shared/jcrosby/eos/sh4b/data/full/data/data18/"

export STORAGE_MC="/mnt/shared/jcrosby/eos/sh4b/signal/x750_s250/"

```

Script: `jobsetup.sh`
```
DIR_DATA=/mnt/shared/jcrosby/eos/sh4b/data/full/data/18/one/two/data18/
DIR_MC=/mnt/shared/jcrosby/eos/sh4b/signal/x750_s250/
```

Script: `Make_input`
```
if len(sys.argv) < 2:
   DIR="/mnt/shared/jcrosby/eos/sh4b/signal/x750_s250/"
```

In order to run, use one of the following three macros:
* `A_RUN_SH4B`
* `A_RUN_SH4B_BSM`
* `A_RUN_SH4B_MC`

**Export terminal output to a log file for your convenience**
```
source A_RUN_SH4B &> log.txt
```
It's set to run over 24 nodes in the GPU. When the program starts, it seperates files into log files and runs over them in batches. 
**THERE IS CURRRENTLY A MEMORY LEAK!!!!** It still runs and generates the output but it will never stop. 
It will continuously output:
```
running on node 4
```
This can either be fixed (ideal) or you can let it run a while until you know it has run over everything.

The output will make root files of all the lists of files that it generates. In the folder of the output, you'll see `list00.root`, `list01.root`, and so on. 
Once all the list outputs are generated we will `hadd` into a single root file:
```
hadd <file1.root> <file2.root> <target_file.root>
```