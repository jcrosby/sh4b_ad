//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jul 24 12:29:29 2023 by ROOT version 6.25/01
// from TTree AnalysisMiniTree/xAOD->NTuple tree
// found on file: user.dabattul.33660347._000001.output-hh4b.root
//////////////////////////////////////////////////////////

#ifndef analysis_h
#define analysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
using namespace std;
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class analysis {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          lumiBlock;
   vector<float>   *mcEventWeights;
   Float_t         averageInteractionsPerCrossing;
   Float_t         actualInteractionsPerCrossing;
   UInt_t          mcChannelNumber;
   Bool_t          trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100;
   Bool_t          trigPassed_HLT_2j55_bmv2c2060_split_ht300_L14J15;
   Bool_t          trigPassed_HLT_j175_bmv2c2040_split;
   Bool_t          trigPassed_HLT_j150_bmv2c2060_split_j50_bmv2c2060_split;
   Bool_t          trigPassed_HLT_j420_a10r_L1J100;
   Bool_t          trigPassed_HLT_j420_a10_lcw_L1J100;
   Bool_t          trigPassed_HLT_2j35_bmv2c2060_split_2j35_L14J15p0ETA25;
   Bool_t          trigPassed_HLT_j100_2j55_bmv2c2060_split;
   Bool_t          trigPassed_HLT_j225_bmv2c2060_split;
   Char_t          passRelativeDeltaRToVRJetCutTopo;
   Char_t          passRelativeDeltaRToVRJetCutUFO;
   vector<float>   *recojet_antikt4PFlow_NOSYS_pt;
   vector<float>   *recojet_antikt4PFlow_NOSYS_eta;
   vector<float>   *recojet_antikt4PFlow_NOSYS_phi;
   vector<float>   *recojet_antikt4PFlow_NOSYS_m;
   vector<char>    *recojet_antikt4PFlow_NOSYS_NNJvtPass;
   vector<char>    *recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_70;
   vector<char>    *recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_77;
   vector<char>    *recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_85;
   vector<char>    *recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_70;
   vector<char>    *recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_77;
   vector<char>    *recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_85;
   vector<float>   *recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_pt;
   vector<float>   *recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_eta;
   vector<float>   *recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_phi;
   vector<float>   *recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_m;
   vector<float>   *recojet_antikt4PFlow_NOSYS_Jvt;
   vector<float>   *recojet_antikt4PFlow_NOSYS_JvtRpt;
   vector<float>   *recojet_antikt4PFlow_NOSYS_JVFCorr;
   vector<unsigned int> *recojet_antikt4PFlow_NOSYS_jvt_selection;
   vector<float>   *recojet_antikt4PFlow_NOSYS_NNJvt;
   vector<float>   *recojet_antikt4PFlow_NOSYS_NNJvtRpt;
   vector<float>   *recojet_antikt4PFlow_GN2v00_pc;
   vector<float>   *recojet_antikt4PFlow_GN2v00_pu;
   vector<float>   *recojet_antikt4PFlow_GN2v00_pb;
   vector<float>   *recojet_antikt10Topo_NOSYS_pt;
   vector<float>   *recojet_antikt10Topo_NOSYS_eta;
   vector<float>   *recojet_antikt10Topo_NOSYS_phi;
   vector<float>   *recojet_antikt10Topo_NOSYS_m;
   vector<int>     *recojet_antikt10Topo_NOSYS_goodVRTrackJets;
   vector<float>   *recojet_antikt10Topo_NOSYS_minRelativeDeltaRToVRJet;
   vector<vector<float> > *recojet_antikt10Topo_NOSYS_leadingVRTrackJetsPt;
   vector<vector<float> > *recojet_antikt10Topo_NOSYS_leadingVRTrackJetsEta;
   vector<vector<float> > *recojet_antikt10Topo_NOSYS_leadingVRTrackJetsPhi;
   vector<vector<float> > *recojet_antikt10Topo_NOSYS_leadingVRTrackJetsM;
   vector<float>   *recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR12;
   vector<float>   *recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR13;
   vector<float>   *recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR32;
   vector<float>   *recojet_antikt10Topo_NOSYS_Xbb2020v3_Higgs;
   vector<float>   *recojet_antikt10Topo_NOSYS_Xbb2020v3_Top;
   vector<float>   *recojet_antikt10Topo_NOSYS_Xbb2020v3_QCD;
   vector<vector<char> > *recojet_antikt10Topo_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77;
   vector<vector<char> > *recojet_antikt10Topo_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85;
   vector<float>   *recojet_antikt10UFO_NOSYS_pt;
   vector<float>   *recojet_antikt10UFO_NOSYS_eta;
   vector<float>   *recojet_antikt10UFO_NOSYS_phi;
   vector<float>   *recojet_antikt10UFO_NOSYS_m;
   vector<int>     *recojet_antikt10UFO_NOSYS_goodVRTrackJets;
   vector<float>   *recojet_antikt10UFO_NOSYS_minRelativeDeltaRToVRJet;
   vector<vector<float> > *recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPt;
   vector<vector<float> > *recojet_antikt10UFO_NOSYS_leadingVRTrackJetsEta;
   vector<vector<float> > *recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPhi;
   vector<vector<float> > *recojet_antikt10UFO_NOSYS_leadingVRTrackJetsM;
   vector<float>   *recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR12;
   vector<float>   *recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR13;
   vector<float>   *recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR32;
   vector<float>   *recojet_antikt10UFO_NOSYS_Xbb2020v3_Higgs;
   vector<float>   *recojet_antikt10UFO_NOSYS_Xbb2020v3_Top;
   vector<float>   *recojet_antikt10UFO_NOSYS_Xbb2020v3_QCD;
   vector<float>   *recojet_antikt10UFO_NOSYS_GN2Xv01_phbb;
   vector<float>   *recojet_antikt10UFO_NOSYS_GN2Xv01_phcc;
   vector<float>   *recojet_antikt10UFO_NOSYS_GN2Xv01_ptop;
   vector<float>   *recojet_antikt10UFO_NOSYS_GN2Xv01_pqcd;
   vector<vector<char> > *recojet_antikt10UFO_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77;
   vector<vector<char> > *recojet_antikt10UFO_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85;

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_lumiBlock;   //!
   TBranch        *b_mcEventWeights;   //!
   TBranch        *b_averageInteractionsPerCrossing;   //!
   TBranch        *b_actualInteractionsPerCrossing;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_trigPassed_HLT_2j55_bmv2c2060_split_ht300_L14J15;   //!
   TBranch        *b_trigPassed_HLT_j175_bmv2c2040_split;   //!
   TBranch        *b_trigPassed_HLT_j150_bmv2c2060_split_j50_bmv2c2060_split;   //!
   TBranch        *b_trigPassed_HLT_j420_a10r_L1J100;   //!
   TBranch        *b_trigPassed_HLT_j420_a10_lcw_L1J100;   //!
   TBranch        *b_trigPassed_HLT_2j35_bmv2c2060_split_2j35_L14J15p0ETA25;   //!
   TBranch        *b_trigPassed_HLT_j100_2j55_bmv2c2060_split;   //!
   TBranch        *b_trigPassed_HLT_j225_bmv2c2060_split;   //!
   TBranch        *b_passRelativeDeltaRToVRJetCutTopo;   //!
   TBranch        *b_passRelativeDeltaRToVRJetCutUFO;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_pt;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_eta;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_phi;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_m;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_NNJvtPass;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_70;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_77;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_85;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_70;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_77;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_85;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_pt;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_eta;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_phi;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_m;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_Jvt;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_JvtRpt;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_JVFCorr;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_jvt_selection;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_NNJvt;   //!
   TBranch        *b_recojet_antikt4PFlow_NOSYS_NNJvtRpt;   //!
   TBranch        *b_recojet_antikt4PFlow_GN2v00_pc;   //!
   TBranch        *b_recojet_antikt4PFlow_GN2v00_pu;   //!
   TBranch        *b_recojet_antikt4PFlow_GN2v00_pb;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_pt;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_eta;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_phi;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_m;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_goodVRTrackJets;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_minRelativeDeltaRToVRJet;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsPt;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsEta;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsPhi;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsM;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR12;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR13;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR32;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_Xbb2020v3_Higgs;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_Xbb2020v3_Top;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_Xbb2020v3_QCD;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77;   //!
   TBranch        *b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_pt;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_eta;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_phi;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_m;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_goodVRTrackJets;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_minRelativeDeltaRToVRJet;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPt;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsEta;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPhi;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsM;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR12;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR13;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR32;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_Xbb2020v3_Higgs;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_Xbb2020v3_Top;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_Xbb2020v3_QCD;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_GN2Xv01_phbb;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_GN2Xv01_phcc;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_GN2Xv01_ptop;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_GN2Xv01_pqcd;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77;   //!
   TBranch        *b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85;   //!

   analysis(TTree *tree=0);
   virtual ~analysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef analysis_cxx
analysis::analysis(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("user.dabattul.33660347._000001.output-hh4b.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("user.dabattul.33660347._000001.output-hh4b.root");
      }
      f->GetObject("AnalysisMiniTree",tree);

   }
   Init(tree);
}

analysis::~analysis()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t analysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t analysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void analysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mcEventWeights = 0;
   recojet_antikt4PFlow_NOSYS_pt = 0;
   recojet_antikt4PFlow_NOSYS_eta = 0;
   recojet_antikt4PFlow_NOSYS_phi = 0;
   recojet_antikt4PFlow_NOSYS_m = 0;
   recojet_antikt4PFlow_NOSYS_NNJvtPass = 0;
   recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_70 = 0;
   recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_77 = 0;
   recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_85 = 0;
   recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_70 = 0;
   recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_77 = 0;
   recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_85 = 0;
   recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_pt = 0;
   recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_eta = 0;
   recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_phi = 0;
   recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_m = 0;
   recojet_antikt4PFlow_NOSYS_Jvt = 0;
   recojet_antikt4PFlow_NOSYS_JvtRpt = 0;
   recojet_antikt4PFlow_NOSYS_JVFCorr = 0;
   recojet_antikt4PFlow_NOSYS_jvt_selection = 0;
   recojet_antikt4PFlow_NOSYS_NNJvt = 0;
   recojet_antikt4PFlow_NOSYS_NNJvtRpt = 0;
   recojet_antikt4PFlow_GN2v00_pc = 0;
   recojet_antikt4PFlow_GN2v00_pu = 0;
   recojet_antikt4PFlow_GN2v00_pb = 0;
   recojet_antikt10Topo_NOSYS_pt = 0;
   recojet_antikt10Topo_NOSYS_eta = 0;
   recojet_antikt10Topo_NOSYS_phi = 0;
   recojet_antikt10Topo_NOSYS_m = 0;
   recojet_antikt10Topo_NOSYS_goodVRTrackJets = 0;
   recojet_antikt10Topo_NOSYS_minRelativeDeltaRToVRJet = 0;
   recojet_antikt10Topo_NOSYS_leadingVRTrackJetsPt = 0;
   recojet_antikt10Topo_NOSYS_leadingVRTrackJetsEta = 0;
   recojet_antikt10Topo_NOSYS_leadingVRTrackJetsPhi = 0;
   recojet_antikt10Topo_NOSYS_leadingVRTrackJetsM = 0;
   recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR12 = 0;
   recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR13 = 0;
   recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR32 = 0;
   recojet_antikt10Topo_NOSYS_Xbb2020v3_Higgs = 0;
   recojet_antikt10Topo_NOSYS_Xbb2020v3_Top = 0;
   recojet_antikt10Topo_NOSYS_Xbb2020v3_QCD = 0;
   recojet_antikt10Topo_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77 = 0;
   recojet_antikt10Topo_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85 = 0;
   recojet_antikt10UFO_NOSYS_pt = 0;
   recojet_antikt10UFO_NOSYS_eta = 0;
   recojet_antikt10UFO_NOSYS_phi = 0;
   recojet_antikt10UFO_NOSYS_m = 0;
   recojet_antikt10UFO_NOSYS_goodVRTrackJets = 0;
   recojet_antikt10UFO_NOSYS_minRelativeDeltaRToVRJet = 0;
   recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPt = 0;
   recojet_antikt10UFO_NOSYS_leadingVRTrackJetsEta = 0;
   recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPhi = 0;
   recojet_antikt10UFO_NOSYS_leadingVRTrackJetsM = 0;
   recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR12 = 0;
   recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR13 = 0;
   recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR32 = 0;
   recojet_antikt10UFO_NOSYS_Xbb2020v3_Higgs = 0;
   recojet_antikt10UFO_NOSYS_Xbb2020v3_Top = 0;
   recojet_antikt10UFO_NOSYS_Xbb2020v3_QCD = 0;
   recojet_antikt10UFO_NOSYS_GN2Xv01_phbb = 0;
   recojet_antikt10UFO_NOSYS_GN2Xv01_phcc = 0;
   recojet_antikt10UFO_NOSYS_GN2Xv01_ptop = 0;
   recojet_antikt10UFO_NOSYS_GN2Xv01_pqcd = 0;
   recojet_antikt10UFO_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77 = 0;
   recojet_antikt10UFO_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
   fChain->SetBranchAddress("mcEventWeights", &mcEventWeights, &b_mcEventWeights);
   fChain->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing, &b_averageInteractionsPerCrossing);
   fChain->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing, &b_actualInteractionsPerCrossing);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("trigPassed_HLT_2j55_bmv2c2060_split_ht300_L14J15", &trigPassed_HLT_2j55_bmv2c2060_split_ht300_L14J15, &b_trigPassed_HLT_2j55_bmv2c2060_split_ht300_L14J15);
   fChain->SetBranchAddress("trigPassed_HLT_j175_bmv2c2040_split", &trigPassed_HLT_j175_bmv2c2040_split, &b_trigPassed_HLT_j175_bmv2c2040_split);
   fChain->SetBranchAddress("trigPassed_HLT_j150_bmv2c2060_split_j50_bmv2c2060_split", &trigPassed_HLT_j150_bmv2c2060_split_j50_bmv2c2060_split, &b_trigPassed_HLT_j150_bmv2c2060_split_j50_bmv2c2060_split);
   fChain->SetBranchAddress("trigPassed_HLT_j420_a10r_L1J100", &trigPassed_HLT_j420_a10r_L1J100, &b_trigPassed_HLT_j420_a10r_L1J100);
   fChain->SetBranchAddress("trigPassed_HLT_j420_a10_lcw_L1J100", &trigPassed_HLT_j420_a10_lcw_L1J100, &b_trigPassed_HLT_j420_a10_lcw_L1J100);
   fChain->SetBranchAddress("trigPassed_HLT_2j35_bmv2c2060_split_2j35_L14J15p0ETA25", &trigPassed_HLT_2j35_bmv2c2060_split_2j35_L14J15p0ETA25, &b_trigPassed_HLT_2j35_bmv2c2060_split_2j35_L14J15p0ETA25);
   fChain->SetBranchAddress("trigPassed_HLT_j100_2j55_bmv2c2060_split", &trigPassed_HLT_j100_2j55_bmv2c2060_split, &b_trigPassed_HLT_j100_2j55_bmv2c2060_split);
   fChain->SetBranchAddress("trigPassed_HLT_j225_bmv2c2060_split", &trigPassed_HLT_j225_bmv2c2060_split, &b_trigPassed_HLT_j225_bmv2c2060_split);
   fChain->SetBranchAddress("passRelativeDeltaRToVRJetCutTopo", &passRelativeDeltaRToVRJetCutTopo, &b_passRelativeDeltaRToVRJetCutTopo);
   fChain->SetBranchAddress("passRelativeDeltaRToVRJetCutUFO", &passRelativeDeltaRToVRJetCutUFO, &b_passRelativeDeltaRToVRJetCutUFO);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_pt", &recojet_antikt4PFlow_NOSYS_pt, &b_recojet_antikt4PFlow_NOSYS_pt);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_eta", &recojet_antikt4PFlow_NOSYS_eta, &b_recojet_antikt4PFlow_NOSYS_eta);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_phi", &recojet_antikt4PFlow_NOSYS_phi, &b_recojet_antikt4PFlow_NOSYS_phi);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_m", &recojet_antikt4PFlow_NOSYS_m, &b_recojet_antikt4PFlow_NOSYS_m);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_NNJvtPass", &recojet_antikt4PFlow_NOSYS_NNJvtPass, &b_recojet_antikt4PFlow_NOSYS_NNJvtPass);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_70", &recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_70, &b_recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_70);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_77", &recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_77, &b_recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_77);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_85", &recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_85, &b_recojet_antikt4PFlow_NOSYS_ftag_select_DL1dv01_FixedCutBEff_85);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_70", &recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_70, &b_recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_70);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_77", &recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_77, &b_recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_77);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_85", &recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_85, &b_recojet_antikt4PFlow_NOSYS_ftag_select_GN120220509_FixedCutBEff_85);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_pt", &recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_pt, &b_recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_pt);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_eta", &recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_eta, &b_recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_eta);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_phi", &recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_phi, &b_recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_phi);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_m", &recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_m, &b_recojet_antikt4PFlow_NOSYS_NoBJetCalibMomentum_m);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_Jvt", &recojet_antikt4PFlow_NOSYS_Jvt, &b_recojet_antikt4PFlow_NOSYS_Jvt);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_JvtRpt", &recojet_antikt4PFlow_NOSYS_JvtRpt, &b_recojet_antikt4PFlow_NOSYS_JvtRpt);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_JVFCorr", &recojet_antikt4PFlow_NOSYS_JVFCorr, &b_recojet_antikt4PFlow_NOSYS_JVFCorr);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_jvt_selection", &recojet_antikt4PFlow_NOSYS_jvt_selection, &b_recojet_antikt4PFlow_NOSYS_jvt_selection);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_NNJvt", &recojet_antikt4PFlow_NOSYS_NNJvt, &b_recojet_antikt4PFlow_NOSYS_NNJvt);
   fChain->SetBranchAddress("recojet_antikt4PFlow_NOSYS_NNJvtRpt", &recojet_antikt4PFlow_NOSYS_NNJvtRpt, &b_recojet_antikt4PFlow_NOSYS_NNJvtRpt);
   fChain->SetBranchAddress("recojet_antikt4PFlow_GN2v00_pc", &recojet_antikt4PFlow_GN2v00_pc, &b_recojet_antikt4PFlow_GN2v00_pc);
   fChain->SetBranchAddress("recojet_antikt4PFlow_GN2v00_pu", &recojet_antikt4PFlow_GN2v00_pu, &b_recojet_antikt4PFlow_GN2v00_pu);
   fChain->SetBranchAddress("recojet_antikt4PFlow_GN2v00_pb", &recojet_antikt4PFlow_GN2v00_pb, &b_recojet_antikt4PFlow_GN2v00_pb);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_pt", &recojet_antikt10Topo_NOSYS_pt, &b_recojet_antikt10Topo_NOSYS_pt);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_eta", &recojet_antikt10Topo_NOSYS_eta, &b_recojet_antikt10Topo_NOSYS_eta);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_phi", &recojet_antikt10Topo_NOSYS_phi, &b_recojet_antikt10Topo_NOSYS_phi);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_m", &recojet_antikt10Topo_NOSYS_m, &b_recojet_antikt10Topo_NOSYS_m);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_goodVRTrackJets", &recojet_antikt10Topo_NOSYS_goodVRTrackJets, &b_recojet_antikt10Topo_NOSYS_goodVRTrackJets);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_minRelativeDeltaRToVRJet", &recojet_antikt10Topo_NOSYS_minRelativeDeltaRToVRJet, &b_recojet_antikt10Topo_NOSYS_minRelativeDeltaRToVRJet);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_leadingVRTrackJetsPt", &recojet_antikt10Topo_NOSYS_leadingVRTrackJetsPt, &b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsPt);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_leadingVRTrackJetsEta", &recojet_antikt10Topo_NOSYS_leadingVRTrackJetsEta, &b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsEta);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_leadingVRTrackJetsPhi", &recojet_antikt10Topo_NOSYS_leadingVRTrackJetsPhi, &b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsPhi);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_leadingVRTrackJetsM", &recojet_antikt10Topo_NOSYS_leadingVRTrackJetsM, &b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsM);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR12", &recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR12, &b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR12);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR13", &recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR13, &b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR13);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR32", &recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR32, &b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsDeltaR32);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_Xbb2020v3_Higgs", &recojet_antikt10Topo_NOSYS_Xbb2020v3_Higgs, &b_recojet_antikt10Topo_NOSYS_Xbb2020v3_Higgs);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_Xbb2020v3_Top", &recojet_antikt10Topo_NOSYS_Xbb2020v3_Top, &b_recojet_antikt10Topo_NOSYS_Xbb2020v3_Top);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_Xbb2020v3_QCD", &recojet_antikt10Topo_NOSYS_Xbb2020v3_QCD, &b_recojet_antikt10Topo_NOSYS_Xbb2020v3_QCD);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77", &recojet_antikt10Topo_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77, &b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77);
   fChain->SetBranchAddress("recojet_antikt10Topo_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85", &recojet_antikt10Topo_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85, &b_recojet_antikt10Topo_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_pt", &recojet_antikt10UFO_NOSYS_pt, &b_recojet_antikt10UFO_NOSYS_pt);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_eta", &recojet_antikt10UFO_NOSYS_eta, &b_recojet_antikt10UFO_NOSYS_eta);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_phi", &recojet_antikt10UFO_NOSYS_phi, &b_recojet_antikt10UFO_NOSYS_phi);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_m", &recojet_antikt10UFO_NOSYS_m, &b_recojet_antikt10UFO_NOSYS_m);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_goodVRTrackJets", &recojet_antikt10UFO_NOSYS_goodVRTrackJets, &b_recojet_antikt10UFO_NOSYS_goodVRTrackJets);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_minRelativeDeltaRToVRJet", &recojet_antikt10UFO_NOSYS_minRelativeDeltaRToVRJet, &b_recojet_antikt10UFO_NOSYS_minRelativeDeltaRToVRJet);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPt", &recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPt, &b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPt);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_leadingVRTrackJetsEta", &recojet_antikt10UFO_NOSYS_leadingVRTrackJetsEta, &b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsEta);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPhi", &recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPhi, &b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPhi);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_leadingVRTrackJetsM", &recojet_antikt10UFO_NOSYS_leadingVRTrackJetsM, &b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsM);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR12", &recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR12, &b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR12);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR13", &recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR13, &b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR13);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR32", &recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR32, &b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsDeltaR32);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_Xbb2020v3_Higgs", &recojet_antikt10UFO_NOSYS_Xbb2020v3_Higgs, &b_recojet_antikt10UFO_NOSYS_Xbb2020v3_Higgs);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_Xbb2020v3_Top", &recojet_antikt10UFO_NOSYS_Xbb2020v3_Top, &b_recojet_antikt10UFO_NOSYS_Xbb2020v3_Top);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_Xbb2020v3_QCD", &recojet_antikt10UFO_NOSYS_Xbb2020v3_QCD, &b_recojet_antikt10UFO_NOSYS_Xbb2020v3_QCD);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_GN2Xv01_phbb", &recojet_antikt10UFO_NOSYS_GN2Xv01_phbb, &b_recojet_antikt10UFO_NOSYS_GN2Xv01_phbb);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_GN2Xv01_phcc", &recojet_antikt10UFO_NOSYS_GN2Xv01_phcc, &b_recojet_antikt10UFO_NOSYS_GN2Xv01_phcc);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_GN2Xv01_ptop", &recojet_antikt10UFO_NOSYS_GN2Xv01_ptop, &b_recojet_antikt10UFO_NOSYS_GN2Xv01_ptop);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_GN2Xv01_pqcd", &recojet_antikt10UFO_NOSYS_GN2Xv01_pqcd, &b_recojet_antikt10UFO_NOSYS_GN2Xv01_pqcd);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77", &recojet_antikt10UFO_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77, &b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77);
   fChain->SetBranchAddress("recojet_antikt10UFO_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85", &recojet_antikt10UFO_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85, &b_recojet_antikt10UFO_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85);
   Notify();
}

Bool_t analysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void analysis::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t analysis::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef analysis_cxx
