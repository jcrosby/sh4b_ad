#!/bin/bash
# S.Chekanov (ANL)

echo "Set ROOT enviroment for Dijet+Lepton program"

HH=`hostname -A` 

echo "HOST=$HH"

export STORAGE="/mnt/shared/jcrosby/eos/sh4b/data/full/data/18/one/two/data18/"

export STORAGE_MC="/mnt/shared/jcrosby/eos/sh4b/signal/x750_s250/"

FANN=./lib/fann/src/
export LD_LIBRARY_PATH=$FANN:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=./lib/src/:$LD_LIBRARY_PATH

echo "DATA STORAGE=$STORAGE"
echo "BSM STORAGE=$STORAGE_MC"
