// put here some global variables 
#include <iostream>
using namespace std;

// use FANN
#define FANN 1

#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<TROOT.h>
#include"Global.h"
#include"TRandom3.h"
#include <algorithm>
#include <vector>
#include <sstream>

// constructor
Global::Global() {


    timer.Start();
    nev = 0;
    nevfill=0;
    debug=false; 
    systematics=0;
    MaxEvents=2147483640;
    data_year=2016;
    sumOfWeights=0;
    max_events4ANN=2147483640; // maximum events for RMM fill
    Nelec=0;
    Nmuon=0;
    firstEvent=true;
    isMonteCarlo=false;
    saveEvent=true;

    grandom= new TRandom();

 // min PT to trigger data fill 
 // jet/electrons have different pT!
  PT_CUT=420;
  PT_LEPTON=30;
  PT_PHOTON=30;
  PT_LEPTON_LEAD=30; // leading pT of lepton used for selection
  MJJ_CUT=35; // default Mjj  
  ETA_CUT=2.0;
  MIN_PT_CUT=60;
  MIN_MASS=50;
  /*
  TFile* f = TFile::Open("2015-PreRecomm-13TeV-MC12-CDI_August3-v1.root");
  line=(TSpline3*)f->Get("MV2c20/AntiKt4EMTopoJets/FlatBEff_85/cutprofile");
  line1=(TSpline3*)f->Get("MV2c20/AntiKt4EMTopoJets/FlatBEff_70/cutprofile");
  */

  cout << "########  Cuts=" << endl;
  cout << "         minPT=" << PT_CUT << endl;
  cout << "         maxEta=" << ETA_CUT << endl;
  
  // RMM settings for this ANN
  angNorm=0.15; // factor to increase weights of angles compare to masses
  maxNumber=5;   // max number for each object (MET is not counted)
  maxTypes=2;   // max numbers of types (met not counted)
  mSize=maxTypes*maxNumber; //+1;
  CMS=13000; // GeV;
  //string names[maxTypes+1] = {"MET","j", "b", "#mu", "e", "#gamma"};
  string names[maxTypes] = {"j","bb"};
  //cout << "here1" << endl;
  //Names1.push_back(names[0]);
  
  for (int h = 0; h < maxTypes; h++) {
             for (int i = 0; i <  maxNumber; i++) {
                        if ( i == 0 && h == 1) {Names1.push_back(" "); continue;}
                        int m=i;
                        //ostringstream ss;
                        //ss << i;
                        if (h==1) m = m-1;
                        string ss = to_string(m);
                        Names1.push_back(names[h]+"_{"+ss+"}");
                        
                }
        }
    for (unsigned int i=0; i<Names1.size(); i++) {
               cout << "Name=" << i << " " << Names1.at(i) << endl;
               Names2.push_back(Names1.at(i));
     }
}


// destructor
Global::~Global () {

    cout << "real time=" << timer.RealTime() << endl;
    timer.Stop();

}


//cout << "here3" << endl;

// read ntuple list
void Global::getNtuples(string xname)
{

    cout << "HERE1" << endl;
    string name="inputs/"+xname;
    ifstream myfile;
    myfile.open(name.c_str(), ios::in);


    if (!myfile) {
      cerr << "Global::getNtuples(): Can't open input file:  " << name << endl;
      exit(1);
    } else {
        cout << "-> Read data file=" << name << endl;
      }

     string temp;
     while (myfile >> temp) {
 //the following line trims white space from the beginning of the string
           temp.erase(temp.begin(), std::find_if(temp.begin(), temp.end(), not1(ptr_fun<int, int>(isspace))));
            if (temp.find("#") == 0) continue; 
            ntup.push_back(temp);

     }
    cout << "-> Number of runs=" << ntup.size()  << endl;
    myfile.close();

    cout << "We got Here2 *******" << endl;
    
    for (unsigned int i=0; i<ntup.size(); i++) {
           cout << ".. file to analyse="+ntup[i] << endl;
    }

}


// read initial parameters
void Global::getIni()
{

    nev=0;
    string name="main.ini";
    ifstream myfile;
    myfile.open(name.c_str(), ios::in);

    if (!myfile) {
      cerr << "\nGlobal::getIni(): Can't open input file:  " << name << endl;
      exit(1);
    } else {
        cout << "\nRead file=" << name << endl;
    }
    //cout << "We got here4 *****" << endl;
    string message;
    int    number;
    myfile >> message >> MaxEvents;
    myfile >> message >> number;
    myfile >> message >> type;
    myfile >> message >> systematics;
    debug=false;  
    if (number==1) debug=true;
    myfile.close();
    print_init();               // print initialisations
    print_cuts();               // print initialisations
    //cout << "We got here5 *****" << endl;

/*
    name="lumi/lumitable2017.csv";
    ifstream mylumi2017file;
    mylumi2017file.open(name.c_str(), ios::in);
    if (!mylumi2017file) {
      cerr << "\nGlobal::getIni(): Can't open input file:  " << name << endl;
      exit(1);
    } else {
        cout << "\nRead file=" << name << endl;
    }
    string value;
    while (  mylumi2017file.good() )
    {

     //the following line trims white space from the beginning of the string
     temp.erase(temp.begin(), std::find_if(temp.begin(), temp.end(), not1(ptr_fun<int, int>(isspace))));
     if (temp.find("#") == 0) continue;
     getline (  mylumi2017file, value, ',' ); // read a string until next comma: http://www.cplusplus.com/reference/string/getline/
     cout << string( value, 1, value.length()-2 ); // display value removing the first and the last character from it
    }

    mylumi2017file.close();
*/



}




